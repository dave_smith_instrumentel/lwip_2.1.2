
import logging
logger = logging.getLogger(__name__)

import argparse
import os
import socket
import sys
import time
import struct
import base64

VERSION_CMD =       1
BL_CMD =            2
UPDATE_CMD =        3
ERASE_DB =          4

class BoardComms(object):
    """
    Base class for handling the comms with the board
    """
    def __init__(self, timeout=10):
        self.timeout = timeout
        
    def connect(self):
        """
        Connect to the board
        Return: True if successful, False if unsuccessful
        """
        return False
        
    def send_message(self, bindata):
        """
        Send the binary data to the board
        Return:
        """
        return None
        
    def recv_message(self):
        """
        Return any list of any response packets
        """
        return []
        
class IPComms(BoardComms):
    """
    IP communications to board
    """
    def __init__(self, ip_address="", **kwargs):
        super().__init__(**kwargs)
        self.ip = ip_address
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
    
    def connect(self):
        """
        Connect and enable TCP comms forward/backward
        """
        try:
            self.socket.connect((self.ip, 4678))
            
        except Exception as e:
            print("failed connection", e)
            return False
        
        # No Exceptions = assume connection worked
        return True
        
    def send_message(self, bindata):
        """
        Send the binary data to the board
        Return:
        """
        self.socket.send(bindata)
        
    def recv_message(self):
        """
        Return any data recieved, will block on timeout
        Return:
        """
        try:
            reply = self.socket.recv()
            
        except Exception as e:
            print(e)
            return
        
        return reply
    
    def close_socket(self):
        self.socket.close()
        

class AutoUpdater( object ):
    def __init__(self, ip_address="", timeout=""):
        #super().__init__(**kwargs)
        self.ip = ip_address
        self.socket = None
        self.timeout = timeout
        self.preamble = "$INST".encode('utf-8')
        self.encoding = 'utf-8'
        self.count_suc_updates = 0              # Counting successful updates (suc = successful)
        self.count_usc_updates = 0              # Counting unsuccesful updates (usc = unsuccessful)
        self.count_connection_errors = 0        # Counting connection errors
        self.count_recv_errors = 0              # Counting receiving errors
        self.previous_fw_versions = 0
        self.current_fw_version = 0
        self.boot_on_same_fw = 0
          
    def recreate_socket(self):
        """
        init the socket ready for a new connection
        """
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(self.timeout)
        
    def run(self):
        """
        
        """
        logger.info("Updating the DB...")
        
        #self.connect()
        
        while True:
            
            logger.debug("Successful updates: {}".format(self.count_suc_updates))
            logger.debug("Unsuccessful updates: {}".format(self.count_usc_updates))
            logger.debug("Boot up on the same FW: {}".format(self.boot_on_same_fw))
            logger.debug("Connection Errors: {}".format(self.count_connection_errors))
            logger.debug("Receiving Errors: {}".format(self.count_recv_errors))
            
            self.on_update(ERASE_DB)
            
            self.on_update(VERSION_CMD)
            time.sleep(2)
            
            self.on_update(BL_CMD)
            time.sleep(2)
            
            self.on_update(UPDATE_CMD)
            time.sleep(2)
            
            if self.count_connection_errors == 0 and self.count_recv_errors == 0:
                self.count_suc_updates += 1
            else:
                self.count_usc_updates += 1
            
            #self.close_socket()
            
            print("\n\n")
            
            time.sleep(20)   
        
    def on_update(self, a_cmd):
        """
        
        """
        if a_cmd == VERSION_CMD:
            _msg = self.build_message("dbfirmware,version")
            logger.debug(_msg)
            version_reply = self.send_bytearray(_msg)
            
            time.sleep(1)
            
            logger.debug("Rx Data: {}".format(version_reply))
            
            self.current_fw_version = version_reply
            
            if self.previous_fw_versions != self.current_fw_version:
                self.count_suc_updates += 1
                
                version_hex = self.print_bin_as_hex(self.current_fw_version)
                
                if version_hex == '4750494F2D44422D3200504342303037302D34000100050030':
                    filepath = "C:\\Users\\site\\Desktop\\FW101_codebase\\GoldenTool\\branches\\20.07.001\\FW0070\\db_bin\\FW000070_Smart_A.bin"
                else:
                    filepath = "C:\\Users\\site\\Desktop\\FW101_codebase\\GoldenTool\\branches\\20.07.001\\FW0070\\db_bin\\FW000070_Smart_B.bin"
                
                self.previous_fw_versions = self.current_fw_version
                
                logger.debug("New firmware loaded...")
                
                self.erase_flash(filepath)
                
                self.write_to_flash(filepath)
                

            elif self.previous_fw_versions == self.current_fw_version:
                self.boot_on_same_fw += 1
                logger.debug("Hub boots up using the same firmware...")
            else:
                self.count_usc_updates += 1
                logger.error("Something went wrong...")
                   
        
        if a_cmd == BL_CMD:
            self.bootloader_mode_cmd()
            
        if a_cmd == UPDATE_CMD:
            self.update_cmd()
            '''
            _msg = self.build_message("dbfirmware,update_db")
            logger.debug(_msg)
            self.send_message(_msg)
            
            time.sleep(1)
            
            rdata = self.recv_message()
            logger.debug("Rx Data: {}".format(rdata))
            
            time.sleep(1)
        
            #self.close_socket()
            '''
    
    def erase_flash(self, a_filepath):
        
        with open(a_filepath, 'rb') as f:
            _data = f.read()  # read the first 4 data
        
        logger.debug("Size of the BIN file is: {}".format(len(_data)))
        
        erase_msg_str = "dbfirmware,erase_flash={}:{}".format(len(_data), 0)
        
        erase_msg = self.build_message(erase_msg_str)
        logger.debug(erase_msg)
        #self.send_message(erase_msg)
                
        erase_reply = self.send_bytearray(erase_msg)
        
        time.sleep(1)
            
        #erase_reply = self.recv_message()
        logger.debug("Rx Data: {}".format(erase_reply))
        
    def write_to_flash(self, a_filepath):
        with open(a_filepath, 'rb') as f:
            _contents = f.read()  # read the first 4 data
                
        logger.debug("Filepath: {}".format(a_filepath))
        logger.debug("Size of BIN: {}".format(len(_contents)))
            
        _contents = struct.pack("<I", len(_contents)) + _contents
            
        CHUNK_SIZE = 1020 # = 341 * 3
            
        for x in range(0, len(_contents), CHUNK_SIZE):
                
            _binchunk = _contents[x:x+CHUNK_SIZE]
            #_dat = print_bin_as_hex(_binchunk)
            #print("Data are: {}".format(_dat))
            #print("Size of BIN chunk is: {}".format(len(_binchunk)))
            #logger.debug("Size of BIN chunk is: {}".format(len(_binchunk)))
        
            if len(_binchunk) %3 != 0:
                # need to round up nicely to 4 bytes
                _binchunk += bytearray([0]*(3-(len(_binchunk) %3)))
                #print("new length:", len(_binchunk))
        
            _b64chunk = base64.b64encode(_binchunk).decode('ascii')
            #print(len(_b64chunk))
    
            _msg_str = "dbfirmware,bin={}:{}:{}".format(x, len(_b64chunk), _b64chunk)
            bin_msg = self.build_message(_msg_str)
            
            _reply = self.send_bytearray(bin_msg)
            logger.debug("Rx Data: {}".format(_reply))
               
    def bootloader_mode_cmd(self):
        bl_cmd_msg = self.build_message("dbfirmware,blmode")
        logger.debug(bl_cmd_msg)
        #self.send_message(_msg)
        
        bl_mode_reply = self.send_bytearray(bl_cmd_msg)
        
        time.sleep(1)
        
        #rdata = self.recv_message()
        logger.debug("Rx Data: {}".format(bl_mode_reply))
        
    def update_cmd(self):
        update_cmd_msg = self.build_message("dbfirmware,update_db")
        logger.debug(update_cmd_msg)
        
        update_cmd_reply = self.send_bytearray(update_cmd_msg)
        
        time.sleep(1)
            
        #erase_reply = self.recv_message()
        logger.debug("Rx Data: {}".format(update_cmd_reply))
        
    
    def connect(self):
        """
        Connect and enable TCP comms forward/backward
        """
        try:
            self.recreate_socket()
            self.socket.connect((self.ip, 4678))
        
        except Exception as e:
            self.count_connection_errors += 1
            logger.error("Failed connection {}".format(e))
            #print("failed connection", e)
            return False
        
        # No Exceptions = assume connection worked
        return True
    
    def send_message(self, bindata):
        """
        Send the binary data to the board
        Return:
        """
        self.socket.send(bindata)
    
    def send_bytearray(self, command):
        #print("Tx: {}".format(command))
        
        try:
            self.recreate_socket()
            self.socket.connect((self.ip, 4678))
        
        except Exception as e:
            self.count_connection_errors += 1
            logger.error("Failed connection {}".format(e))
            #print("failed connection", e)
            return False
        
        self.socket.send(command)
        
        try:
            reply = self.socket.recv(1000)
            
        except Exception as e:
            self.count_recv_errors += 1
            logger.error("Failed receive {}".format(e))
            #print(e)
            return
        
        self.socket.close()
        return reply
        
        
    
    def recv_message(self):
        """
        Return any data recieved, will block on timeout
        Return:
        """
        try:
            reply = self.socket.recv(10000)
            
        except Exception as e:
            self.count_recv_errors += 1
            logger.error("Failed receive {}".format(e))
            #print(e)
            return
        
        return reply
        
    def close_socket(self):
        logger.info("Closing the socket until next update...")
        self.socket.close()
        
    def print_bin_as_hex(self, a_data):
        return "".join(["{:02X}".format(x) for x in a_data])
    
    def build_message(self, a_message):
        _payload = a_message.encode(self.encoding)
        _crc_16 = self.gen_crc16(_payload)
        
        _return = self.preamble + _payload + bytearray([(_crc_16 >> 8) & 0xFF, _crc_16 & 0xFF])
        
        return _return
          
    def gen_crc16(self, a_data):
        size = len(a_data)
        data_index = 0
        out = 0;  # 16b
        bits_read = 0
        bit_flag = 0

        while (size > 0):
            bit_flag = out >> 15;

            # Get next bit: */
            out <<= 1
            out |= ((a_data[data_index] >> bits_read) & 1)  # item a) work from the least significant bits
            out = out & 0xFFFF

            # Increment bit counter: */
            bits_read += 1;
            if (bits_read > 7):
                bits_read = 0
                data_index += 1
                size -= 1

            # Cycle check: */
            if (bit_flag):
                out ^= 0x8005
                out &= 0xFFFF

        # item b) "push out" the last 16 bits
        for i in range(16):
            bit_flag = out >> 15
            out <<= 1
            out = out & 0xFFFF

            if (bit_flag):
                out ^= 0x8005
                out &= 0xFFFF

        # item c) reverse the bits
        crc = 0
        i = 0x8000
        j = 0x0001
        
        while (i != 0):
            if (i & out):
                crc |= j

            i >>= 1
            j <<= 1

        return crc
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='DB Auto Updater')
    parser.add_argument("-ip", help="IP address to communicate to")
    parser.add_argument("-timeout", help="Set a timeout for the communications with the board", type=float, default=1.0)
    
    parser.add_argument('-debuglog', action='store_true', help='turn logging output to debug')
    
    args = parser.parse_args()
    
    # set up debug
    if args.debuglog:
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s [ %(levelname)-8s ] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
        logger.debug("output debug --Active--")
    else:
        logging.basicConfig(level=logging.WARNING,
                            format='%(asctime)s [ %(levelname)-8s ] %(message)s',
                            datefmt='%Y-%m-%d %H:%M:%S')
                            
    
    comms = None
    
    #if args.ip is not None:
    #    logger.info("Setting up Com IP: {}".format(args.ip))
    #    comms = IPComms(ip_address=args.ip)
    
    # Connect
    #logger.info("Connecting...")
    #connect_success = comms.connect()
    #print("connect_success:", connect_success)
    
    #if not connect_success:
    #    logger.error("Failed to connect...")
    #    sys.exit()
    #else:
    #    logger.info("We're on the network")
    
    #comms.close_socket()
    
    auto_updater = AutoUpdater(ip_address=args.ip, timeout=args.timeout)
    
    auto_updater.run()
    
    