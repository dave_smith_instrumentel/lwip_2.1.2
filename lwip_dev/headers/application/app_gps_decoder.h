//****************************************************************************
//
// Guard
//
//****************************************************************************


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define M_PI   3.14159265358979323846264338327950288

#define TIME_INDEX_POS                                      0
#define VOID_INDEX_POS                                      1
#define LAT_INDEX_POS                                       2
#define NORTH_SOUTH_INDEX_POS                               3
#define LONG_INDEX_POS                                      4
#define EAST_WEST_INDEX_POS                                 5
#define SPEED_INDEX_POS                                     6
#define COURSE_INDEX_POS                                    7
#define DATE_INDEX_POS                                      8
#define MAGNETIC_VARIATION_INDEX_POS                        9
#define CHECKSUM_INDEX_POS                                  11


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct Nmea
{
    bool arrived;
    char message[100];
    uint32_t message_len;
    char date[7];
    char time[11];
    bool valid;
    float latitude;
    float longitude;
    float speed;
    float course;
    uint32_t checksum;
} Nmea_t;



//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern Nmea_t g_gps_data;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

bool parse_gps_data(char* a_gps_data, uint32_t a_data_len);
void log_gps_data(char* a_gps_data, uint32_t a_data_len);
bool inside_rectangle(float a_long, float a_lat, ActivationZone_t a_rectangle);
bool measure_distance(float a_long, float a_lat, uint32_t a_radius);

