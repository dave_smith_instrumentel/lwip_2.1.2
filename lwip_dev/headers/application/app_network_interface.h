//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __INST_INTERFACE_H__
#define __INST_INTERFACE_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define CS_LEN                      2
#define INST_PREAMBLE_LEN           5
#define PREAMBLE                    "$INST"



#define XMLPARAM_ERR				0
#define	XMLPARAM_PARAMS_BEGIN 		1
#define XMLPARAM_PARAMS_END			2
#define XMLPARAM_GENERAL_BEGIN		3
#define XMLPARAM_GENERAL_END		4
#define XMLPARAM_GENERAL_VALUE		5
#define XMLPARAM_TYPE				6
#define	XMLPARAM_MAC				7
#define	XMLPARAM_FW					8
#define	XMLPARAM_TIME				9
#define	XMLPARAM_REF				10
#define	XMLPARAM_OPENREF			11
#define	XMLPARAM_CLOSEREF			12
#define	XMLPARAM_ACT				13
#define	XMLPARAM_DEV				14
#define XMLPARAM_LOCATION 			15
#define	XMLPARAM_STARTDETAIL		16
#define	XMLPARAM_ENDDETAIL			17
#define XMLPARAM_POSITION			18
#define XMLPARAM_IP					19
#define XMLPARAM_GW					20
#define XMLPARAM_SN					21
#define XMLPARAM_RF					22
#define XMLPARAM_DAY				23
#define XMLPARAM_NODAY				24
#define XMLPARAM_DAY_SUMMARY		25
#define XMLPARAM_IODATA				27
#define XMLPARAM_TAGS				28
#define XMLPARAM_REBOOT				29
#define XMLPARAM_TEMPERATURE		31
#define XMLPARAM_ARMTEMPERATURE		32
#define XMLRESPONSE_BEGIN		    33
#define	XMLRESPONSE_END			    34
#define XMLRESPONSE_NAME		    35
#define XMLPARAM_OPENCOUNT			36
#define XMLPARAM_CLOSECOUNT			37
#define XMLPARAM_BINFILE			38
#define XMLPARAM_SERVERIP			39
#define XMLPARAM_REDTHRESH			40
#define XMLPARAM_AMBERTHRESH		41
#define XMLPARAM_NOTAGS				42
#define XMLPARAM_IOTEST				43
#define XMLPARAM_SERIAL				44
#define XMLPARAM_FLAGS				45

#define TCP_PREAMBLE_FAIL           -1
#define TCP_CHECKSUM_FAIL           -2
#define TCP_COMMANDS_FAIL           -3
#define TCP_COMMAND_NOT_FOUND       -4
#define TCP_KILL                    -5


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef void (*command_handler_call)(char* a_string, uint32_t a_len, struct API_state* a_api);


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern uint8_t g_bin_commands[2048];
extern uint32_t g_bin_commands_to_tx;
extern char g_db_flash_commands[1024];

extern uint32_t g_bin_offset;
extern uint32_t g_b64_len;
extern char* g_bin_file;


extern uint32_t g_read_offest;
extern uint32_t g_block_to_read;

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

// Parse XML strings we have received
void parse_udp_command(RingBuffer * a_rb);
int32_t parse_tcp_command(char *a_request, int32_t a_actual_len, struct API_state *a_api);


#endif
