#ifndef __PEPWAVE_GPS_H__
#define __PEPWAVE_GPS_H__

/*****************************************************************************
*
*** Includes
*
*****************************************************************************/

#include <stdint.h>
#include <stdbool.h>


/*****************************************************************************
*
*** Variable Declarations
*
*****************************************************************************/

extern volatile bool g_gps_data_ready;
extern volatile int gLastGPSLen;
   

/*****************************************************************************
*
*** Function Declarations
*
*****************************************************************************/

void PWGPS_init( void );
void PWGPS_onPacket(char * data, uint32_t len );
int32_t  PWGPS_getLastMessage(char* datain, int32_t lenin );

#endif
