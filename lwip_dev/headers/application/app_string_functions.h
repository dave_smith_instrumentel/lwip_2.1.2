//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __STRING_FUNCTIONS_H__
#define __STRING_FUNCTIONS_H__

//****************************************************************************
//
// Defines
//
//****************************************************************************


//****************************************************************************
//
// Typedefs
//
//****************************************************************************


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

unsigned long Inst_Otherstring2hex (char *str);
unsigned long Inst_string2hex(char str[3]);
unsigned long Inst_char2hex (char hex);

char * Inst_itoa(int in);
char * Inst_uitoa(int in);
char * Inst_htoa(char input);
int Inst_atoi(const char *s);
void strn2(char *dest, int maxLen, char *in1, char *in2);
void strn3(char *dest, int maxLen, char *in1, char *in2, char *in3);
void strn4(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4);
void strn5(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4, char *in5);
void strn6(char *dest, int maxLen, char *in1, char *in2, char *in3, char *in4, char *in5, char *in6);
int Inst_StringToInt(char *string, int length);	  
unsigned long Inst_Otherstring2hex (char *str);

int Inst_CharToInt(char charater);
char Inst_IntToChar(unsigned long number);
double Inst_StringToDouble(char *string, int length);
char* Inst_Int2hexstring(unsigned long var);
unsigned long Inst_Long2String(char *data, unsigned long Number, unsigned long length, unsigned long PaddingFlag);
unsigned long Inst_Double2String(char *data, double Number, unsigned long IntegerLength, unsigned long DecimalLength);

#endif
