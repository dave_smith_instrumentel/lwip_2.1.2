//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __FW70_UARTMOBO_H__
#define __FW70_UARTMOBO_H__

#ifdef	__cplusplus
extern "C" {
#endif

//****************************************************************************
//
// Defines
//
//****************************************************************************

#define CMD_IDENTIFY            0x01
#define CMD_ECHO                0x02
#define CMD_POWERUPMESSAGE      0x03
#define CMD_SETCONFIG           0x04
#define CMD_SAMPLEDATA          0x05
#define CMD_FIRMWAREUPDATE      0x06
#define CMD_FLASHOP             0x08
    
#define CMD_HS_DATAREADY        0x20 // Mark as Data ready
#define CMD_HS_MOREDATA         0x21 // MB ready to accept data
#define CMD_HS_DATATX           0x22 // DB sending data down
#define CMD_HS_DATACOMPLETE     0x23 // DB has no more data to TX, close off capture log
#define CMD_HS_SOFTWARETRIGGER  0x24 // Software trigger to sample data out of ram

#define RSP_IDENTIFY            0x81
#define RSP_ECHO                0x82
#define RSP_POWERUPMESSAGE      0x83
#define RSP_SETCONFIG           0x84
#define RSP_SAMPLEDATA          0x85
#define RSP_FIRMWAREUPDATE      0x86

#define ECHOLEN                 30
#define POWERUPLEN              30
#define IDENTIFYLEN             32
#define SETCONFIGLEN            30


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct UComsHSReady
{
    uint32_t bytes_to_expect;
    uint32_t trigger_flags;
    uint32_t trigger_timer_event;
    uint32_t trigger_timer_start;
    uint32_t trigger_timer_end;
    uint32_t memory_start;
    uint32_t memory_end;
    uint32_t memory_total;
    
} UComsHSReady_t;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void uartmobo_init(void);
void uartmobodriv_send(const uint8_t * buff,  uint32_t len, void * ud);
uint32_t uartmobo_output_stream_init(uint8_t a_cmd, uint16_t data_len);
uint32_t uartmobo_output_stream_data(uint8_t * a_data, uint16_t a_data_len);
void uartmobo_powerupmessage( void );

void uartcomms_send_HS_Dataready( UComsHSReady_t * a_data );
void uartcomms_send_sampledata( uint8_t * a_buff, uint32_t a_bufflen );
void uartcomms_send_HS_DataComplete( UComsHSReady_t * a_data );
void uartmobo_SetConfigStatic(uint8_t * buff, uint32_t len);

#ifdef	__cplusplus
}
#endif


#endif
