#ifndef HARDWARE_H
#define HARDWARE_H

#include "inc/hw_memmap.h" 
#include "driverlib/gpio.h"

#define PROCESSOR_CLOCK 80000000    //Workaround for Tivaware 2.1 bug in SysCtlClockSet()

#define UNIX_TIME_CHECK 1293753600u // represents a "recent" time to ensure clock is at least vaguely set

//Interupt priorities upper 3 bits 0x00 = Highest, 0xE0 = Lowest
#define INT_PRIORITY_UART       0xE0
#define INT_PRIORITY_ADCTIMER   0x00
#define INT_PRIORITY_DIGIINPUTS   0x20

// flashing lights definition
#define LED_GREEN           GPIO_PIN_5 
#define LED_RED             GPIO_PIN_3 

// digitial inputs
#define DIG_TRIG_BASE       GPIO_PORTH_BASE
#define DIG1_PIN            GPIO_PIN_3
#define DIG2_PIN            GPIO_PIN_4

// UARTS
#define DEBUG_UART_PERIPH 			SYSCTL_PERIPH_UART0
#define DEBUG_UART_PERIPH_GPIO 	    SYSCTL_PERIPH_GPIOA
#define DEBUG_UART_PORT_BASE 		GPIO_PORTA_BASE
#define DEBUG_UART_BASE 			UART0_BASE
#define DEBUG_UART_TX_PIN 			GPIO_PIN_1
#define DEBUG_UART_RX_PIN 			GPIO_PIN_0

#define ARMTOARM_UART_PERIPH 		SYSCTL_PERIPH_UART2
#define ARMTOARM_UART_PERIPH_GPIO 	SYSCTL_PERIPH_GPIOG
#define ARMTOARM_UART_PORT_BASE 	GPIO_PORTG_BASE
#define ARMTOARM_UART_BASE 			UART2_BASE
#define ARMTOARM_UART_TX_PIN 		GPIO_PIN_1
#define ARMTOARM_UART_RX_PIN 		GPIO_PIN_0

// I2C
#define I2C_PERIPH 			        SYSCTL_PERIPH_I2C0
#define I2C_PERIPH_GPIO 	        SYSCTL_PERIPH_GPIOB
#define I2C_PORT_BASE 		        GPIO_PORTB_BASE
#define I2C_CLK_PIN 			    GPIO_PIN_2
#define I2C_DATA_PIN 			    GPIO_PIN_3

/* Peripheral definitions for EK-LM3S6965 board */
// SSI port
#define SDC_SSI_BASE                SSI0_BASE
#define SDC_SSI_SYSCTL_PERIPH       SYSCTL_PERIPH_SSI0

// GPIO for SSI pins
#define SDC_GPIO_PORT_BASE          GPIO_PORTA_BASE
#define SDC_GPIO_SYSCTL_PERIPH      SYSCTL_PERIPH_GPIOA
#define SDC_SSI_CLK                 GPIO_PIN_2
#define SDC_SSI_TX                  GPIO_PIN_5
#define SDC_SSI_RX                  GPIO_PIN_4
#define SDC_SSI_FSS                 GPIO_PIN_3
#define SDC_SSI_PINS                (SDC_SSI_TX | SDC_SSI_RX | SDC_SSI_CLK)

#define SDC_CS_GPIO_PORT_BASE      GPIO_PORTA_BASE
#define SDC_CS_GPIO_SYSCTL_PERIPH  SYSCTL_PERIPH_GPIOA
#define SDC_CS                     GPIO_PIN_6

#endif
