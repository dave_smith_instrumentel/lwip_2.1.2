/*****************************************************************************
*
*** Include
*
*****************************************************************************/

// Data format numbers
#define DATA_FORMAT_BASE                                                000
#define DATA_DB_CONFIG                                                  001
#define DATA_GPS                                                        002
#define DATA_DVRS                                                       003

// Logging
#define HUB_BEHAVIOUR_BASE                                              100
#define HUB_ON_MOTHERBOARD                                              101
#define HUB_ON_DAUGHTERBOARD                                            102
#define HUB_DB_CONFIG_ACK                                               103


// Warnings
#define WARNING_BASE                                                    800


// Errors
#define ERROR_BASE                                                      900
#define WATCHDOG_TRIPPED                                                901
