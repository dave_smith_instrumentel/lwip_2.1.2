//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __MAIN_H__
#define __MAIN_H__

//****************************************************************************
//
// Defines
//
//****************************************************************************

#define RSP_CAPTURE_DATA_START                      0x20
#define RSP_CAPTURE_DATA_NEXT                       0x21
#define RSP_CAPTURE_DATA_CONT                       0x22
#define RSP_CAPTURE_DATA_FINISH                     0x23
#define CMD_SOFT_TRIGGER                            0x24
#define RSP_CAPTURE_DATA_COMPLETE                   0xA3
#define RSP_IDENTIFY                                0x81
#define RSP_ECHO                                    0x82
#define RSP_POWERUPMESSAGE                          0x83
#define RSP_SETCONFIG                               0x84
#define RSP_SAMPLEDATA                              0x85
#define RSP_FIRMWAREUPDATE                          0x86
#define RSP_FLASHOP                                 0x88

#define CAPTURE_PACKET_SIZE                         1024

//#define MANUALLY_OVERWRITE_CONF 


//****************************************************************************
//
// Typedefs
//
//****************************************************************************




//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern uint32_t g_sd_card_fail;
extern bool g_safe_mode;
extern bool g_liveview;
extern bool g_ddu_liveview;
extern bool g_soft_trigger;
extern bool g_hard_trigger;
extern uint8_t g_trigger_params[200];
extern uint32_t g_trig_len;
extern uint32_t g_sys_clk;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void slave_reset(void);


#endif
