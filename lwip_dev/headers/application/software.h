// -----------------------
//
//  This file will contain all compile time software configurable
// items such as CRC enable and buffer sizes
//
// -----------------------

#ifndef __SOFTWARE_H__
#define __SOFTWARE_H__

#include "uart_codec.h"

// Defines for setting up the system clock.
//! \brief Systick Seconds.
#define SYSTICKHZ               100
//! \brief Systick milli-seconds.
#define SYSTICKMS               (1000 / SYSTICKHZ)
//! \brief Systick micro-seconds.
#define SYSTICKUS               (1000000 / SYSTICKHZ)
//! \brief Systick nano-seconds.
#define SYSTICKNS               (1000000000 / SYSTICKHZ)


// Interrupt priority definitions.  
// The top 3 bits of these values are significant with lower values indicating higher priority interrupts.
#define UART_INT_PRIORITY		0x80
#define SYSTICK_INT_PRIORITY    0xC0  
#define ETHERNET_INT_PRIORITY   0xE0
#define PENDSV_INT_PRIORITY		0xE0

#define SYSTICKPERIODUS			100


#define UARTMOBO_DECODE_BUFFERSIZE 1000
#define UARTMOBO_BAUD 2000000
#define UARTMOBO_DECODERFEATURES (UARTDECODER_FLAGS_CRCENABLED | UARTDECODER_FLAGS_TWOBYTLELEN)
#define UARTMOBO_ENCODERFEATURES (UARTENCODER_FLAGS_CRCENABLED | UARTENCODER_FLAGS_TWOBYTLELEN)

#define CONFIG_MAXSIGNALS  32
#define CONFIG_MAXSEQUENCE  8
#define CONFIG_MAXMISC 8

#endif 

