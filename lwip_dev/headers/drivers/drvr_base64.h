/*

Auto Download Reboot

A Simple module that will help count down and reset for when the user wants to automatically reboot when a
download is started. The thinking behind is that if the downloading locksup this recovers it. A pseudo 
watchdog timer. But worse.  

*/

#ifndef __AUTODOWNLOADREBOOT_H__
#define __AUTODOWNLOADREBOOT_H__
#include <stdint.h>

#ifndef REBOOTCALL
#define REBOOTCALL 0
#endif

extern char gB64Data[2048];

int b64_Decoder(char* inStr, long inLen, char* outBin, int binLen);
void b64_Encoder(char* inbin, long binLen, char* outStr, int strLen);


#endif
