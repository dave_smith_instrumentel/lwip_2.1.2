/*
    Bit shuffle is for compacting 12 bit ADC samples together for more
    effecient storage.
    
    Presently 8 Samples (12*8 = 96 bits) into 12 Bytes (12*8 = 96 bits)
*/

//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __BIT_SHUFFLE_H__
#define __BIT_SHUFFLE_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define BITSHUFFLE_SAMPLECHUNKSIZE 8


//****************************************************************************
//
// Typedefs
//
//****************************************************************************


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

int32_t bitshuffle_12bit(uint16_t * a_samples, uint8_t * a_output, uint32_t a_sample_count);


#endif
