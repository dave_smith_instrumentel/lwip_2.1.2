//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __DRVR_BL_EMAC_H__
#define __DRVR_BL_EMAC_H__

#include <stdint.h>    
#include <stddef.h>

#ifdef	__cplusplus
extern "C" {
#endif

//****************************************************************************
//
// Defines
//
//****************************************************************************

#define MAGIC_PACKET_PORT               9
#define MAGIC_PACKET_HEADER_LEN         6
#define MAGIC_PACKET_MAC_REP            4
#define MAGIC_PACKET_MAC_LEN            6
#define MAGIC_PACKET_LEN                (MAGIC_PACKET_HEADER_LEN + (MAGIC_PACKET_MAC_REP * MAGIC_PACKET_MAC_LEN))
    
#define MAGIC_PACKET_MARKER             0xAA

// The BOOTP commands    
#define BOOTP_REQUEST                   1
#define BOOP_REPLY                      2

// The TFTP commands
#define TFTP_READ_REQUEST               1
#define TFTP_WRITE_REQUEST              2
#define TFTP_DATA                       3
#define TFTP_ACK                        4
#define TFTP_ERROR                      5

// UDP ports used by BOOTP protocol
#define BOOTP_HOST_PORT                 67
#define BOOTP_CLIENT_PORT               68

// UDP port for the TFTP server
#define TFTP_PORT                       69
//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef void(*SoftwareUpdateRequested) (void);

typedef struct BOOTPPacket
{
    uint8_t operation;                          // Operation identifier; 1 is for request, 2 is for reply
    uint8_t hardware_type;                      // The hardware type used for updating. In this case it will be Ethernet
    uint8_t hardware_addr_len;                  // The hardware address length. For Ethernet, this is the MAC address
    uint8_t hops_count;                         // Hop count is used by gateways for cross-gateway booting
    uint8_t transaction_id;                     // The transaction ID
    uint16_t seconds_elapsed;                   // Number of seconds elapsed since the client started trying to boot
    uint16_t bootp_flags;                       // The BOOTP flags
    uint32_t known_client_ip_addr;              // The client's IP address if it is known
    uint32_t assigned_client_ip_addr;           // The client's IP address as assigned by the BOOTP server
    uint32_t tftp_server_ip_addr;               // The TFTP server's IP address
    uint32_t gateway_ip_addr;                   // The gateway IP address if booting cross-gateway
    uint8_t hub_mac_addr;                       // The hardware's/hub's MAC address
    char server_name;                           // The name of the server that handles the BOOTP requests
    char firmware_name;                         // The name of the firmware to be loaded via TFTP
} BOOTPPacket;

//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

extern void software_update_init(SoftwareUpdateRequested pfnCallback);
extern void software_update_begin(uint32_t system_clock);

#ifdef	__cplusplus
}
#endif

#endif // (end of __DRVR_BL_EMAC_H__)

