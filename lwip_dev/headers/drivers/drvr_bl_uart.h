//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __BL_UART_H__
#define __BL_UART_H__

#include <stdint.h>
#include <stdbool.h>

//****************************************************************************
//
// Defines
//
//****************************************************************************
#define BLUART_APP_START_ADDRESS            0x00000
#define TRANSFER_SIZE                       252
#define RB_BLUART_RX_BUFF_LEN               7
#define BL_FLASH_READ_SIZE                  63 // TRANSFER_SIZE/BYTES_PER_WORD = 252/4 = 63
#define FLASH_BIN_SIZE_TO_READ              4

#define BLOCK_MB_FLASH_SIZE_DB_BIN          0x80000

#define CMD_DB_BUFF_LEN                     50

#define BLUART_STATE_IDLE                   1 // Idle state
#define BLUART_STATE_FIRMWARE_SELSCTION     2 // Select the firmware to be downloaded on the DB
#define BLUART_STATE_PING                   3 // Ping the DB
#define BLUART_STATE_DOWNLOAD_INIT          4 // Initialise the download process
#define BLUART_STATE_STATUS_REQUEST         5 // Request the status of the DB by sending the status command (Type-1 + Type-2 response)
#define BLUART_STATE_DOWNLOAD_DATA          6 // Loads the new firmware on the DB
#define BLUART_STATE_DOWNLOAD_FIN           7 // Indicates the end of the FW update
#define BLUART_STATE_DOWNLOAD_ERR           8 // Error state

#define ACK                                 0xCC
#define NAK                                 0x33

// Type-1 Command Packets 
#define COMMAND_PING                        0x20
#define COMMAND_DOWNLOAD                    0x21
#define COMMAND_RUN                         0x22
#define COMMAND_GET_STATUS                  0x23
#define COMMAND_SEND_DATA                   0x24
#define COMMAND_RESET                       0x25

// Type-2 Response Packets
#define COMMAND_INVALID_DATA_PACKET         0x39
#define COMMAND_RET_SUCCESS                 0x40
#define COMMAND_RET_UNKNOWN_CMD             0x41
#define COMMAND_RET_INVALID_CMD             0x42
#define COMMAND_RET_INVALID_ADR             0x43
#define COMMAND_RET_FLASH_FAIL              0x44
#define COMMAND_RET_CRC_FAIL                0x45

#define BL_RX_DELAY_SHORT                   500000        // 2 seconds
#define BL_RX_DELAY_MEDIUM                  5000000      // 10 seconds
#define BL_RX_DELAY_LONG                    100000000       // 20 seconds (105500000)

// Command we receive over TCP
#define BL_CMD_NONE                         0
#define BL_CMD_START                        1
#define BL_CMD_MODE                         2
#define BL_CMD_VERSION                      3

// Required for safety checks
#define VERSION_NUMBER_LEN                  50
            
//****************************************************************************
//
// Typedefs
//
//****************************************************************************
typedef struct BLUARTMaster
{
    uint32_t bl_state;                              // Bootloader current state
    uint32_t bl_state_prev;                         // Bootloader previous state
    uint32_t timeout_timer;                         // Bootloader timer
    RingBuffer rb_uart_rx;                          // Ring buffer used to record the responses sent by the DB during the update
    uint8_t uart_rx_buff[RB_BLUART_RX_BUFF_LEN];    // Actual buffer keeping the responsing or other relevan data during the update
    unsigned char bl_uart_rx[7];                    // Buffer at which the responses are copied out 
    //bool download_data_complete;                    // Flag indicating that the update process has been completed
} BLUARTMaster_t;

//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

// Global Variables
extern uint32_t g_bl_command;

extern bool g_update_flag;
extern bool g_flash_busy;

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

// Bootloader Programmer Function Prototypes
void bluart_init( void );
void bluart_start( void );
void bluart_service_routine( void );

void bluart_incoming_packet( unsigned long a_byte);
bool bluart_ethcmd_updatedb ( void );


#endif

