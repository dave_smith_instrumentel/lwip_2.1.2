//****************************************************************************
//
// FW23/75 Debug
//  Debug mainly contains output from FW23/75 code
//  Will need to be called from drivers/drvr_debug_sw.h for FW70 debug calls.
// This was to stop renaming "drivers/drvr_debug_sw.h" includes in all FW70 files!
//
//****************************************************************************


//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef _DEBUG_SW_H
#define _DEBUG_SW_H


//****************************************************************************
//
// Defines
//
//****************************************************************************

// Master Debug Enable
#define DEBUG_ENABLE

#define DEBUG_GENERAL		            0x00000001
#define DEBUG_UDP   			    	0x00000002
#define DEBUG_TCP        				0x00000004
#define DEBUG_RTC        				0x00000008
#define DEBUG_MICRO_SD     			    0x00000010
#define DEBUG_FLASH        			    0x00000020
#define DEBUG_SYS_CTRL     			    0x00000040
#define DEBUG_HUB_CLIENT   			    0x00000080
#define DEBUG_BOOTLOADER   			    0x00000100
#define DEBUG_WATCHDOG                  0x00000200
#define DEBUG_RING_BUFFER               0x00000400
#define DEBUG_LWIP                      0x00000800
#define DEBUG_MMC                       0x00001000
#define DEBUG_SYS                       0x00002000
#define DEBUG_CODEC                     0x00004000
#define DEBUG_GPS                       0x00008000
#define DEBUG_FW70                      0x00010000
#define DEBUG_RFID_DECODER			    0x00020000
#define DEBUG_RFID_DDU_SM				0x00040000
#define DEBUG_CALIB_DATA				0x00080000
#define DEBUG_DIGITAL				    0x00100000
#define DEBUG_PULSE 			        0x00200000
#define DEBUG_CONFIG_LOAD		        0x00400000
#define DEBUG_TRIGGERS			        0x00800000
#define DEBUG_HS_CAPTURE		        0x01000000
#define DEBUG_ADC   			        0x02000000
#define DEBUG_BUFFMAN			        0x04000000
#define DEBUG_HW_LAYER			        0x08000000
#define DEBUG_DRVR  			        0x10000000
#define DEBUG_APP			            0x20000000
#define DEBUG_BLANK9			        0x40000000
#define DEBUG_BLANK10			        0x80000000
#define DEBUG_ALL                       0xFFFFFFFF

#define debugf(x, ...) do{debug_output(x, __VA_ARGS__);}while(0)

//#define DEBUGLED_LED1_MODE DEBUGLED_MODE_SAMPLETAKE
//#define DEBUGLED_LED1_MODE DEBUGLED_MODE_SEQTIMER_LEADINC_A
//#define DEBUGLED_LED1_MODE DEBUGLED_MODE_PULSECOUNT_GPIOINT
//#define DEBUGLED_LED1_MODE DEBUGLED_MODE_SYSTICK_SEQTIMERINC
#define DEBUGLED_LED1_MODE DEBUGLED_MODE_DEBOUNCE_COUNT

#define DEBUGLED_LED2_MODE DEBUGLED_MODE_SAMPLETAKE 
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_SAMPLETAKE
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_UART_RXINT
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_SAMPLEDURATION
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_SEQTIMER_LEADINC_B
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_SAMPLEDURATIONSEQIDSPECIFIC 
//#define DEBUGLED_LED2_MODE DEBUGLED_MODE_MAINNOSAMPLES

#define DEBUGLED_LED3_MODE DEBUGLED_MODE_MAINNOSAMPLES // MIGTH CHANGE OR REMOVED

// Sepcific debug led to toggle on
#define DEBUG_LEADINC_SEQIDA 1
#define DEBUG_LEADINC_SEQIDB 2


#define DEBUGLED_ON 1
#define DEBUGLED_OFF 2
#define DEBUGLED_TOGGLE 3


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef enum Led_Colours
{
    RED,
    AMBER, 
    GREEN,
    LED_OFF
} Led_Colours_t;

typedef enum Led_Flashing
{
    FAST,
    SLOW, 
    OFF
} Led_Flashing_t;


enum DebugLedMode 
    {
        DEBUGLED_MODE_NONE=0,
        DEBUGLED_MODE_SAMPLETAKE,
        DEBUGLED_MODE_CONFIGWAIT,
        DEBUGLED_MODE_PULSECOUNT_TO,
        DEBUGLED_MODE_UART_RXINT,
        DEBUGLED_MODE_WDG_FED,
        DEBUGLED_MODE_SAMPLEDURATION,
        DEBUGLED_MODE_SEQTIMER_LEADINC_A,
        DEBUGLED_MODE_SEQTIMER_LEADINC_B,
        DEBUGLED_MODE_SAMPLEDURATIONSEQIDSPECIFIC,
        DEBUGLED_MODE_MAINNOSAMPLES,
        DEBUGLED_MODE_PULSECOUNT_GPIOINT,
        DEBUGLED_MODE_SYSTICK_SEQTIMERINC,
        DEBUGLED_MODE_DEBOUNCE_COUNT,

        // FW85 DEBUG MODES ADD ON
        DEBUGLED_MODE_TIMER_INT,
        DEBUGLED_MODE_SYSTICK_TICK,
        DEBUGLED_MODE_ETHMAC_RXPACKET,
        DEBUGLED_MODE_SPIHS_DEQUEUE_CH1,
        DEBUGLED_MODE_SPIHS_DEQUEUE_CH2,
        DEBUGLED_MODE_SPIHS_ISTXING_CH1,
        DEBUGLED_MODE_SPIHS_ISTXING_CH2,
        DEBUGLED_MODE_SPIHS_QUEUEFULL,
        DEBUGLED_MODE_SPIHS_DEQUEUEFUALT,
        DEBUGLED_MODE_ADC1_INTSEQ0,
        DEBUGLED_MODE_ADC1_INTSEQ0_PING,
        DEBUGLED_MODE_ADC1_INTSEQ0_PONG,
        DEBUGLED_MODE_BUFFMAN_ADCPOP,
        DEBUGLED_MODE_BUFFMAN_ADCPUSH,
        DEBUGLED_MODE_ADC1_DMAOVERFLOW,
        DEBUGLED_MODE_BUFFMAN_SPIPUSH,
        DEBUGLED_MODE_BUFFMAN_SPIPOP,
        DEBUGLED_MODE_ADS8584_STIMER,
        DEBUGLED_MODE_BUFFMAN_EXTNADCPUSH,
        DEBUGLED_MODE_BUFFMAN_EXTNADCPOP,
        DEBUGLED_MODE_ADC2_INTSEQ0,
        DEBUGLED_MODE_ADC2_INTSEQ0_PING,
        DEBUGLED_MODE_ADC2_INTSEQ0_PONG,
        DEBUGLED_MODE_ADC2_DMAOVERFLOW,
        DEBUGLED_MODE_MMA8652FC_STIMER,
        DEBUGLED_MODE_BUFFMAN_ACCELPUSH,
        DEBUGLED_MODE_BUFFMAN_ACCELPOP,
        DEBUGLED_MODE_RMS_CALCSQRT,
        DEBUGLED_MODE_RMS_PROCCESS,
        DEBUGLED_MODE_BITSHUFFLE_12BITFUNC,
        DEBUGLED_MODE_EXTSRAM_WRITEBUFFER_WITH_CACHE
    };



//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern uint32_t debug_sources;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void debug_init(uint32_t source);
void debug_udp(bool a_on);
void debug_output(unsigned long source, char *data, ...);
void debug_output_len (char *data, uint32_t len);
void toggle_debug_pin(uint32_t a_pin, bool on);
void toggle_debug_pin2(void);
void toggle_debug_pin3(void);
void set_led_pin(uint32_t a_pin, bool a_dir);
void set_led_colour(Led_Colours_t a_colour, Led_Flashing_t a_flashing);
void led_tick(uint32_t a_tick);

void debug_format(char *format, ...);
void debug_setup( void );
void debug_welcomeMessage( void );
void debug_flush( void );
char * debug_byteToASCII(uint8_t a_byte);
void debug_output_raw (const char *pcString, ...);

void debug_led(uint32_t a_mode, uint32_t a_offontoggle);


#endif
