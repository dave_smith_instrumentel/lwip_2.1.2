//
//  Fifo.c 
//  A simple push pop queue that will work in a First in First out system
//  Similiar to the way a ring buffer works but instead of read/write 
//  arbetry data. You add in buffers of data.
//
// See struct FifoEntry
//
//

//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __FIFO_H__
#define __FIFO_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct FifoEntry
{
    uint8_t * buffer;
    uint32_t bufflen;
} FifoEntry_t;

typedef struct FifoQueue
{
    volatile uint32_t  write_index;
    volatile uint32_t  read_index;
    volatile uint32_t  write_counter; // These values are for helping distingquishing 
    volatile uint32_t  read_counter;  //     when read == write is the queue empty or full?
    uint32_t  queue_size;
    FifoEntry_t * queue;
} FifoQueue_t;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void fifo_init(FifoQueue_t * a_queue, FifoEntry_t * a_entryBuffer, uint32_t a_entryBufferSize);
bool fifo_push(FifoQueue_t * a_queue, uint8_t * buffer,  uint32_t bufflen );
uint8_t * fifo_pop(FifoQueue_t * a_queue,  uint32_t * bufflen );
uint32_t fifo_countEmpty(FifoQueue_t * a_queue);
uint32_t fifo_count(FifoQueue_t * a_queue);

#endif
