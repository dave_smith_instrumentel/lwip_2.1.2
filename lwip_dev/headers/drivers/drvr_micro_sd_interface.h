//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __MICRO_SD_INTERFACE_H
#define __MICRO_SD_INTERFACE_H


//****************************************************************************
//
// Defines
//
//****************************************************************************


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef enum 
{
    IDLE,
    READING_SETUP,
    READING,
    WRITING,
    LISTING,
    NAVIGATE,
    FORMAT,
    DELETE,
    SPACE_AVAILABLE,
    DEBUG
} micro_sd_states;

typedef struct 
{
    bool file_end;  // Tell the TCP that the file end has been hit
    bool reading;   // Semaphore to block tcp accessing the ring buffer while it is being modified
    bool data_available;
    uint32_t read_file_index;
    uint32_t read_file_len;
    uint32_t bytes_read;
    uint32_t rb_end_index;
} ReadEvent;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

// Ring buffer objects
extern RingBuffer rb_write_to_sd;

extern Listing g_listing;

extern ReadEvent read_event;

extern char g_write_folder_path[10];
extern char g_write_file_path[10];

extern char g_read_folder_path[10];
extern char g_read_file_path[10];
extern uint32_t g_read_offset;
extern uint32_t g_read_length;

extern char g_delete_folder_path[10];

extern char sd_card_preamble_string[25];


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void build_sd_card_preamble(int16_t log_number, uint32_t len);
void service_micro_sd(void);
void add_micro_sd_event(micro_sd_states event_to_add);
void remove_micro_sd_event(micro_sd_states event_to_remove);
void remount_fs(void);
void build_write_path(bool init);
void build_sd_card_preamble(int16_t log_number, uint32_t len);


#endif // __MICRO_SD_INTERFACE_H
