//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __RING_BUFFER_H
#define __RING_BUFFER_H


//****************************************************************************
//
// Defines
//
//****************************************************************************

static const int32_t MAXBUFFERSIZE = 4096;


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct { 
    int32_t total_size;
    int32_t read_index;
    int32_t write_index;
    size_t step_size;
    uint8_t * pBuf;
    bool buffer_full;
} RingBuffer;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************




//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void RingBuffer_init( RingBuffer * rb, void * pBuf, int32_t bufSize, size_t step_size);
void RingBuffer_Reset(RingBuffer * rb);
int32_t RingBuffer_Count( RingBuffer * rb );
int32_t RingBuffer_Space( RingBuffer * rb );
void * RingBuffer_GetNextRead( RingBuffer * rb );
void RingBuffer_MarkNextRead( RingBuffer * rb );
void * RingBuffer_GetNextWrite( RingBuffer * rb );
void RingBuffer_MarkNextWrite( RingBuffer * rb );
bool RingBuffer_CopyIn( RingBuffer * rb, uint8_t * pBuf, uint32_t len );
uint32_t RingBuffer_CopyOut( RingBuffer * rb, uint8_t * pBuf, uint32_t len );
void debug_ring_buffer(RingBuffer * rb);

void write_update(RingBuffer * rb, uint32_t len);


#endif
