//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __INST_ENET_API_H_
#define __INST_ENET_API_H_


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define API_MAX_NO_PARAMETERS 50

#define COMMAND_TRUE					-2	
#define COMMAND_FALSE					-1

#define TXMODE_NONE						1
#define TXMODE_CHUNKTRANSFER			3

#define NETWORK_VPN 1 
#define NETWORK_LOCAL 2
#define NETWORK_DEFAULT NETWORK_VPN

#define TCP_PAYLOAD_SIZE                1300


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

#ifndef APIDEF
#define APIDEF 

struct DaysToSend 
{
  	int status;
  	int no_files;
  	char path[40];
};
// API connection structure
struct API_state 
{
  	unsigned long State;
  	char *buf;
  	long int buf_len;
  	long int data_len;
  	long int left;
  	long int retries;
  	long int CurrentGetFileDay;
  	struct DaysToSend GetFilesDay;
};

#endif

typedef enum
{  
    COMMAND_DEFAULT,
    COMMAND_REBOOT,
    COMMAND_FORBOOT,
    COMMAND_READFILE,
    COMMAND_READFILE_BUFFER
} tcp_commands;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern volatile int gFileReadChunkSize;

extern unsigned long gNetworkSetting;

extern RingBuffer rb_tcp_tx;

extern bool g_do_not_tx;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

static err_t conn_accepted(void *arg, struct tcp_pcb *pcb, err_t err);
static void conn_error(void *arg, err_t err);
static err_t conn_poll(void *arg, struct tcp_pcb *pcb);
static err_t packet_received(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err);
static err_t packet_ack(void *arg, struct tcp_pcb *pcb, u16_t len);
void tcp_api_init(void);
void downSetChunkSize(int size);
void send_packets(struct tcp_pcb *pcb, struct API_state *API, RingBuffer* a_rb, int datalength);
//bool send_file_data(void *g_API, struct tcp_pcb *g_pcb);

// Uart stuff
int32_t Inst_PBufIntoArray(struct pbuf *p, int8_t* buff, int32_t bufferLen, int32_t* outputLen, struct pbuf **pEnd);


#endif
