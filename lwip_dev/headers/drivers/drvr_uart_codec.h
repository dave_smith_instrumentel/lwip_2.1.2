/* 
 * File:   uart_codec.h
 * Author: Lee Barker
 *
 * Created on 17 July 2017, 14:50
 */

#ifndef __UART_CODEC_H__
#define	__UART_CODEC_H__

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdint.h>    
#include <stddef.h>
#include <stdbool.h>

    
typedef struct UARTCRC16
{
    uint16_t out;
    int bits_read;
} UARTCRC16;

typedef void (*UARTEncoderCall)(const uint8_t * buff,  uint32_t len, void * ud);
typedef void (*UARTDecoderCall)(uint8_t * buff, uint32_t len, void * ud);

    
    
typedef struct UARTEncoder
{
    UARTEncoderCall sendcall;
    void * tag;
    uint32_t feature_flags;
    bool is_stream;
    uint16_t stream_bytes_rem;
    UARTCRC16 stream_crc;
    
} UARTEncoder;
    
typedef struct UARTDecoder
{
    uint32_t state;
    uint32_t length;
    uint32_t dataindex;
    uint32_t maxdatalen;
    uint8_t * data;
    uint32_t checksum;
    UARTDecoderCall call;
    void * tag;
    uint32_t feature_flags;
    uint32_t stat_procmsgs;
    uint32_t stat_crcfailures;
    uint32_t stat_preamblemiss;
    uint32_t stat_allrxbytes;
    bool data_available; // This is a FW75/23 Special
    
} UARTDecoder;

// ENCODER Config Flags
#define UARTENCODER_FLAGS_NONE          0
#define UARTENCODER_FLAGS_CRCENABLED    0x01
#define UARTENCODER_FLAGS_TWOBYTLELEN   0x02   

//state machine stages
// [0xA5,0X5A,<data length>,<tag ID>,<tag mode>,<packet count*>,<data>,<crc>]
#define UARTDECODER_STATE_SYNCBYTEONE     0
#define UARTDECODER_STATE_SYNCBYTETWO     1
#define UARTDECODER_STATE_MSGTYPE         2
#define UARTDECODER_STATE_PAYLOADSIZE     3
#define UARTDECODER_STATE_PAYLOADSIZE_TWO 4
//#define TAG_ID                            4
//#define TAG_MODE                          5
#define UARTDECODER_STATE_PACKETCOUNT     6
#define UARTDECODER_STATE_UARTDATA        7
#define UARTDECODER_STATE_COMMAND         8
#define UARTDECODER_STATE_CHECKSUMA       9
#define UARTDECODER_STATE_CHECKSUMB       10

#define UARTDECODER_NULL 0

#define UARTDECODER_FLAGS_NONE          0
#define UARTDECODER_FLAGS_CRCENABLED    0x01
#define UARTDECODER_FLAGS_TWOBYTLELEN   0x02

extern UARTEncoder g_UARTMoboEncoder;
extern UARTDecoder g_UARTMoboDecoder;

void UARTEncoder_Init( UARTEncoder * uen );
void UARTEncoder_SendPacket( UARTEncoder * uen , const uint8_t *data, uint32_t len );
void UARTEncoder_SetSendCall( UARTEncoder * uen, UARTEncoderCall call);

void UARTDecoder_Init( UARTDecoder * ud , uint8_t * buff, uint32_t bufflen);
void UARTDecoder_ResetStats( UARTDecoder * ud );
void UARTDecoder_DecodeMessage( UARTDecoder * ud , uint8_t newByte );
void UARTDecoder_SetMessageCall( UARTDecoder * ud, UARTDecoderCall call);
void UARTDecoder_DecodeMessageBuff( UARTDecoder * ud , uint8_t * pBuf, uint32_t len );

void UARTEncoder_StreamPacketInit(  UARTEncoder * uen , uint32_t total_len );
void UARTEncoder_StreamPacketData(  UARTEncoder * uen , const uint8_t *data, uint32_t len );


uint16_t UARTDecoder_gencrc16(const uint8_t *data, uint16_t size);

void Loopback_MBtoDB(const uint8_t *data, uint32_t len, void * ud);
void Loopback_DBtoMB(const uint8_t *data, uint32_t len, void * ud);
//void Loopback_DBtoMB_Call(uint8_t *data, uint32_t len, void * ud);

#ifdef	__cplusplus
}
#endif

#endif	/* __UART_CODEC_H__ */

