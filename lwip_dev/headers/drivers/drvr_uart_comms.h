
#ifndef __UART_COMMS_H__
#define __UART_COMMS_H__

#define CMD_IDENTIFY                    0x01
#define CMD_ECHO                        0x02
#define CMD_POWERUPMESSAGE              0x03
#define CMD_SETCONFIG                   0x04
#define CMD_SAMPLEDATA                  0x05  // was #define CMD_SAMPLESTART 
#define CMD_FIRMWAREUPDATE              0x06
#define CMD_RESET                       0x07   // 
#define CMD_FLASHOP                     0x08
#define CMD_SPIARMETH_READY             0x11
#define CMD_SPIARMADC_READY             0x12
#define CMD_SPIARMADC_TXCHECKDONE       0x13
#define CMD_SPIARMETH_RXCHECKPASS       0x14
#define CMD_ARMETH_FETCHSENSOR          0x0B // Special command to request GPS and or time from ethernet ARM
#define CMD_HS_DATAREADY                0x20 // Mark as Data ready
#define CMD_HS_MOREDATA                 0x21 // MB ready to accept data
#define CMD_HS_DATATX                   0x22 // DB sending data down
#define CMD_HS_DATACOMPLETE             0x23 // DB has no more data to TX, close off capture log
#define CMD_HS_SOFTWARETRIGGER          0x24 // Software trigger to sample data out of ram

#define RSP_BITFLAG 0x80

#define RSP_IDENTIFY 0x81
#define RSP_ECHO 0x82
#define RSP_POWERUPMESSAGE 0x83
#define RSP_SETCONFIG 0x84
#define RSP_SAMPLEDATA 0x85 // was #define RSP_SAMPLESTART 
#define RSP_FIRMWAREUPDATE 0x86
#define RSP_RESET 0x87
#define RSP_SPIARMETH_READY 0x91
#define RSP_SPIARMADC_READY 0x92
#define RSP_SPIARMADC_TXCHECKDONE 0x93
#define RSP_SPIARMETH_RXCHECKPASS 0x94
#define RSP_ARMETH_FETCHSENSOR 0x8B 
#define RSP_HS_DATACOMPLETE     0xA3 // DB has no more data to TX, close off capture log
#define RSP_HS_SOFTWARETRIGGER  0xA4 //  

#define RSP_UNKOWNCMD 0xC0


#define RSP_IDENT_VERMAXLEN 30
#define RSP_IDENT_PCBMAXLEN 10
#define RSP_IDENT_BUILDMAXLEN 30


/*
#TODO - Removed by DS as already defined in FW70_uartmobo.h

typedef struct UComsHSReady
{
    uint32_t bytes_to_expect;
    uint32_t trigger_flags;
    uint32_t trigger_timer_event;
    uint32_t trigger_timer_start;
    uint32_t trigger_timer_end;
    uint32_t memory_start;
    uint32_t memory_end;
    uint32_t memory_total;
    
} UComsHSReady_t;
*/

typedef void (*UARTCommsCallBasic)(uint8_t * buff, uint32_t len);

typedef struct UartCommsMaster 
{
    UARTCommsCallBasic fetchsensor_call;
} UartCommsMaster_t;

void uartcomms_init( void );
void uartcomms_demo1(void);
void uartadcarm_sendSpiADCReady(uint32_t a_result);
void uartadcarm_sendSpiTxCheckDone(uint32_t a_result);
void uartcomms_set_fetchsensor_callback( UARTCommsCallBasic a_call );
void uartadcarm_sendFetchSensor(uint8_t * a_buff, uint32_t a_len);
void uartcomms_powerup_hack(void);
void uartcomms_onrecv(uint8_t * buff, uint32_t len, void * ud);

void uartcomms_send_HS_Dataready( UComsHSReady_t * a_data );
void uartcomms_send_HS_DataComplete( UComsHSReady_t * a_data );
void uartcomms_send_sampledata( uint8_t * a_buff, uint32_t a_bufflen );
void uartcomms_send_swtrig_ack( void );

#endif
