//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __INST_UDP_API_H_
#define __INST_UDP_API_H_


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define UDPLDDATALEN 1024
#define BLANK_IP 0x00000000


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct
{
    uint8_t ip_addr[4];
    uint32_t ip_addr_long;
    uint32_t port;
    uint32_t timer;
} UDPSocket;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern UDPSocket udp_socket;
extern RingBuffer rb_udp_tx;

extern uint8_t udp_ddu_tx_buff[MAXBUFFERSIZE];


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void upd_init(void);
void liveView_init( void );
int32_t liveView_register( void );

void udp_send_packets(char *str_UPDTransmitPayload, uint32_t datalen, struct ip4_addr *udpipaddr, uint32_t a_udp_port);
//void udp_send_main(RingBuffer *a_rb, char *str_UPDTransmitPayload, uint32_t datalen, ip_addr_t* udpipaddr, uint32_t a_udp_port);
void udp_send_main(RingBuffer *a_rb, bool a_broadcast);

#endif
