// Add the following into your pre build user commands
// SubWCRev . Source\Inst_Reader_Details.template.h Source\Inst_Reader_Details.h

//! \brief Hardware Revision Firmware is running on
//! \details NOTE: This define affects IO locations and presence of RTC

#define FIRMWARE_DECRIPTION "Instrumentel FW0075"

//! \brief Firmware Version
//! \details Uses SVN revision number to generate FW version
#define FIRMWARE_VERSION "FW0075 20.07.001\0"

//! \brief Type of Firmware
#define FIRMWARE_TYPE	"FW0075\0"

//! \brief Diagnostic Hub Default Label
#define DEFAULT_LABEL "Unassigned\0", 11

//! \brief IP address configuration
//#define TCPIP_STATIC_DEFINED	
#define TCPIP_STATIC
//#define TCPIP_DHCP	
//#define TCPIP_CONFIGDEFINED

//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC1 0x00
//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC2 0x03
//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC3 0x93
//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC4 0xEC
//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC5 0x21
//! \brief Diagnostic Hub MAC Defaults
#define DEFAULT_MAC6 0x93

#define STATIC_IP_A 172
#define STATIC_IP_B 16
#define STATIC_IP_C 0
#define STATIC_IP_D 99

#define STATIC_GW_A 172
#define STATIC_GW_B 16
#define STATIC_GW_C 0
#define STATIC_GW_D 1

#define STATIC_NM_A 255
#define STATIC_NM_B 255
#define STATIC_NM_C 255
#define STATIC_NM_D 0

#define LIVEVIEWSERVER_ENABLED
#define LIVEVIEWSERVER_RATE 60
#define STATIC_LVIP_A 192
#define STATIC_LVIP_B 168
#define STATIC_LVIP_C 51
#define STATIC_LVIP_D 1

//! IP address Macro
#define MACRO_IP_ADDR(ipaddress)	((((unsigned long)(ipaddress[3]))&0x000000FF)|((((unsigned long)(ipaddress[2]))<<8)&0x0000FF00)|((((unsigned long)(ipaddress[1]))<<16)&0x00FF0000)|((((unsigned long)(ipaddress[0]))<<24)&0xFF000000))
//! Static IP Default
#define DEFAULT_IPADDR	((((unsigned long)(STATIC_IP_D))&0x000000FF)|((((unsigned long)(STATIC_IP_C))<<8)&0x0000FF00)|((((unsigned long)(STATIC_IP_B))<<16)&0x00FF0000)|((((unsigned long)(STATIC_IP_A))<<24)&0xFF000000))
//! Static Gateway Default
#define DEFAULT_GWADDR ((((unsigned long)(STATIC_GW_D))&0x000000FF)|((((unsigned long)(STATIC_GW_C))<<8)&0x0000FF00)|((((unsigned long)(STATIC_GW_B))<<16)&0x00FF0000)|((((unsigned long)(STATIC_GW_A))<<24)&0xFF000000))
//! Static Netmask Default
#define DEFAULT_NMADDR ((((unsigned long)(STATIC_NM_D))&0x000000FF)|((((unsigned long)(STATIC_NM_C))<<8)&0x0000FF00)|((((unsigned long)(STATIC_NM_B))<<16)&0x00FF0000)|((((unsigned long)(STATIC_NM_A))<<24)&0xFF000000))

// Diagnostic Hub Defaults
#define DEFAULT_IPARRAY(ipaddress)		ipaddress[0] = STATIC_IP_A;ipaddress[1] = STATIC_IP_B;ipaddress[2] = STATIC_IP_C;ipaddress[3] = STATIC_IP_D	
#define DEFAULT_GWARRAY(ipaddress)		ipaddress[0] = STATIC_GW_A;ipaddress[1] = STATIC_GW_B;ipaddress[2] = STATIC_GW_C;ipaddress[3] = STATIC_GW_D	
#define DEFAULT_SNARRAY(ipaddress)		ipaddress[0] = STATIC_NM_A;ipaddress[1] = STATIC_NM_B;ipaddress[2] = STATIC_NM_C;ipaddress[3] = STATIC_NM_D
#define DEFAULT_TFTPADDRESS(ipaddress)	ipaddress[0] = 0;ipaddress[1] = 0;ipaddress[2] = 0;ipaddress[3] = 0


