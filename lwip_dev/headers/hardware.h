#ifndef __HARDWARE_H__
#define __HARDWARE_H__


////******************************************************************************
//
// Basic Board Details
//
////******************************************************************************

#define PCB_NUMBER 75 // pcb identifier for configs
#define PCB_STRING "PCB0075-E"

//******************************************************************************
//
// System Clock Frequency
//
//******************************************************************************

// System Clock
#define PROCESSOR_CLOCK                 120000000 

// System Tick
#define SYSTICKHZ                       1000
#define SYSTICK_PERIOD_US               (1000000/SYSTICKHZ) 

//******************************************************************************
//
// GPIO - Output Pins
//
//******************************************************************************

// Debug Pins
#define DEBUG_PIN_PERIPH                SYSCTL_PERIPH_GPIOH
#define DEBUG_PIN_BASE                  GPIO_PORTH_BASE
#define DEBUG_PIN_1                     GPIO_PIN_1
#define DEBUG_PIN_2                     GPIO_PIN_2

// LED Pins for Motherboard
#define LED_PIN_PERIPH                  SYSCTL_PERIPH_GPIOP
#define LED_PIN_BASE                    GPIO_PORTP_BASE
#define LED_PIN_1                       GPIO_PIN_2
#define LED_PIN_2                       GPIO_PIN_3
#define LED_PIN_3                       GPIO_PIN_4

// Daughterboard Reset Pin
#define DB_RESET_PIN_PERIPH             SYSCTL_PERIPH_GPIOH
#define DB_RESET_PIN_BASE               GPIO_PORTH_BASE
#define DB_RESET_PIN                    GPIO_PIN_3

// Daughterboard GPS Activation Pin
#define DB_GPS_ZONE_PIN_PERIPH          SYSCTL_PERIPH_GPIOP
#define DB_GPS_ZONE_PIN_BASE            GPIO_PORTP_BASE
#define DB_GPS_ZONE_PIN                 GPIO_PIN_0

// LED Pins for Daughterboard
#define LED_ON_STATE	    			1
#define LED_OFF_STATE       			0


//******************************************************************************
//
// UARTS
//
//******************************************************************************

typedef enum 
{
    DEBUG_UART = 0,
    ARM_TO_ARM_UART,
    SPARE_UART,
    NUMBER_OF_UARTS     // This should always be last in the ENUM list
} UART_INDEXES;

// ARM_TO_ARM is UART2
#define ARM_TO_ARM_UART_PERIPH          SYSCTL_PERIPH_UART2
#define ARM_TO_ARM_GPIO_PERIPH          SYSCTL_PERIPH_GPIOA
#define ARM_TO_ARM_GPIO_BASE            GPIO_PORTA_BASE
#define ARM_TO_ARM_GPIO_TXPIN           GPIO_PIN_7
#define ARM_TO_ARM_GPIO_RXPIN           GPIO_PIN_6
#define ARM_TO_ARM_UART_INT             INT_UART2
#define ARM_TO_ARM_UART_CONFIG          (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define ARM_TO_ARM_UART_BASE            UART2_BASE
#define ARM_TO_ARM_BAUD                 115200 // 2000000
#define ARM_TO_ARM_UART_TXCONFPIN       GPIO_PA7_U2TX
#define ARM_TO_ARM_UART_RXCONFPIN       GPIO_PA6_U2RX
#define ARM_TO_ARM_UART_INT_ENABLED     1
#define ARM_TO_ARM_UART_INT_PRIO        16

#if 1
// Debug is UART0
// PCB 75
#define DEBUG_UART_PERIPH               SYSCTL_PERIPH_UART0
#define DEBUG_GPIO_PERIPH               SYSCTL_PERIPH_GPIOA
#define DEBUG_GPIO_BASE                 GPIO_PORTA_BASE
#define DEBUG_GPIO_TXPIN                GPIO_PIN_1
#define DEBUG_GPIO_RXPIN                GPIO_PIN_0
#define DEBUG_UART_INT                  INT_UART0
#define DEBUG_UART_CONFIG               (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define DEBUG_UART_BASE                 UART0_BASE
#define DEBUG_BAUD                      2000000
#define DEBUG_UART_TXCONFPIN            GPIO_PA1_U0TX
#define DEBUG_UART_RXCONFPIN            GPIO_PA0_U0RX
#define DEBUG_UART_INT_ENABLED          0
#define DEBUG_UART_INT_PRIO             8

#else

// SPARE_1 is UART5
// PCB 85
#define DEBUG_UART_PERIPH               SYSCTL_PERIPH_UART5
#define DEBUG_GPIO_PERIPH               SYSCTL_PERIPH_GPIOC
#define DEBUG_GPIO_BASE                 GPIO_PORTC_BASE
#define DEBUG_GPIO_TXPIN                GPIO_PIN_7
#define DEBUG_GPIO_RXPIN                GPIO_PIN_6
#define DEBUG_UART_INT                  INT_UART5
#define DEBUG_UART_CONFIG               (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define DEBUG_UART_BASE                 UART5_BASE
#define DEBUG_BAUD                      115200
#define DEBUG_UART_TXCONFPIN            GPIO_PC7_U5TX
#define DEBUG_UART_RXCONFPIN            GPIO_PC6_U5RX
#define DEBUG_UART_INT_ENABLED          0
#define DEBUG_UART_INT_PRIO             8

#endif 

// Spare is UART2
#define SPARE_UART_PERIPH               SYSCTL_PERIPH_UART2
#define SPARE_GPIO_PERIPH               SYSCTL_PERIPH_GPIOD
#define SPARE_GPIO_BASE                 GPIO_PORTD_BASE
#define SPARE_GPIO_TXPIN                GPIO_PIN_5
#define SPARE_GPIO_RXPIN                GPIO_PIN_4
#define SPARE_UART_INT                  INT_UART2
#define SPARE_UART_CONFIG               (UART_CONFIG_WLEN_8 | UART_CONFIG_STOP_ONE | UART_CONFIG_PAR_NONE)
#define SPARE_UART_BASE                 UART2_BASE
#define SPARE_BAUD                      115200
#define SPARE_UART_TXCONFPIN            GPIO_PD5_U2TX
#define SPARE_UART_RXCONFPIN            GPIO_PD4_U2RX
#define SPARE_UART_INT_ENABLED          0
#define SPARE_UART_INT_PRIO             4

//******************************************************************************
//
// Real TIme Clock / I2C
//
//******************************************************************************

#define RTC_I2C_PERIPH                  SYSCTL_PERIPH_I2C0
#define RTC_GPIO_PERIPH                 SYSCTL_PERIPH_GPIOB

// SDA 0 Port B Bit 3 Output
// SCL 0 Port B Bit 2 Output
#define RTC_I2C_SDA_PIN                 GPIO_PB3_I2C0SDA
#define RTC_I2C_SCL_PIN                 GPIO_PB2_I2C0SCL

// GPIO Hardware details
#define I2C_PIN_BASE                    GPIO_PORTB_BASE
#define I2C_PIN_SDA                     GPIO_PIN_3
#define I2C_PIN_SCL                     GPIO_PIN_2

// I2C 
#define RTC_I2C_BASE                    I2C0_BASE
#define I2C_PERIPH                      SYSCTL_PERIPH_I2C0
#define I2C_PERIPH_GPIO                 SYSCTL_PERIPH_GPIOB
#define I2C_PORT_BASE                   GPIO_PORTB_BASE
#define I2C_CLK_PIN                     GPIO_PIN_2
#define I2C_DATA_PIN                    GPIO_PIN_3
#define I2C0_MASTER_BASE                0x40020000

// Flashing lights definition

//******************************************************************************
//
// SD Card
//
//******************************************************************************


#if 1

/* Peripheral definitions for EK-LM3S6965 board */
// SSI port
#define SDC_SSI_BASE                SSI0_BASE
#define SDC_SSI_SYSCTL_PERIPH       SYSCTL_PERIPH_SSI0

// GPIO for SSI pins
#define SDC_GPIO_PORT_BASE          GPIO_PORTA_BASE
#define SDC_GPIO_SYSCTL_PERIPH      SYSCTL_PERIPH_GPIOA
#define SDC_SSI_CLK                 GPIO_PIN_2
#define SDC_SSI_TX                  GPIO_PIN_4
#define SDC_SSI_RX                  GPIO_PIN_5
#define SDC_SSI_PINS                (SDC_SSI_TX | SDC_SSI_RX | SDC_SSI_CLK)

#define SDC_CS_GPIO_PORT_BASE      GPIO_PORTA_BASE
#define SDC_CS_GPIO_SYSCTL_PERIPH  SYSCTL_PERIPH_GPIOA
#define SDC_CS                     GPIO_PIN_3

// PCB 75
#define SDC_SD_ENABLE_GPIO_PORT_BASE            GPIO_PORTQ_BASE
#define SDC_SD_ENABLE_GPIO_SYSCTL_PERIPH        SYSCTL_PERIPH_GPIOQ
#define SDC_SD_ENABLE_GPIO_PIN                  GPIO_PIN_4

// PCB 85
//#define SDC_SD_ENABLE_GPIO_PORT_BASE            GPIO_PORTF_BASE
//#define SDC_SD_ENABLE_GPIO_SYSCTL_PERIPH        SYSCTL_PERIPH_GPIOF
//#define SDC_SD_ENABLE_GPIO_PIN                  GPIO_PIN_2

#else

/* Peripheral definitions for EK-LM3S6965 board */
// SSI port
#define SDC_SSI_BASE                SSI0_BASE
#define SDC_SSI_SYSCTL_PERIPH       SYSCTL_PERIPH_SSI0

// GPIO for SSI pins
#define SDC_GPIO_PORT_BASE          GPIO_PORTA_BASE
#define SDC_GPIO_SYSCTL_PERIPH      SYSCTL_PERIPH_GPIOA
#define SDC_SSI_CLK                 GPIO_PIN_2
#define SDC_SSI_FSS                 GPIO_PIN_3
#define SDC_SSI_DATA0               GPIO_PIN_4
#define SDC_SSI_DATA1               GPIO_PIN_5
#define SDC_SSI_DATA2               GPIO_PIN_6
#define SDC_SSI_DATA3               GPIO_PIN_7
#define SDC_SSI_PINS                (SDC_SSI_CLK | SDC_SSI_FSS | SDC_SSI_DATA0 | SDC_SSI_DATA2 | SDC_SSI_DATA2 | SDC_SSI_DATA3)

#define SDC_CS_GPIO_PORT_BASE      GPIO_PORTA_BASE
#define SDC_CS_GPIO_SYSCTL_PERIPH  SYSCTL_PERIPH_GPIOA

#endif

#endif
