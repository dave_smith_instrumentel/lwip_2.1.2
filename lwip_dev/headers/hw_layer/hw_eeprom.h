//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __EEPROM_HW_H__
#define __EEPROM_HW_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define BYTES_PER_WORD                          4

// Address - These are free for use
// Motherboard block start
#define FLASH_MOTHERBOARD_CONFIG_START          0x1F400

// MOTHERBOARD ADDRESS LOCATIONS
#define FLASH_LOCATION_SERIAL                   0x1F400
#define FLASH_LOCATION_MAC                      0x1F414 // DO NOT ACCESS
#define FLASH_LOCATION_IP                       0x1F41C
#define FLASH_LOCATION_SUBNET                   0x1F420
#define FLASH_LOCATION_GATEWAY                  0x1F424
#define FLASH_LOCATION_LOCATION                 0x1F428
#define FLASH_LOCATION_BITFLAGS                 0x1F450
#define FLASH_LOCATION_GPS_PREAMBLE             0x1F454

// MOTHERBOARD FLASH DWORD SIZES
#define FLASH_SIZE_OF_SERIAL	                5	
#define FLASH_SIZE_OF_MAC   	                2   // DO NOT ACCESS
#define FLASH_SIZE_OF_IP    	                1
#define FLASH_SIZE_OF_SUBNET	                1
#define FLASH_SIZE_OF_GATEWAY	                1
#define FLASH_SIZE_OF_LOCATION	                10
#define FLASH_SIZE_OF_BITFLAGS	                1
#define FLASH_SIZE_OF_GPS_PREAMBLE              2

// Bit flags
#define BITFLAG_IP_SETUP                        0x03
#define BITFLAG_SEND_DB_CONFIG                  0x04
#define BITFLAG_WATCHDOG_TRIPPED                0x08
#define BITFLAG_FILE_TYPE                       0x10  // If high, five min file
#define BITFLAG_PORT_46780                      0x20
#define BITFLAG_SAFE_MODE                       0x40  // If the SD init fails
#define BITFLAG_SEND_TAG_CALIBRATION			0x80
#define BITFLAG_PARSE_GPS           			0x01

// Daughterboard block start
#define FLASH_DAUGHTERBOARD_CONFIG_START        0x1F800
#define FLASH_DAUGHTERBOARD_CONFIG_2_START      0x1FC00

#define FLASH_DAUGHTERBOARD_SIZE                512

#define NUM_ACTIVATION_ZONES                    8


//****************************************************************************
//
// Typedefs
//
//****************************************************************************


typedef struct ActivationZone
{
    float longitude_a;
    float latitude_a;
    float longitude_b;
    float latitude_b;
    float longitude_c;
    float latitude_c;
    float longitude_d;
    float latitude_d;
} ActivationZone_t;


// Must be a mulitple of four!
typedef struct EEPROMConfig
{
    uint8_t serial[20];
    uint8_t mac[8];
    uint8_t ip[4];
    uint8_t sn[4];
    uint8_t gw[4];
    uint8_t location[40];
    uint8_t bit_flags[4];
    uint32_t gps_port;
    uint8_t udp_ip[4];
    uint32_t udp_port;
    uint16_t cs;
    uint8_t gps_preamble[8];
} EEPROMConfig_t;

typedef struct Config
{
    EEPROMConfig_t eeprom;
    uint8_t mac[8];
    
} Config_t;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern Config_t g_mb_conf;
extern uint8_t g_daughterboard_config[2048];
extern uint8_t g_tag_calib[2048];
extern ActivationZone_t g_activation_zones[8];

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void eeprom_init(bool a_default);
void eemprom_write(void);
void eemprom_read(void);


#endif

