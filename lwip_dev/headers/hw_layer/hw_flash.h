//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef FLASH_HW_H
#define FLASH_HW_H

//****************************************************************************
//
// Defines
//
//****************************************************************************

#define BYTES_PER_WORD                          4

// Address - These are free for use
// Motherboard block start
#define FLASH_MOTHERBOARD_CONFIG_START          0x1F400

// The flash start address where the DB's firmware will be stored before updating
#define FLASH_BLOCK_TO_ERASE                    0x80000
#define FLASH_BL_DB_BIN_FILE_ADDR_START         0x80004 // was 0x80004
#define FLASH_DB_BIN_SIZE_ADDR_START            0x80000
#define FLASH_ERASE_BLOCKSIZE                   0x2000


// MOTHERBOARD ADDRESS LOCATIONS
#define FLASH_LOCATION_SERIAL                   0x1F400
#define FLASH_LOCATION_MAC                      0x1F414 // DO NOT ACCESS
#define FLASH_LOCATION_IP                       0x1F41C
#define FLASH_LOCATION_SUBNET                   0x1F420
#define FLASH_LOCATION_GATEWAY                  0x1F424
#define FLASH_LOCATION_LOCATION                 0x1F428
#define FLASH_LOCATION_BITFLAGS                 0x1F450

// MOTHERBOARD FLASH DWORD SIZES
#define FLASH_SIZE_OF_SERIAL	                5	
#define FLASH_SIZE_OF_MAC   	                2   // DO NOT ACCESS
#define FLASH_SIZE_OF_IP    	                1
#define FLASH_SIZE_OF_SUBNET	                1
#define FLASH_SIZE_OF_GATEWAY	                1
#define FLASH_SIZE_OF_LOCATION	                10
#define FLASH_SIZE_OF_BITFLAGS	                1


// Daughterboard block start
#define FLASH_DAUGHTERBOARD_CONFIG_START        0x1F800
#define FLASH_DAUGHTERBOARD_CONFIG_2_START      0x1FC00

#define FLASH_DAUGHTERBOARD_SIZE                512


//****************************************************************************
//
// Typedefs
//
//****************************************************************************


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern char g_daughterboard_bin[1024];

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

bool flash_init(void);
void flash_write(uint32_t a_block);
void flash_read(uint32_t address, uint32_t size, uint32_t *save_to_var);


#endif
