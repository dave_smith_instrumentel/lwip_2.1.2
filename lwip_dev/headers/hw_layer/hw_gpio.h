//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __GPIO_HW_H__
#define __GPIO_HW_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************

#define HIGH    true
#define LOW     false
    

//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct  GPIOState
{
    bool     is_config_loaded;
} GPIOState;

typedef struct SETUPState
{
    bool    is_setup_finished;
} SETUPState;


//****************************************************************************
//
// Declarations
//
//****************************************************************************

void setup_output_pin(uint32_t a_peripheral, uint32_t a_base, uint32_t a_pin);
void set_output_pin(uint32_t a_base, uint32_t a_pin, bool a_direction);
void toggle_output_pin(uint32_t a_base, uint32_t a_pin);

void fw70_config_flag_init( void );
void fw70_set_config_loaded( void );
bool fw70_is_config_loaded( void );

void fw70_setup_flag_init( void );
void fw70_set_setup_finish_flag( void );
bool fw70_is_setup_finished( void );

#endif
