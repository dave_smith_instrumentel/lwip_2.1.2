//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __MICRO_SD_H
#define __MICRO_SD_H


//****************************************************************************
//
// Defines
//
//****************************************************************************

//! File Information Structure is active, file not physically open
#define FILE_STATUS_OPEN_IDLE	 	    0
#define FILE_STATUS_OPEN_WRITING 	    1
#define FILE_STATUS_OPEN_PENDING 	    2
#define FILE_STATUS_OPEN_READING 	    3
#define FILE_STATUS_CLOSED 			    4

#define ERRORFLAG_OK		            0
#define ERRORFLAG_NOTOKAY	            1
#define ERRORFLAG_END		            2

#define FILE_OPERATION_NULL 			0
#define FILE_OPERATION_OPEN 			1
#define FILE_OPERATION_OPENPENDING		2	
#define FILE_OPERATION_APPEND 			3
#define FILE_OPERATION_APPENDPENDING	4
#define FILE_OPERATION_READ				5
#define FILE_OPERATION_PENDING			6
#define FILE_OPERATION_CLOSE			7
#define FILE_OPERATION_CLOSEPENDING		8

#define FILE_TYPE_NULL			        9
#define FILE_TYPE_CONFIG		        10
#define FILE_TYPE_READER		        12
#define FILE_TYPE_IO			        13

#define FILE_SEARCH_ERROR		        14
#define FILE_SEARCH_OK			        15
#define FILE_SEARCH_NOFILE		        16
#define FILE_SEARCH_NODAY		        17

#define FILE_READ_MORE			        18
#define FILE_READ_END			        15

#define FILE_DELETED			        20
#define FILE_DIRECTORY			        21
#define FILE_ERRORS				        22

#define IO_RUNNING	  	                0
#define IO_DOWNLOADING	                1
#define IO_HOLD			                2

#define IOFILE_IDLE		                0
#define IOFILE_BUSY		                1

#define FOLDER_STRING_LEN               10


//****************************************************************************
//
// Typedefs
//
//****************************************************************************

typedef struct FileSystemInit
{
    bool mount;
    uint32_t mount_response;
    bool opendir;
    uint32_t opendir_response;
    bool unmount;
    uint32_t unmount_response;
} FileSystemInit;

typedef struct
{
    DIR root_dir;
    DIR day_dir;
    char folder[FOLDER_STRING_LEN];
    char file[FOLDER_STRING_LEN];
    uint32_t dir_index;
    uint32_t file_index;
    bool new_file;
    bool complete;
} Listing;


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern const int8_t PREAMBLE_LEN;

//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

// Debug Functions
unsigned long Inst_fs_NumberFresult(FRESULT function_output);
void micro_sd_test(void);

// Interrupt Function
void Inst_fs_tick(unsigned long ulTickMS);

// Main functions
FileSystemInit file_system_init(void);
bool append_or_create_file(void);
void delete_file(char* a_filepath);
void copy_to_sd_write_rb(char* string_to_save, uint32_t len, bool add_cr);
void emergency_shutdown(void);

// Actions for interaction with the SD card
bool list_sd_card(Listing* a_listing, RingBuffer* a_rb);
bool format_sd_card(void);
void read_from_sd(RingBuffer* a_rb);
void write_to_sd(bool end_of_buff);
bool delete_files(void);
void space_available(DWORD* total_size, DWORD* free_size);

#endif // __MICRO_SD_H
