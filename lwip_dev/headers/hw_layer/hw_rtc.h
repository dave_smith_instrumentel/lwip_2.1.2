//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef RTC_HW_H
#define RTC_HW_H

//****************************************************************************
//
// Defines
//
//****************************************************************************

//Slave addresses

#define I2C_ADDRESS 			0x68	//b1101000, for RTC and WD
#define EEPROM_ADDRESS_1		0x69	//b1101001,	EEPROM block 1
#define EEPROM_ADDRESS_2		0x6A	//b1101010, EEPROM block 2

//Registers
#define HUNDRED_SEC_REG			0x00	//hundredth of seconds
#define SEC_REG					0x01	//seconds
#define MIN_REG					0x02	//minutes
#define HOUR_REG				0x03	//hour
#define DAY_REG					0x04	//day
#define	DATE_REG				0x05	//date
#define MONTH_REG				0x06	//month
#define YEAR_REG				0x07	//year
#define WD_HUNDRED_SEC_REG		0x08	//watchdog hundredth seconds
#define WD_SEC_REG				0x09	//watchdog seconds
#define TRICKLE_CHG_REG			0x0A	//trickle charger
#define FLAG_REG				0x0B	//flags
#define CONTROL_REG				0x0C	//control

//Control Register
#define EN_OSCILLATOR			0x00	
#define DIS_OSCILLATOR			0x80
#define EN_WD_COUNTER			0x02
#define DIS_WD_COUNTER			0x00		
#define	WD_RST					0x01	//trigger reset if WD counter is enable and counter reach 0

//Time format
#define HOUR_MODE_12			0x40
#define HOUR_MODE_24			0x00
#define AM						0x00
#define PM						0x20


// INST defines
#define ERRORFLAG_OK                    0
#define ERRORFLAG_NOTOKAY               1
#define ERRORFLAG_END                   2
#define TIME_TOLERANCE                  3

#define TEMP_BUFF_LEN                   100


//****************************************************************************
//
// Typedefs
//
//****************************************************************************


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************

extern uint32_t g_unixtime, g_unixtime_ms;
extern struct tm g_file_datetime;
extern struct tm *g_datetime;


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void rtc_init(void);
uint32_t rtc_read(void);
uint32_t rtc_write(uint32_t a_time_var);


#endif // RTC_HW_H
