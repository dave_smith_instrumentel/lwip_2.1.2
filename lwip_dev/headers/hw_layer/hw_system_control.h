//****************************************************************************
//
// Guard
//
//****************************************************************************

#ifndef __SYSTEM_CONTROL_HW_H__
#define __SYSTEM_CONTROL_HW_H__


//****************************************************************************
//
// Defines
//
//****************************************************************************

// Default File name
#define DEFAULT_BLIMAGE_FILENAME "development.bin\0\0\0\0\0"
// Addseess
#define FLASH_BOOT_CONF_START 	0x2000
#define FLASH_BOOT_FLAG			FLASH_BOOT_CONF_START	
#define FLASH_HUB_IP 			0x2010
#define FLASH_ROUTE_IP 			0x2020
#define FLASH_SNMASK			0x2030
#define FLASH_TFTP_SERVER_IP	0x2040
#define FLASH_BIN_FILE		   	0x2050

#define FLASH_APP_START_ADD     0x0000
//****************************************************************************
//
// Typedefs
//
//****************************************************************************


//****************************************************************************
//
// Variable Declarations
//
//****************************************************************************


//****************************************************************************
//
// Function Declarations
//
//****************************************************************************

void Inst_SysCtrlReset(void);
int InstDieTemp(void);
void TCPUpdateBegin(void);
void UDPUpdateBegin(void);
void InstSoftwareUpdateConfig(unsigned long IP_addresses[16],char *filename);

#endif
