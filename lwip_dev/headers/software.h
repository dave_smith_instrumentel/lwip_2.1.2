// -----------------------
//
//  This file will contain all compile time software configurable
// items such as CRC enable and buffer sizes
//
//
//  TM4C123GH6PZ - 32KB SRAM, 256KB Flash, 2KB EEPROM
//
// -----------------------

#ifndef __SOFTWARE_H__
#define __SOFTWARE_H__


#define VERSION_BIG                         1
#define VERSION_SMALL                       0
#define VERSION_SOFTWARE_STRING             "TM4C-TEMPLATE"
#define VERSION_SOFTWARE_DESC_STRING        "HighSpeedCurrent Monitoring"

#define VERSION_YEAR                        20
#define VERSION_MONTH                       06
#define VERSION_RELEASE                     1

#define RFID_DECODER_BUFFERSIZE             200
#define UARTMOBO_DECODE_BUFFERSIZE          2048
#define UARTMOBO_BAUD                       2000000
#define UARTMOBO_DECODERFEATURES (UARTDECODER_FLAGS_CRCENABLED | UARTDECODER_FLAGS_TWOBYTLELEN)
#define UARTMOBO_ENCODERFEATURES (UARTENCODER_FLAGS_CRCENABLED | UARTENCODER_FLAGS_TWOBYTLELEN)



////******************************************************************************
//
//  FW0070 Module Config
//
////******************************************************************************

#define CONFIG_BINARYSIZE 			            1024
#define CALIBRATION_BINARYSIZE	                1024
#define CONFIG_MAXSIGNALS  			            32
#define CONFIG_MAXSEQUENCE  		            8
#define CONFIG_MAXMISC 					        8
#define CALIBRATION_MAXMISC			            12
#define CONFIG_MAXBUFFERS				        8
#define CONFIG_MAXRESOURCE 			            4

#define SEQUENCE_MAXSEQUENCEOUTPUTBUFFERS  		50
#define SEQUENCE_MASTERBUFFERSIZE  				4096
#define SEQUENCE_MAXREGISTERPERSEQ  			20

#define UART_MAXUARTS   				        1
#define PULSECOUNTER_MAXCHANS                   5

#define SYSTICK_USEDOUBLEBUFFER                 1
#define SYSTICK_BUFFERCOUNT                     2

#define CONFIG_MAXTRIGGER                       1


////******************************************************************************
//
//  FW0085 Module Config
//
////******************************************************************************

// timer.c
#define TIMER_PERIPH SYSCTL_PERIPH_TIMER2
#define TIMER_BASE TIMER2_BASE
#define TIMER_SIDE TIMER_A                      // use A side for 32 bit mode (not both!)
#define TIMER_CONFIG (TIMER_CFG_PERIODIC)       // 32bit mode
#define TIMER_LOADVALUE                         0xFFFFFFFF
#define TIMER_PRESCALE                          0        // Prescaler only works in 16 bit mode, 32bit goes straight through.. 
#define TIMER_SIDE_INT TIMER_TIMA_TIMEOUT
#define TIMER_TIMERINTFLAG TIMER_TIMA_TIMEOUT
#define TIMER_INT INT_TIMER2A

//buffer_manager.c
#define BUFFMAN_MAXELEMENTS                     14
#define BUFFMAN_MEMORYSIZE                      (1024*BUFFMAN_MAXELEMENTS)

//adc.c
#define ADC_SAMPLEQUEUELEN                      3

#define ADC_CHAN1_SEQ0                          0
#define ADC_CHAN1_SEQ0_SAMPLESIZE               4
#define ADC_CHAN1_SEQ0_PRIORITY                 1
#define ADC_CHAN1_SEQ0_INT                      INT_ADC0SS0 // This is IntEnable()
#define ADC_CHAN1_SEQ0_INTDMA                   ADC_INT_DMA_SS0           ///ADCIntEnableEx

#define ADC_CHAN2_SEQ0                          0
#define ADC_CHAN2_SEQ0_SAMPLESIZE               4
#define ADC_CHAN2_SEQ0_PRIORITY                 1
#define ADC_CHAN2_SEQ0_INT                      INT_ADC1SS0 // This is IntEnable()
#define ADC_CHAN2_SEQ0_INTDMA                   ADC_INT_DMA_SS0           ///ADCIntEnableEx

#define UDMA_ADCCHAN1_CHANASSIGN                UDMA_CH14_ADC0_0 //14
#define UDMA_ADCCHAN1_CHAN                      UDMA_CHANNEL_ADC0 //14
#define UDMA_ADCCHAN1_CONTROL                   (UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_4 )

#define UDMA_ADCCHAN2_CHANASSIGN                UDMA_CH24_ADC1_0
#define UDMA_ADCCHAN2_CHAN                      UDMA_CHANNEL_SSI1RX // 24
#define UDMA_ADCCHAN2_CONTROL                   (UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16 | UDMA_ARB_4 | UDMA_NEXT_USEBURST)

// Timer ADC
#define ADC_TIMER_PERIPH                        SYSCTL_PERIPH_TIMER3
#define ADC_TIMER_BASE                          TIMER3_BASE
#define ADC_TIMER_SIDE                          TIMER_A
#define ADC_TIMER_CONFIG                        (TIMER_CFG_SPLIT_PAIR  | TIMER_CFG_A_PERIODIC)

// RMS
#define HSRMS_MAXCHANNELS                       8

// Trigger
#define HSTRIG_MAXCHANNELS                      8
#define HSTRIG_MAXLIMITPERCHANNEL               4

// ExtRam
#define EXTSRAM_SAMPLEQUEUELEN                  20
#define EXTSRAM_SAMPLEHEADERPREAMBLE            0x6D417253

#define RINGBUFF_RESOURCE_KEYS_LEN              14
#define RINGBUFF_RESOURCE_KEYS_SIZE             (RINGBUFF_RESOURCE_KEYS_LEN * 4)

#define MIN_ADC_BUFFS_REQURIED                  3


//******************************************************************************
//
// Interrupt Priorities
//
//******************************************************************************

// Interrupt priority definitions.  
// The top 3 bits of these values are significant with lower values indicating higher priority interrupts.
#define SYSTICK_INT_PRIORITY                    0x00  
#define UART_INT_PRIORITY		                0x10
#define ETHERNET_INT_PRIORITY                   0x20

// FW70 Interupts
#define PRIORITY_UART_DB                        0xE0
#define PRIORITY_PULSECOUNT_TIMER               0x10
#define PRIORITY_PULSECOUNT_GPIO                0x11  // This should be the same Group, but Lower sub priority than TIMER overflow
#define PRIORITY_UART_RX                        0x20  // Intentionally lower priority than PULSECOUNT

// FW85 Interupts
#define PRIORITY_TIMER_TIMER                    0x00  // reset ASAP
#define PRIORITY_ADC_CHAN1                      0x21
#define PRIORITY_ADC_CHAN2                      0x31

// inst_adc???
#define TEMPBUFFER_SIZE                         24
#define READBUFFER_SIZE                         8
#define SAMPLEBUFFER_SIZE                       2//25000

//******************************************************************************
//
// RTC I2C Interface Parameters
//
//******************************************************************************

// Base Address of device
#define RTC_I2C_ADDR                            0x68

// Address of individual bytes on the RTC
#define RTC_I2C_HUNDRETH_ADDR                   0x00
#define RTC_I2C_SECOND_ADDR                     0x01
#define RTC_I2C_MINUTE_ADDR                     0x02
#define RTC_I2C_HOUR_ADDR                       0x03
#define RTC_I2C_DAY_ADDR                        0x04
#define RTC_I2C_MONTH_ADDR                      0x05
#define RTC_I2C_YEAR_ADDR                       0x06
#define RTC_I2C_STATUS_ADDR                     0x0B

// Bool used on I2C Driverlib read/write
#define RTC_I2C_READ                            true
#define RTC_I2C_WRITE                           false

// Bit flags on RTC status register
#define RTC_I2C_CLEAR_OSC                       0x40
#define RTC_I2C_STATUS_FLAG_WF                  0x40
#define RTC_I2C_STATUS_FLAG_OSF                 0x80

// Delay if RTC has lot power
#define RTC_I2C_BUSY_DELAY                      1000000


#endif 

