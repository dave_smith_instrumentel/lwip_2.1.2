//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <time.h>


// TivaWare includes


// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/flash.h"

// LWIP
#include "lwip-2.1.2/src/include/lwip/tcp.h"
//#include "lwip-2.1.2/src/include/ipv4/lwip/ip4_addr.h"
#include "lwip-2.1.2/src/include/lwip/ip4_addr.h"
#include "lwip-2.1.2/src/include/lwip/ip.h"

// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "firmware_details.h"

// Must be called before micro SD
#include "fatfs/src/ff.h"
#include "drivers/drvr_ring_buffer.h"

// Instrumentel HW Layer
#include "hw_layer/hw_rtc.h"
#include "hw_layer/hw_flash.h"
#include "hw_layer/hw_eeprom.h"
#include "hw_layer/hw_micro_sd.h"
#include "hw_layer/hw_system_control.h"

// Instrumentel Drivers
#include "drivers/drvr_udp.h"
#include "drivers/drvr_debug_sw.h"
//#include "drivers/drvr_uart_codec.h"
#include "drivers/drvr_tcp.h"
//#include "drivers/drvr_micro_sd_interface.h"
//#include "drivers/drvr_bl_uart.h"
//#include "drivers/drvr_base64.h"
//#include "drivers/drvr_bl_emac.h"

// Instrumentel Application
#include "application/app_network_interface.h"
#include "application/app_string_functions.h"
//#include "application/app_uart_db_api.h"
#include "application/interrupt_handler.h"
//#include "application/app_checksum.h"
#include "application/main.h"



//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void discover_response(RingBuffer* a_rb);

// Function used to parse and process messages coming in
static void command_handle_action(char* a_string, uint32_t a_len, struct API_state* a_api);
static void command_handle_debug(char* a_string, uint32_t a_len, struct API_state* a_api);
static void command_handle_dbfirmware(char* a_string, uint32_t a_len, struct API_state* a_api);
static void command_handle_mainfirmware(char* a_string, uint32_t a_len, struct API_state* a_api);
static void command_handle_change(char* a_string, uint32_t a_len, struct API_state* a_api);
//static void command_handle_dbflashop(char* a_string, uint32_t a_len, struct API_state* a_api);

// Functions used to build and send messages going out.

void build_reply(char* a_message, uint32_t a_len);

// Inline functions used for string parsing
static inline int32_t find_char(char* a_string_to_search, uint32_t a_len, char a_char);
static inline uint32_t find_between_chars(char* a_string_to_search, int32_t a_len, char a_char);


#ifdef LEGACY

void discover_response_legacy(RingBuffer* a_rb);
    
#endif


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

//char* g_bin_commands;
uint8_t g_bin_commands[2048];
uint32_t g_bin_commands_to_tx;
char g_db_flash_commands[1024];

uint32_t g_bin_offset;
uint32_t g_b64_len;
char* g_bin_file;

uint32_t g_read_offest;
uint32_t g_block_to_read;

//****************************************************************************
//
// Local Variables
//
//****************************************************************************

// used by the string search function
static uint32_t indexes_of_char[20] = {0};

static char command[3000] = {0};
static char key[50] = {0};
static char value[2048] = {0};
static uint32_t key_len = 0;
static uint32_t value_len = 0;

//static char test_value[1024] = {0};

static char var[1024] = {0};
uint32_t _chunk_to_read = 0;

uint32_t _total_bin_size;
uint32_t _b64_chunk_len = 0;
uint32_t _block_to_write;
uint32_t _bin_to_program;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

int32_t parse_tcp_command(char *a_request, int32_t a_len, struct API_state* a_api)
//********************************************************************************
// Desc: 
// Inputs: 
// Return:
{
    #if 0
    uint16_t _checksum, _recv_checksum;
    #endif
    uint32_t _commands_found;
    
    debugf(DEBUG_HUB_CLIENT, "parse_tcp_command");
    debugf(DEBUG_HUB_CLIENT, "len: %d, %s", a_len, a_request);
    
    // If we have been told to kill the connection then do so.
    if (memcmp(a_request, "kill", 4) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "Kill the TCP conn.");
        
        #if 0
        
        // Remove all stream events from the SD card buffer
        remove_micro_sd_event(LISTING);
        remove_micro_sd_event(READING_SETUP);
        remove_micro_sd_event(READING);
        
        #endif
        
        // Purge any remaining data from the Tx buffer
        RingBuffer_Reset(&rb_tcp_tx);
        return TCP_KILL;
    }
    
    else if (memcmp(a_request, "hi", 2) == 0)
    {
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"HELLO", strlen("HELLO"));
        return TCP_COMMANDS_FAIL;
    }
    
    else if (memcmp(a_request, "cs_off", 2) == 0)
    {
        g_cs_timer = 300000;
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"CS_OFF", strlen("CS_OFF"));
        return TCP_COMMANDS_FAIL;
    }
    
    // Check the first and second bytes are the preamble
    if (memcmp(a_request, PREAMBLE, INST_PREAMBLE_LEN) != 0)
    {
        debugf(DEBUG_HUB_CLIENT, "TCP_PREAMBLE_FAIL %s, %s, %d", a_request, PREAMBLE, INST_PREAMBLE_LEN);
        return TCP_PREAMBLE_FAIL;
    }
    
    #if 0
    // Make sure the checksums add up
    _recv_checksum = (a_request[a_len-2] << 8) + a_request[a_len-1];
    _checksum = gen_crc_16((uint8_t*)&a_request[INST_PREAMBLE_LEN], a_len - (INST_PREAMBLE_LEN + CS_LEN));
    
    debugf(DEBUG_GENERAL, "gen_crc_16(0x%08X , 0x%08X) ", (uint8_t*)&a_request[INST_PREAMBLE_LEN], a_len - (INST_PREAMBLE_LEN + CS_LEN));
    
    if ((_recv_checksum != _checksum) && (g_cs_timer == 0))
    {
        debugf(DEBUG_HUB_CLIENT, "Checksums do not match, _cs: %X, _recv_cs :%X", _checksum, _recv_checksum);
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"cs failed", strlen("cs failed"));
        return TCP_CHECKSUM_FAIL;
    }
    #endif
    
    // Find the number of actions inside the command by splitting the string into 
    // its parts (based on , char)
    _commands_found = find_between_chars((char*)&a_request[INST_PREAMBLE_LEN], a_len - (INST_PREAMBLE_LEN + CS_LEN), ',');
    debugf(DEBUG_HUB_CLIENT, "_commands_found = %d", _commands_found);
    
    // Debug code
    uint32_t _last_index = 0;
    uint32_t _len;
    
    void* _string_start = (void*)&a_request[INST_PREAMBLE_LEN];
    command_handler_call _command_func;
    
    // We haven't found any separators in the string
    if (_commands_found == 1)
    {
        debugf(DEBUG_HUB_CLIENT, "We can't find any commands so exit.");
        return TCP_COMMANDS_FAIL;
    }
    
    // Get the first command to determine which kind of action to call. 
    _len = (indexes_of_char[0] - _last_index) - 1;
    
    // Set the function pointer the pass the arguments too
    if (memcmp(_string_start, "action", _len) == 0)
    {
        _command_func = command_handle_action;
    }
    else if (memcmp(_string_start, "change", _len) == 0)
    {
        _command_func = command_handle_change;
    }
    else if (memcmp(_string_start, "debug", _len) == 0)
    {
        _command_func = command_handle_debug;
    }
    else if (memcmp(_string_start, "mainfirmware", _len) == 0)
    {
        _command_func = command_handle_mainfirmware;
    }
    else if (memcmp(_string_start, "dbfirmware", _len) == 0)
    {
        _command_func = command_handle_dbfirmware;
        //debugf(DEBUG_GENERAL,"_len: %d", _len);
    }
    else if (memcmp(_string_start, "dbflashop", _len) == 0)
    {
        //_command_func = command_handle_dbflashop;
    }
    else
    {
        debugf(DEBUG_HUB_CLIENT, "Cannot find command.");
        return TCP_COMMAND_NOT_FOUND;
    }
    
    // Make sure the ',' char at the start is skipped past 
    _string_start = (char*)_string_start + _len + 1; 
    _last_index = indexes_of_char[0];
        
    for (uint32_t x = 1; x < _commands_found; x++)
    {
        debugf(DEBUG_HUB_CLIENT, "Len = %d - %d", indexes_of_char[x], _last_index);
        
        // Make sure the ',' char at the end is removed 
        _len = (indexes_of_char[x] - _last_index) - 1;
        memcpy(command, _string_start, _len);
        command[_len] = 0x00; // Null the end length!!!!!!
        
        debugf(DEBUG_HUB_CLIENT, "_string_start: %d, _len: %d", _string_start, _len);
                
        // Pass the command to the handler function
        _command_func((char*)&command, _len, a_api);
        
        debugf(DEBUG_HUB_CLIENT, "_command, %s", command);
        
        // Make sure the ',' char at the start is skipped past 
        _string_start = (char*)_string_start + _len + 1; 
        
        // Update the index and clear the command buffer
        _last_index = indexes_of_char[x];
        memset(command, 0x00, 100);
    }
    
    return 0;
}


void parse_udp_command(RingBuffer * a_rb)
//***************************************
// Desc: 
// Inputs: 
// Return:
{
    static char _temp[200] = {0};
    memset(_temp, 0x00, sizeof(_temp));
    
    uint32_t _data_len = RingBuffer_Count(a_rb);
    RingBuffer_CopyOut(a_rb, (uint8_t*)_temp, _data_len);

    // Added a len chack to ensure we arent responding to someone elses response.
    if ((memcmp(_temp, "discover", 8) == 0) && (strlen(_temp) < 10))
    {
        discover_response(&rb_udp_tx);
    }
    
    #ifdef LEGACY
    
    else if (memcmp(_temp, "<request><name>discovery</name></request>", strlen("<request><name>discovery</name></request>")) == 0)
    {
        discover_response_legacy(&rb_udp_tx);
    }
    
    else if (memcmp(_temp, "<request><name>mainfirmware", 27) == 0)
    {
        #if 0
        software_update_begin(g_sys_clk);
        #endif
    }
    
    #endif
    
    // We need to bootload some new firmware
    else if (memcmp(_temp, "mainfirmware", 12) == 0)
    {        
        #if 0
        software_update_begin(g_sys_clk);
        #endif
        
        #if 0
        // #TODO - Remove once new update function has been tested
        InstSoftwareUpdateConfig(UpdateIPSettings, UpdateFileName);
        UDPUpdateBegin();
        #endif
    }
    else if (memcmp(_temp, "reboot", 6) == 0)
    {
        // Reboot the hub!
        Inst_SysCtrlReset();
    }
    
    // Send the UDP response
    int32_t rb_count = RingBuffer_Count(&rb_udp_tx);
    
    if (rb_count == 0)
    {
        return;
    }
    
    udp_send_main(&rb_udp_tx, true);
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


static inline int32_t find_char(char* a_string_to_search, uint32_t a_len, char a_char)
//************************************************************************************
// Desc: Take a string and find the index of a particular character occurs.
// Inputs: a_string_to_search within, lenght of string to search, charatcer to search for.
// Return: index positions of char, or -1 if it cannot be found
{
    // Loop through all the chars in the string.
    for (uint32_t x = 0; x < a_len; x++)
    {
        if (a_string_to_search[x] == a_char)
        {
            return x;
        }
    }
    
    return -1;
}

static inline uint32_t find_between_chars(char* a_string_to_search, int32_t a_len, char a_char)
//*******************************************************************************************
// Desc: Take a string and find the index within it of where a particular character occurs.
// Inputs: a_string_to_search within, lenght of string to search, charatcer to search for.
// Return: Pointer to array of index positions
{
    uint32_t _index = 0;
    
    // Clean the buffer of indexes
    memset(indexes_of_char, 0x00, sizeof(indexes_of_char));
        
    // Loop through all the chars in the string.
    for (uint32_t x = 0; x < a_len; x++)
    {
        if (a_string_to_search[x] == a_char)
        {
            debugf(DEBUG_HUB_CLIENT, "Found %c at %d", a_char, x);
            indexes_of_char[_index++] = x + 1;
        }
        
        // Check the index hasn't gone above the max value.
        if (_index >= 20)
        {
            debugf(DEBUG_HUB_CLIENT, "_index is above 20");
        }
    }
        
    // Add the last char position of the string as an index
    indexes_of_char[_index++] = a_len + 1;
    debugf(DEBUG_HUB_CLIENT, "Found %c at %d", a_char, a_len + 1);
    
    return _index;
}

static void findBetweenChar(char* write_to_var, char* string, const char* character, uint8_t cycles, bool hex)
//************************************************************************************************************
// Desc: Used to parse network address variables into flash memory.
// Inputs: var to write to, string to parse, character to split on, number of cycles, hex ascii or bin.
// Return: None
{
    char* temp;
    uint8_t i = 0;
    
    temp = strtok(string, character);
    
    while(temp != NULL)
    {
        if (hex)
            write_to_var[i] = Inst_string2hex((char*)temp);
        else
            write_to_var[i] = Inst_StringToInt(temp,3);
        
        temp = strtok(NULL, character);
        
        if(++i > cycles)
            break;
    }
}

 
static inline void find_key_and_value(char* a_string, uint32_t a_len, char* a_key, uint32_t* a_key_len, char* a_value, uint32_t* a_value_len)
//**********************************************************************************************************************
// Desc: Take a string and it's lenght and see if it is a dictionary pair or just a command. Update the varaibles passed in.
// Inputs:
// Return:
{
    int32_t _split_char = find_char(a_string, a_len, '=');
    
    // Can't find a split char so only update the key variable.
    if (_split_char == -1)
    {
        // We only have a key so move the whole string across
        *a_key_len = a_len;
        memcpy(a_key, a_string, *a_key_len);
        
        // Set the _value to blank
        *a_value_len = 0;
        memset(a_value, 0x00, sizeof(*a_value));
        return;
    }
    else
    {
        // Get the key out of the string.
        *a_key_len = _split_char;
        memcpy(a_key, a_string, *a_key_len);
        
        // Get the value out of the string.
        *a_value_len = strlen(a_string) - _split_char;
        memcpy(a_value, a_string + _split_char + 1, *a_value_len);
    }
    
    debugf(DEBUG_HUB_CLIENT, "_key = %s", a_key);
    debugf(DEBUG_HUB_CLIENT, "_value = %s", a_value);
}

static void command_handle_mainfirmware(char* a_string, uint32_t a_len, struct API_state* a_api)
//**********************************************************************************************
// Desc: Parse over motherboard firmware commands and execute the appropriate actions.
// Inputs: A pointer to the string with the information within it and the len of the string itself.
// Return: None
{    

}

static void command_handle_dbfirmware(char* a_string, uint32_t a_len, struct API_state* a_api)
//********************************************************************************************
// Desc: Parse over db firmware commands and execute the appropriate actions.
// Inputs: A pointer to the string with the information within it and the len of the string itself.
// Return: None
{
    memset(key, 0x00, sizeof(key));
    memset(value, 0x00, sizeof(value));
    
    memset((char*)g_daughterboard_bin, 0x00, sizeof(g_daughterboard_bin));
    memset(var, 0x00, sizeof(var));
    
    #if 0
    uint32_t _parts_found = 0;
    static unsigned long i;
    
    uint32_t _bin_length = 0;
    
    // Update the local vars with the key and value info
    find_key_and_value(a_string, a_len, (char*)&key, &key_len, (char*)&value, &value_len);  
    
    // Check the version of the firmware running on the DB
    if (memcmp(key, "version", 7) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, version");
        
        // Send a version request over UART
        g_bl_command = BL_CMD_VERSION;
    }
    
    // Erase specific block of the MB flash in order to store the DB's new FW version
    else if(memcmp(key, "erase_flash", 11) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, erase_flash");
        
        _parts_found = find_between_chars((char*)&value, value_len, ':');
        
        if (_parts_found == 2)
        {
            // Set the erase flag to avoid triggering the watchdog timer
            g_flash_busy = true;
            
            _bin_length = Inst_StringToInt(&value[0], indexes_of_char[0]);
            
            debugf(DEBUG_GENERAL,"Length of bin file is: %d", _bin_length);
            
            uint32_t _start_addr = FLASH_BLOCK_TO_ERASE;
            uint32_t _end_addr = FLASH_BLOCK_TO_ERASE + _bin_length;
            uint32_t _block_start = _start_addr/FLASH_ERASE_BLOCKSIZE;
            _start_addr = _block_start * FLASH_ERASE_BLOCKSIZE;
            
            // number of blocks to erase:
            uint32_t _block_end = (_end_addr)/FLASH_ERASE_BLOCKSIZE;
            
            uint32_t _block_erase_count = (_block_end - _block_start);
            if( _end_addr%FLASH_ERASE_BLOCKSIZE != 0 )
            {
                _block_erase_count += 1;
                _end_addr = _end_addr + (_end_addr%FLASH_ERASE_BLOCKSIZE );
            }
            
            debugf(DEBUG_GENERAL," Erase between 0x%08X to 0x%08X blocks(%d)", _start_addr, _end_addr, _block_erase_count);
            
            // Erase each block seperately
            uint32_t _addr_erase = _start_addr;
            for(uint32_t _n = 0; _n<_block_erase_count; _n++)
            {
                debugf(DEBUG_GENERAL," Erase Addr 0x%08X (%d / %d)", _addr_erase, _n, _block_erase_count);
            
                FlashErase(_addr_erase);
                _addr_erase += FLASH_ERASE_BLOCKSIZE;
            }
            debugf(DEBUG_GENERAL, " Erase Complete");
            
            // Initialise the counters when erasing the flash
            _chunk_to_read = 0;
            _total_bin_size = 0;
        }
        
        // Once erase the flash, set the flag back to false
        g_flash_busy = false;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"ERASE_FLASH", strlen("ERASE_FLASH"));
    }
    
    // Read the contents of the flash, starting at specific address
    else if(memcmp(key, "read_flash", 10) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, read_flash");
        
        // Set the read flag in order to avoid locking up and restarting due to watchdog timer
        g_flash_busy = true;
        
        _parts_found = find_between_chars((char*)&value, value_len, ':');
        
        if (_parts_found == 2)
        {
            g_read_offest = Inst_StringToInt(&value[0],indexes_of_char[0]);
            debugf(DEBUG_HUB_CLIENT,"g_read_offest: %d, _chunk_to_read: %d", g_read_offest, _chunk_to_read);
            
            // Increase the address value before reading from flash. This is done to make sure we're reading from the correct flash address
            g_block_to_read = _chunk_to_read + FLASH_BL_DB_BIN_FILE_ADDR_START;
            
            // Read the flash contents by pointing at specific flash address
            memcpy((uint32_t*)&var, (void*)g_block_to_read, 1024);
            
            // Loop through the whole buffer to output the values written into flash. Can be used for debugging
            for(i = 0; i < 1024; i++)
            {
                debugf(DEBUG_HUB_CLIENT, "%05X:%02X",i,var[i]);
            }
            
            //flash_read(g_block_to_read, 1024, (uint32_t*)&var);    // Just leave this here please, can be used for testing - DrK
            _chunk_to_read += 1024; 
        }
        
        // Set the read flag back to false
        g_flash_busy = false;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"READ_FLASH", strlen("READ_FLASH"));  
    }
    
    // Write the new bin file into the flash, starting from specific address
    else if(memcmp(key, "bin", 3) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, bin");
        
        // Set the read flag in order to avoid locking up and restarting due to watchdog timer
        g_flash_busy = true;
        
        // Get the bin command parts for offset and the B64 string
        _parts_found = find_between_chars((char*)&value, value_len, ':');
        //debugf(DEBUG_GENERAL, "_parts_found: %d", _parts_found);
        
        if (_parts_found == 3)
        {
            // offset, b64data_len(string size), b64 data to write
            // Parse the offset and the B64 string sent on TCP
            g_bin_offset = Inst_StringToInt(&value[0], indexes_of_char[0] - 1);
            
            _b64_chunk_len = Inst_StringToInt(&value[indexes_of_char[0]],indexes_of_char[1] - indexes_of_char[0] - 1);
            debugf(DEBUG_GENERAL, "_b64_chunk_len: %d", _b64_chunk_len);
            
            g_bin_file = (char*)&value[indexes_of_char[1]];
            debugf(DEBUG_GENERAL, "read offset = %d, len of b64 = %d, b64_bin_file: %s", g_bin_offset, _b64_chunk_len, g_bin_file); //strlen((char*)g_bin_file)
            
            // Increase the address value based on the value of the offset
            _block_to_write = g_bin_offset + FLASH_BLOCK_TO_ERASE;
            debugf(DEBUG_GENERAL,"g_block_to_write: %X", _block_to_write);
            
            // Decode the B64 in order to store the values in binary format
            _bin_to_program = b64_Decoder((char*)g_bin_file, _b64_chunk_len, (char*)&g_daughterboard_bin[1], sizeof(g_daughterboard_bin)); //strlen((char*)g_bin_file)
            debugf(DEBUG_GENERAL,"g_bin_to_program: %d", _bin_to_program);
            
            // Calculate the total size of the file that is stored into flash.
            _total_bin_size += _bin_to_program;
            debugf(DEBUG_GENERAL,"g_total_bin_size: %d", _total_bin_size);
            
            // Write the new binary contents into flash (on specific addresses)
            FlashProgram((uint32_t*)&g_daughterboard_bin[1], _block_to_write , 1020);
        }
        
        // Set the write flag back to false
        g_flash_busy = false;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"BIN_FILE_FLASH", strlen("BIN_FILE_FLASH"));
    }
    
    // Put the DB in bootloader mode
    else if(memcmp(key, "blmode", 7) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, blmode");
        
        g_bl_command = BL_CMD_MODE;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"BL_CMD_MODE", strlen("BL_CMD_MODE"));
    }
    
    // Start the update of DB
    else if(memcmp(key, "update_db", 8) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "command_handle_dbfirmware, update_db");
        
        // Start the update of the DB
        debugf(DEBUG_HUB_CLIENT,"Starting update process...\n");
        bool _success = bluart_ethcmd_updatedb();
        
        if(_success)
        {
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"UPDATE_DB_FW_SUCC", strlen("UPDATE_DB_FW_SUCC"));
        }
        else
        {
            // issue happend
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"UPDATE_DB_FW_ERROR", strlen("UPDATE_DB_FW_ERROR"));
        
        }
    }
    #endif
}

static void command_handle_action(char* a_string, uint32_t a_len, struct API_state* a_api)
//****************************************************************************************
// Desc: Parse over action commands and execute the appropriate actions.
// Inputs: A pointer to the string with the information within it and the len of the string itself.
// Return: None
{
    memset(key, 0x00, sizeof(key));
    memset(value, 0x00, sizeof(value));
    
    #if 0
    uint32_t _parts_found = 0;
    #endif
    
    debugf(DEBUG_HUB_CLIENT, "command_handle_action: %s", a_string);
    
    // Update the local vars with the key and value info
    find_key_and_value(a_string, a_len, (char*)&key, &key_len, (char*)&value, &value_len);
    
    
    // Hub discover over TCP
    if (memcmp(key, "discover", 8) == 0)
    {
        discover_response(&rb_tcp_tx);
    }
    #if 0
    // Change directory
    else if (memcmp(key, "cd", 2) == 0)
    {
        memcpy(g_read_folder_path, value, value_len);
        // #TODO - put back in later once Download script handles data stream.
        //RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"\r\n", strlen("\r\n"));
    }
    
    // List contents on current directory.
    else if (memcmp(key, "ls", 2) == 0)
    {
        add_micro_sd_event(LISTING);
    }
    
    // Remove a folder
    else if (memcmp(key, "rm", 2) == 0)
    {
        memcpy(g_delete_folder_path, value, value_len);
        add_micro_sd_event(DELETE);
    }
    
    // How much disk is free?
    else if (memcmp(key, "df", 2) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "disk free");
        add_micro_sd_event(SPACE_AVAILABLE);
    }
    
    // Download a file directly into the TCP Tx buffer. (fast download)
    else if (memcmp(key, "cbt", 3) == 0)
    {
        a_api->State = COMMAND_READFILE;
    }
    
    // Download a file into the tcp ring buffer. (slow download)
    else if (memcmp(key, "cct", 3) == 0)
    {
        // Get the cct command parts for folder, file, offset and size
        _parts_found = find_between_chars((char*)&value, value_len, ':');
        
        if (_parts_found == 4)
        {            
            memset(g_read_folder_path, 0x00, sizeof(g_read_folder_path));
            memset(g_read_file_path, 0x00, sizeof(g_read_file_path));
            memcpy(g_read_folder_path, &value[0], indexes_of_char[0] - 1);
            memcpy(g_read_file_path, &value[indexes_of_char[0]], indexes_of_char[1] - indexes_of_char[0] - 1);
            g_read_offset = Inst_StringToInt(&value[indexes_of_char[1]], indexes_of_char[2] - indexes_of_char[1] - 1);
            g_read_length = Inst_StringToInt(&value[indexes_of_char[2]], indexes_of_char[3] - indexes_of_char[2] - 1);
            
            debugf(DEBUG_HUB_CLIENT, "read folder = %s, read file = %s, read offset = %d, read lenght = %d", g_read_folder_path, g_read_file_path, g_read_offset, g_read_length);
        }
        
        add_micro_sd_event(READING_SETUP);

        a_api->State = COMMAND_READFILE_BUFFER;
    }
    
    // Format the SD card
    else if (memcmp(key, "format", 6) == 0)
    {
        add_micro_sd_event(FORMAT);
    }
    
    // Reboot the system
    else if (memcmp(key, "reboot", 6) == 0)
    {
        // Send the reply but don't do anything until it has actually gone.
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"REBOOT", strlen("REBOOT"));
        a_api->State = COMMAND_REBOOT;
    }
    
    // Format the SD card and then reboot the system
    else if (memcmp(key, "forboot", 7) == 0)
    {
        add_micro_sd_event(FORMAT);
        
        a_api->State = COMMAND_FORBOOT;
    }
    #endif
    
    // Send a command to the daughterboard regarding the Config file
    else if (memcmp(key, "db_conf", 7) == 0)
    {
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)g_daughterboard_config, strlen((char*)g_daughterboard_config));
    }
    
    // Send a command to the daughterboard 
    else if (memcmp(key, "db_calib", 8) == 0)
    {
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)g_tag_calib, strlen((char*)g_tag_calib));
    }
	#if 0	
    // Send a command to the daughterboard
    else if (memcmp(key, "soft_trigger", 12) == 0)
    {
        g_soft_trigger = true;
        debugf(DEBUG_GENERAL, "Value %s", value);
        
        g_trig_len = b64_Decoder((char*)value, value_len, (char*)&g_trigger_params[1], sizeof(g_trigger_params)-1);
        
        for (uint32_t x=0; x<(g_trig_len + 1); x++)
        {
            debugf(DEBUG_GENERAL, "g_trigger_params[%d]: %X", x, g_trigger_params[x]);
        }
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"SOFT_TRIGGER", strlen("SOFT_TRIGGER"));
    }
    #endif
    // Send a command to the daughterboard
    else if (memcmp(key, "hard_trigger", 12) == 0)
    {
        g_hard_trigger = true;
        g_hard_trigger_timer = 10000;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"HARD_TRIGGER", strlen("HARD_TRIGGER"));
    }
    
    // DDU Liveview on/off
    else if (memcmp(key, "ddu_liveview", 12) == 0)
    {
        if (memcmp(value, "on", 2) == 0)
        {                
            g_ddu_liveview = true;
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"LIVEVIEW_DDU_ON", strlen("LIVEVIEW_DDU_ON"));
        }
        else if (memcmp(value, "off", 3) == 0)
        {
            g_ddu_liveview = false;
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"LIVEVIEW_DDU_OFF", strlen("LIVEVIEW_DDU_OFF"));
        }
    }
    
    // Ethernet ROM Bootloader
    else if (memcmp(key, "rom_ethernet", 12) == 0)
    {
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"ETHERNET_BOOTLOADER", strlen("ETHERNET_BOOTLOADER"));
        
        #if 0
        // #TODO - Remove once new update function has been tested
        InstSoftwareUpdateConfig(UpdateIPSettings, UpdateFileName);
        UDPUpdateBegin();
        #endif
        #if 0
        software_update_begin(g_sys_clk);
        #endif
        
    }
}

static void command_handle_change(char* a_string, uint32_t a_len, struct API_state* a_api)
//***************************************************************
{
    memset(key, 0x00, sizeof(key));
    memset(value, 0x00, sizeof(value));
    
    memset((char*)g_db_flash_commands, 0x00, sizeof(g_db_flash_commands));
    
    uint32_t _parts_found = 0;
    uint32_t _chunk_offset = 0;
    char* _conf_file = 0;
    
    debugf(DEBUG_GENERAL, "command_handle_change, a_len: %d", a_len);
    
    //debugf(DEBUG_GENERAL, "command_handle_change: %s", a_string);
    
    // Update the local vars with the key and value info
    find_key_and_value(a_string, a_len, (char*)&key, &key_len, (char*)&value, &value_len);  
    
    // Save the daughterboard config in B64 encoding.
    if (memcmp(key, "db_conf_parts", 13) == 0)
    {            
        debugf(DEBUG_GENERAL, "value_len: %d", value_len);
        
        // Get the bin command parts for offset and the B64 string
        _parts_found = find_between_chars((char*)&value, value_len, ':');
        
        if (_parts_found == 2)
        {
            _chunk_offset = Inst_StringToInt(&value[0], indexes_of_char[0]);
            
            _conf_file = (char*)&value[indexes_of_char[0]];
            debugf(DEBUG_GENERAL, "_conf_file: %s", _conf_file);
            
            if(_chunk_offset == 0)
            {
                // first chunk, blank whole buffer
                memset(g_daughterboard_config, 0x00, sizeof(g_daughterboard_config));
            }
            
            memcpy((char*)&g_daughterboard_config[_chunk_offset], (char*)_conf_file, value_len);
//            
//            for(uint32_t i=0; i<=sizeof(g_daughterboard_config);i++)
//            {
//                debugf(DEBUG_GENERAL, "g_daughterboard_config[%d]: %02X", i, g_daughterboard_config[i]);
//            }
            
        }
        
        g_mb_conf.eeprom.bit_flags[0] |= BITFLAG_SEND_DB_CONFIG;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW DB CONF", strlen("NEW DB CONF"));
    }
    
    // Save the daughterboard config in B64 encoding.
    else if (memcmp(key, "db_conf", 7) == 0)
    {            
        debugf(DEBUG_GENERAL, "value_len: %d", value_len);
        
        debugf(DEBUG_GENERAL, "In db_conf");
        
        memcpy((char*)&g_daughterboard_config, (char*)&value, value_len);
        
        g_mb_conf.eeprom.bit_flags[0] |= BITFLAG_SEND_DB_CONFIG;
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW DB CONF", strlen("NEW DB CONF"));
    }
		
    // Set the calibration data
    else if(memcmp(key, "db_calib", 8) == 0)
    {
        debugf(DEBUG_HUB_CLIENT, "value_len: %d", value_len);
        
        memcpy((char*)&g_tag_calib, (char*)&value, value_len);
        g_mb_conf.eeprom.bit_flags[0] |= BITFLAG_SEND_TAG_CALIBRATION;
    
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW CALIB", strlen("NEW CALIB"));
    }
    #if 0
    // Send over UART the DB flash commands and read the response
    else if(memcmp(key, "db_cmd", 6) == 0)
    {
        debugf(DEBUG_GENERAL, "value_len: %d", value_len);
        
        memcpy((char*)&g_bin_commands, (char*)&value, value_len);
        
        //g_bin_commands = (char*)&value[indexes_of_char[1]];
        debugf(DEBUG_GENERAL, "len of b64 = %d, b64_bin_cmds: %s", strlen((char*)g_bin_commands), g_bin_commands);
        
        // 1. Decode the b64 payload received from TCP
        g_bin_commands_to_tx = b64_Decoder((char*)g_bin_commands, strlen((char*)g_bin_commands), (char*)&g_db_flash_commands, sizeof(g_db_flash_commands));
        debugf(DEBUG_GENERAL,"g_bin_commands_to_tx: %d", g_bin_commands_to_tx);
        
        // 2. Encode the payload (preamble+len+data+CRC) and send it to DB through UART2
        UARTEncoder_SendPacket(&g_UARTMoboEncoder, (uint8_t*)&g_db_flash_commands, g_bin_commands_to_tx); // sizeof(g_db_flash_commands)
        
        // 3. Decode the response received from DB
        // 4. Copy in the ring buffer the response and transmit it
        
        //RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"FLASH_DB", strlen("FLASH_DB")); // #TODO - Must be removed from here, otherwise it will terminate the TCP connection, DrK
    }
    #endif
    
    // Set the time of the device.
    else if (memcmp(key, "time", 4) == 0)
    {
        rtc_write(Inst_StringToInt(value, value_len));
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW TIME - ", strlen("NEW TIME - "));
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)&value, value_len);
    }
    
    // Set the IP address of the device.
    else if (memcmp(key, "ip", 2) == 0)
    {
        findBetweenChar((char*)&g_mb_conf.eeprom.ip[0], (char*)&value, ".", 4, false);

        debugf(DEBUG_HUB_CLIENT, "Received IP Address = %d.%d.%d.%d", 
            g_mb_conf.eeprom.ip[0], g_mb_conf.eeprom.ip[1], 
            g_mb_conf.eeprom.ip[2], g_mb_conf.eeprom.ip[3]);
        
        build_reply("NEW IP", strlen("NEW IP"));
    }
    
    // Set the subnet address of the device.
    else if (memcmp(key, "sn", 2) == 0)
    {
        findBetweenChar((char*)&g_mb_conf.eeprom.sn[0], (char*)&value, ".", 4, false);

        debugf(DEBUG_HUB_CLIENT, "Received SN Address = %d.%d.%d.%d", 
            g_mb_conf.eeprom.sn[0], g_mb_conf.eeprom.sn[1], 
            g_mb_conf.eeprom.sn[2], g_mb_conf.eeprom.sn[3]);
        
        build_reply("NEW SN", strlen("NEW SN"));
    }
    
    // Set the Gateway address of the device.
    else if (memcmp(key, "gw", 2) == 0)
    {
        findBetweenChar((char*)&g_mb_conf.eeprom.gw[0], (char*)&value, ".", 4, false);

        debugf(DEBUG_HUB_CLIENT, "Received GW Address = %d.%d.%d.%d", 
            g_mb_conf.eeprom.gw[0], g_mb_conf.eeprom.gw[1], 
            g_mb_conf.eeprom.gw[2], g_mb_conf.eeprom.gw[3]);
        
        build_reply("NEW GW", strlen("NEW GW"));
    }
    
    // Set the serial number of the device.
    else if (memcmp(key, "serial", 6) == 0)
    {
        memset(g_mb_conf.eeprom.serial, 0x00, sizeof(g_mb_conf.eeprom.serial));
        memcpy((char*)g_mb_conf.eeprom.serial, (char*)value, value_len);
        
        build_reply("NEW SERIAL", strlen("NEW SERIAL"));
    }
    
    // Set the location of the device.
    else if (memcmp(key, "location", 8) == 0)
    {
        memset(g_mb_conf.eeprom.location, 0x00, sizeof(g_mb_conf.eeprom.location));
        memcpy((char*)g_mb_conf.eeprom.location, (char*)value, value_len);
        
        build_reply("NEW LOCATION", strlen("NEW LOCATION"));
    }
    
    // Set how often the hub should create new files.
    else if (memcmp(key, "file_type", 9) == 0)
    {
        if (memcmp(value, "1", 1) == 0)
        {                
            g_mb_conf.eeprom.bit_flags[0] &= ~BITFLAG_FILE_TYPE;
        }
        else
        {
            g_mb_conf.eeprom.bit_flags[0] |= BITFLAG_FILE_TYPE;
        }
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW FILE TYPE", strlen("NEW FILE TYPE"));
    }
    
    else if (memcmp(key, "gps_parse", 9) == 0)
    {
        if (memcmp(value, "1", 1) == 0)
        {                
            g_mb_conf.eeprom.bit_flags[1] |= BITFLAG_PARSE_GPS;
            debugf(DEBUG_HUB_CLIENT, "GPS PARSE: %X", g_mb_conf.eeprom.bit_flags[1]);
        }
        else
        {
            g_mb_conf.eeprom.bit_flags[1] &= ~BITFLAG_PARSE_GPS;
            debugf(DEBUG_HUB_CLIENT, "GPS LOG: %X", g_mb_conf.eeprom.bit_flags[1]);
        }
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW FILE TYPE", strlen("NEW FILE TYPE"));
    }
    
    else if (memcmp(key, "gps_preamble", 12) == 0)
    {
        if (value_len > sizeof(g_mb_conf.eeprom.gps_preamble))
        {
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"GPS LEN > 8 BYTES", strlen("GPS LEN > 8 BYTES"));
        }
        else
        {
            memset(g_mb_conf.eeprom.gps_preamble, 0x00, sizeof(g_mb_conf.eeprom.gps_preamble));
            memcpy((char*)g_mb_conf.eeprom.gps_preamble, (char*)value, value_len);
            
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"GPS PREAMBLE SET", strlen("GPS PREAMBLE SET"));
        }
    }

    else if (memcmp(key, "gps_zone", 8) == 0)
    {
        // Get the cct command parts for folder, file, offset and size
        uint32_t _parts_found = find_between_chars((char*)&value, value_len, ':');
        
        if (_parts_found == 9)
        {            
            uint8_t _array_index = Inst_StringToInt(&value[0], indexes_of_char[0] - 1);
            
            g_activation_zones[_array_index].latitude_a = Inst_StringToDouble(&value[indexes_of_char[0]], indexes_of_char[1] - indexes_of_char[0] - 1);
            g_activation_zones[_array_index].longitude_a  =  Inst_StringToDouble(&value[indexes_of_char[1]], indexes_of_char[2] - indexes_of_char[1] - 1);
            g_activation_zones[_array_index].latitude_b = Inst_StringToDouble(&value[indexes_of_char[2]], indexes_of_char[3] - indexes_of_char[2] - 1);
            g_activation_zones[_array_index].longitude_b =  Inst_StringToDouble(&value[indexes_of_char[3]], indexes_of_char[4] - indexes_of_char[3] - 1);
            g_activation_zones[_array_index].latitude_c = Inst_StringToDouble(&value[indexes_of_char[4]], indexes_of_char[5] - indexes_of_char[4] - 1);
            g_activation_zones[_array_index].longitude_c =  Inst_StringToDouble(&value[indexes_of_char[5]], indexes_of_char[6] - indexes_of_char[5] - 1);
            g_activation_zones[_array_index].latitude_d = Inst_StringToDouble(&value[indexes_of_char[6]], indexes_of_char[7] - indexes_of_char[6] - 1);
            g_activation_zones[_array_index].longitude_d =  Inst_StringToDouble(&value[indexes_of_char[7]], indexes_of_char[8] - indexes_of_char[7] - 1);
            
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"NEW GPS ZONE", strlen("NEW GPS ZONE"));
        }
    }
    
    // Set the IP address of the udp.
    else if (memcmp(key, "udp_ip", 6) == 0)
    {
        findBetweenChar((char*)&g_mb_conf.eeprom.udp_ip[0], (char*)&value, ".", 4, false);

        debugf(DEBUG_HUB_CLIENT, "Received IP Address = %d.%d.%d.%d", 
            g_mb_conf.eeprom.udp_ip[0], g_mb_conf.eeprom.udp_ip[1], 
            g_mb_conf.eeprom.udp_ip[2], g_mb_conf.eeprom.udp_ip[3]);
        
        build_reply("NEW UDP IP", strlen("NEW UDP IP"));
    }
    
    // Set the port address of the udp.
    else if (memcmp(key, "udp_port", 8) == 0)
    {
        g_mb_conf.eeprom.udp_port = Inst_StringToInt(value, value_len);

        build_reply("NEW UDP PORT", strlen("NEW UDP PORT"));
    }
    
    // Set the port address of the udp.
    else if (memcmp(key, "gps_port", 8) == 0)
    {
        g_mb_conf.eeprom.gps_port = Inst_StringToInt(value, value_len);
        debugf(DEBUG_GENERAL, "GPS port: %d", g_mb_conf.eeprom.gps_port);

        build_reply("NEW GPS PORT", strlen("NEW GPS PORT"));
    }
    
    // Commit all the changes to flash.
    else if (memcmp(key, "commit", 6) == 0)
    {
        eemprom_write();
        
        debugf(DEBUG_HUB_CLIENT, "Commit, bit flags = %X", g_mb_conf.eeprom.bit_flags[0]);
        
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"COMMIT", strlen("COMMIT"));
    }
}

static void command_handle_debug(char* a_string, uint32_t a_len, struct API_state* a_api)
//**************************************************************
// Desc: Parse over debug commands and execute the appropriate actions.
// Inputs: A pointer to the string with the information within it and the len of the string itself.
// Return: None
{
    memset(key, 0x00, sizeof(key));
    memset(value, 0x00, sizeof(value));
    
    debugf(DEBUG_HUB_CLIENT, "command_handle_debug: %s", a_string);
    
    // Update the local vars with the key and value info
    find_key_and_value(a_string, a_len, (char*)&key, &key_len, (char*)&value, &value_len);
    
    // liveview on/off
    if (memcmp(key, "liveview", 8) == 0)
    {
        if (memcmp(value, "on", 2) == 0)
        {                
            g_liveview = true;
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"LIVEVIEW ON", strlen("LIVEVIEW ON"));
        }
        else if (memcmp(value, "off", 3) == 0)
        {
            g_liveview = false;
            RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)"LIVEVIEW OFF", strlen("LIVEVIEW OFF"));
        }
    }
    
    // Turn on additional tracing
    else if (memcmp(key, "on", 2) == 0)
    {
        debug_sources |= Inst_StringToInt(value, value_len);
        debugf(DEBUG_GENERAL, "debug_sources = %X", debug_sources);
    }
    
    // Turn off tracing
    else if (memcmp(key, "off", 3) == 0)
    {
        uint32_t flippedNumber = ~Inst_StringToInt(value, value_len);
        debugf(DEBUG_GENERAL, "debug_sources = %X", debug_sources);
        
        debug_sources &= flippedNumber;
    }
    
    // Dump the debug tracing every...
    else if (memcmp(key, "timer", 5) == 0)
    {
        udp_socket.timer = Inst_StringToInt(value, value_len) * 1000;
        debugf(DEBUG_GENERAL, "timer = %d", udp_socket.timer);
    }
    
    // Turn safe mode on/off
    else if (memcmp(key, "safe_mode", 9) == 0)
    {

    }
}



void build_reply(char* a_message, uint32_t a_len)
//*******************************
// Desc: Build the reply to send on the TCP. If there is already data to send then add a ,
// Inputs: String to add
// Return: None
{
    uint32_t _msg_len = RingBuffer_Count(&rb_tcp_tx);
    
    if (_msg_len > 0)
        RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)",", strlen(","));
    
    RingBuffer_CopyIn(&rb_tcp_tx, (uint8_t*)a_message, a_len);
}

void discover_response(RingBuffer* a_rb)
//**************************************
// Desc: The discover command can be received via UDP or TCP so build standard response.
// Inputs: ring buffer to put the data in.
// Return: None
{
    char _temp[30] = {0};
    
    debugf(DEBUG_GENERAL, "discover_response");
    
    RingBuffer_CopyIn(a_rb, (uint8_t*)"discover", 8);
        
    // Add the mac address.
    memset(_temp, 0x00, 30);
    snprintf(_temp, 60,",mac=%.2X-%.2X-%.2X-%.2X-%.2X-%.2X",g_mb_conf.mac[0],g_mb_conf.mac[1],
    g_mb_conf.mac[2],g_mb_conf.mac[3], g_mb_conf.mac[4],g_mb_conf.mac[5]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    // Add the serial number
    RingBuffer_CopyIn(a_rb, (uint8_t*)",serial=", strlen(",serial="));
    RingBuffer_CopyIn(a_rb, (uint8_t*)&g_mb_conf.eeprom.serial, strlen((char*)&g_mb_conf.eeprom.serial));
    
    // Add the Firmware version
    RingBuffer_CopyIn(a_rb, (uint8_t*)",firmware=", strlen(",firmware="));
    RingBuffer_CopyIn(a_rb, (uint8_t*)FIRMWARE_VERSION, strlen(FIRMWARE_VERSION));
    
    // Add the location
    RingBuffer_CopyIn(a_rb, (uint8_t*)",location=", strlen(",location="));
    RingBuffer_CopyIn(a_rb, (uint8_t*)&g_mb_conf.eeprom.location, strlen((char*)&g_mb_conf.eeprom.location));
    
    // Add the IP
    RingBuffer_CopyIn(a_rb, (uint8_t*)",ip=", strlen(",ip="));
    snprintf(_temp, 60,"%d.%d.%d.%d",g_mb_conf.eeprom.ip[0], g_mb_conf.eeprom.ip[1], g_mb_conf.eeprom.ip[2], g_mb_conf.eeprom.ip[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    // Add the SN
    RingBuffer_CopyIn(a_rb, (uint8_t*)",sn=", strlen(",sn="));
    snprintf(_temp, 60,"%d.%d.%d.%d",g_mb_conf.eeprom.sn[0], g_mb_conf.eeprom.sn[1], g_mb_conf.eeprom.sn[2], g_mb_conf.eeprom.sn[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    // Add the GW
    RingBuffer_CopyIn(a_rb, (uint8_t*)",gw=", strlen(",gw="));
    snprintf(_temp, 60,"%d.%d.%d.%d",g_mb_conf.eeprom.gw[0], g_mb_conf.eeprom.gw[1], g_mb_conf.eeprom.gw[2], g_mb_conf.eeprom.gw[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    // Add the GPS port number
    RingBuffer_CopyIn(a_rb, (uint8_t*)",gps_port=", strlen(",gps_port="));
    snprintf(_temp, 60,"%d", g_mb_conf.eeprom.gps_port);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    #if 0
    // Add the time
    rtc_read();
    sprintf(_temp, "%lu.", (unsigned long)g_unixtime);
    Inst_Long2String(&_temp[11],g_unixtime_ms,1,0);
    
    RingBuffer_CopyIn(a_rb, (uint8_t*)",time=", strlen(",time="));
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    #endif
    
    // Add the bit flags
    RingBuffer_CopyIn(a_rb, (uint8_t*)",bit_flags=", strlen(",bit_flags="));
    snprintf(_temp, 20, "%X", g_mb_conf.eeprom.bit_flags[0]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
}

#ifdef LEGACY

void discover_response_legacy(RingBuffer* a_rb)
//**********************
{
    char _temp[100] = {0};
    char _time_temp[20] = {0};
    
    debugf(DEBUG_HUB_CLIENT, "discover_response_legacy");
        
    RingBuffer_CopyIn(a_rb, (uint8_t*)"<response><name>status</name><params>", strlen("<response><name>status</name><params>"));

    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>mac</name><value>%.2X-%.2X-%.2X-%.2X-%.2X-%.2X</value></param>", 
        g_mb_conf.mac[0],g_mb_conf.mac[1], g_mb_conf.mac[2],g_mb_conf.mac[3], g_mb_conf.mac[4],g_mb_conf.mac[5] );
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>serial</name><value>%s</value></param>", (char*)&g_mb_conf.eeprom.serial);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>firmware</name><value>%s</value></param>", (char*)&FIRMWARE_VERSION);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>location</name><value>%s</value></param>", (char*)&g_mb_conf.eeprom.location);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>ip</name><value>%d.%d.%d.%d</value></param>", 
        g_mb_conf.eeprom.ip[0],g_mb_conf.eeprom.ip[1], g_mb_conf.eeprom.ip[2],g_mb_conf.eeprom.ip[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>sn</name><value>%d.%d.%d.%d</value></param>", 
        g_mb_conf.eeprom.sn[0], g_mb_conf.eeprom.sn[1], g_mb_conf.eeprom.sn[2], g_mb_conf.eeprom.sn[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>gw</name><value>%d.%d.%d.%d</value></param>", 
        g_mb_conf.eeprom.gw[0], g_mb_conf.eeprom.gw[1], g_mb_conf.eeprom.gw[2], g_mb_conf.eeprom.gw[3]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
    
    #if 1
    
    rtc_read();
    
    memset(_time_temp, 0x00, sizeof(_time_temp));
    
    // UNIX TIME
    sprintf(_time_temp, "%lu.", (unsigned long)g_unixtime);
    Inst_Long2String(&_time_temp[11],g_unixtime_ms,1,0);
    
    #endif
    
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>time</name><value>%s</value></param>", _time_temp);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
                                            
    memset(_temp, 0x00, sizeof(_temp));
    snprintf(_temp, 100, "<param><name>bit_flags</name><value>%X</value></param>", g_mb_conf.eeprom.bit_flags[0]);
    RingBuffer_CopyIn(a_rb, (uint8_t*)_temp, strlen(_temp));
        
    RingBuffer_CopyIn(a_rb, (uint8_t*)"</params></response>", strlen("</params></response>"));
}

#endif
