//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"
#include "inc/hw_nvic.h"
#include "inc/hw_sysctl.h"

// TivaWare driverlib
#include "driverlib/uart.h"
#include "driverlib/watchdog.h"
#include "driverlib/timer.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"
#include "driverlib/interrupt.h"
#include "driverlib/systick.h"
#include "driverlib/rom.h"

// LWIP
#include "lwip/ip_addr.h"
#include "lwip-2.1.2/src/include/lwip/ip4_addr.h"

// FATFs
#include "fatfs/src/ff.h"

// Include the text replace for hardware definitions
#include "hardware.h"
#include "software.h"

// Include the Firmware details file


// Must be called before micro SD
#include "drivers/drvr_ring_buffer.h"

// Instrumentel HW Layer
#include "hw_layer/hw_gpio.h"
#include "hw_layer/hw_watchdog.h"
#include "hw_layer/hw_micro_sd.h"
#include "hw_layer/hw_rtc.h"

// Instrumentel Drivers
#include "drivers/drvr_bl_uart.h"
#include "drivers/drvr_debug_sw.h"
#include "drivers/drvr_udp.h"
#include "drivers/lwiplib.h"
#include "drivers/drvr_uart_codec.h"


// Instrumentel Application
#include "application/interrupt_handler.h"
#include "application/app_uart_db_api.h"
#include "application/app_checksum.h"
#include "application/main.h"
//#include "application/app_uartmobo.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************



//****************************************************************************
//
// Global Variables
//
//****************************************************************************

uint32_t g_sd_card_fail = 0;
uint32_t g_last_rtc_read = 0;
uint32_t g_db_transfer_timer = 0;
uint32_t g_cs_timer = 0;
uint32_t g_hard_trigger_timer = 0;
uint32_t g_debug_alive_timer = 0; // for in teh main thread 

//****************************************************************************
//
// Local Variables
//
//****************************************************************************

//static char g_uart_uuid_temp[READER_UUID_LENGTH+1];		// Temporary UUID Store
//volatile int g_uuid_pointer;													// Temporary UUID pointer

uint32_t counter = 0;

#if TEST_UART
char _test[24] = {0xA5, 0x5A, 18, 0, 0x85, 0x00, 'D', 'a', 'v', 'e', ' ', 'i', 's', ' ', 'a', ' ', 'l', 'e', 'g', 'e', 'n', 'd', 0xD3, 0x6D};
uint8_t _count = 0x30;
    uint16_t _crc;
    
uint16_t test_gen_crc_16(uint8_t *data, uint16_t size)
//*****************************************************
{
    uint16_t out = 0;
    int bits_read = 0, bit_flag;

    /* Sanity check: */
    if(data == NULL)
        return 0;

    while(size > 0)
    {
        bit_flag = out >> 15;

        /* Get next bit: */
        out <<= 1;
        out |= (*data >> bits_read) & 1; // item a) work from the least significant bits

        /* Increment bit counter: */
        bits_read++;
        if(bits_read > 7)
        {
            bits_read = 0;
            data++;
            size--;
        }

        /* Cycle check: */
        if(bit_flag)
            out ^= CRC16;

    }

    // item b) "push out" the last 16 bits
    uint16_t i;
    for (i = 0; i < 16; ++i) {
        bit_flag = out >> 15;
        out <<= 1;
        if(bit_flag)
            out ^= CRC16;
    }

    // item c) reverse the bits
    uint16_t crc = 0;
    i = 0x8000;
    int j = 0x0001;
    //int ii = 0x7FFF;
    for (; i != 0; i= i>>1, j <<= 1) {
        if (i & out) crc |= j;
    }

    return crc;
}

#endif
    

//****************************************************************************
//
// Global Functions
//
//****************************************************************************


void interrupt_init(void)
//************************
{
    // Enable processor interrupts.
    //IntMasterEnable();
    //
    // Set the interrupt priorities.  We set the SysTick interrupt to a higher
    // priority than the Ethernet interrupt to ensure that the file system
    // tick is processed if SysTick occurs while the Ethernet handler is being
    // processed.  This is very likely since all the TCP/IP and API work is
    // done in the context of the Ethernet interrupt. We also set the UART Interrupt lower 
    // than the Ethernet and the Systick, as when running code on a reader
    // This will be called 30 times every 15ms or so.
    //
    IntPriorityGroupingSet(4);
    
    //IntPrioritySet(INT_UART0_TM4C129, UART_INT_PRIORITY);
    IntPrioritySet(INT_EMAC0_TM4C129, ETHERNET_INT_PRIORITY);
    IntPrioritySet(FAULT_SYSTICK, SYSTICK_INT_PRIORITY);
    
    debugf(DEBUG_GENERAL, "System Interrupts enabled...");
    
    /* Enable all processor interrupts. */
    IntMasterEnable();	
}

#define TEST_TRIGGER

#ifdef TEST_TRIGGER

uint32_t test_trigger = 0;

#endif

// System tick operates as a 1ms interrupt
void sys_tick_int_handler( void )
//*******************************
{
    //watchdog_safe_flag(WDG_SYSCALL);
    //test_counter++;
        
    //heartbeat
    if (++counter >= 1000)
    {
        counter = 0;
        //debugf(DEBUG_GENERAL, "sys, Dave is a legend.");
        
        //udp_send_packets("sys, Dave is a legend.", strlen("sys, Dave is a legend."), (struct ip4_addr*)IP4_ADDR_BROADCAST, 46787);
                
        // Used to ensure the RTC is read correctly.
        //g_seconds_since_last_read++;
         
#if 0        
        if (++test_trigger == 60)
        {
            test_trigger = 0;
            
            //debugf(DEBUG_GENERAL, "Set the trigger.");
            //g_triggered = true;
            //g_hs_trigger = true;
        }
        
        #if TEST_UART
        
        debugf(DEBUG_GENERAL, "Rx test data.");
        
        _test[5] = _count;
        _crc = test_gen_crc_16((uint8_t*)&_test[4], 18);
        
        _test[22] = _crc & 0xFF;
        _test[23] = (_crc >> 8) & 0xFF;
                
        // Loop while characters are available
        for (uint32_t x = 0; x < sizeof(_test); x++)
        {
            // Feed byte into the Decoder!
            UARTDecoder_DecodeMessage(&g_UARTDaughterDecoder, _test[x]);        
        }
        
        if (++_count == 0x3A)
            _count = 0x30;
        #endif
#endif
    }
    
    // Timers
    if (g_last_rtc_read)
        g_last_rtc_read--;

    if (g_db_transfer_timer)
        g_db_transfer_timer--;
    
    if (g_cs_timer)
        g_cs_timer--;
    
    if (g_hard_trigger_timer)
        g_hard_trigger_timer--;
    
    if(g_debug_alive_timer)
        g_debug_alive_timer--;
		
    // Ticks
    lwIPTimer(1);
    //led_tick(1);
}

//! \brief TCP/IP Host Timer Interrupt
void lwIPHostTimerHandler(void)
/*****************************/
{    

}




//****************************************************************************
//
// Local Functions
//
//****************************************************************************




