//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_ints.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/interrupt.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/flash.h"

// LWIP
#include "lwip-2.1.2/src/include/lwip/ip_addr.h"

// FATFs
#include "fatfs/src/ff.h"

// Include the text replace for hardware definitions
#include "hardware.h"
#include "software.h"
#include "log_numbers.h"

// Include the Firmware details file
#include "firmware_details.h"

// Must be called before others
#include "drivers/drvr_ring_buffer.h"
#include "drivers/drvr_bl_uart.h"

// Instrumentel HW Layer
#include "hw_layer/hw_gpio.h"
#include "hw_layer/hw_uart.h"
#include "hw_layer/hw_watchdog.h"
#include "hw_layer/hw_flash.h"
#include "hw_layer/hw_eeprom.h"
#include "hw_layer/hw_rtc.h"
#include "hw_layer/hw_ethernet.h"
#include "hw_layer/hw_micro_sd.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"
#include "drivers/lwiplib.h"
#include "drivers/drvr_udp.h"
#include "drivers/drvr_tcp.h"
#include "drivers/drvr_micro_sd_interface.h"
#include "drivers/drvr_uart_codec.h"
#include "drivers/drvr_base64.h"
#include "drivers/drvr_bl_uart.h"
#include "drivers/drvr_bl_emac.h"

// Instrumentel Application
#include "application/app_uart_db_api.h"
//#include "application/app_network_interface.h"
#include "application/interrupt_handler.h"
#include "application/app_gps_decoder.h"
#include "application/main.h"
#include "application/app_string_functions.h"

#include "application/app_uartmobo.h"
#include "hw_layer/hw_gpio.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void system_init(void);
//void slave_reset(void);
void db_message_handler(void);
bool gps_trigger_check( void );


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

bool g_safe_mode = false;
bool g_liveview = false;
bool g_ddu_liveview = false;
bool g_soft_trigger = false;
bool g_hard_trigger = false;
uint8_t g_trigger_params[200];
uint32_t g_trig_len;
uint32_t g_sys_clk;

static volatile bool g_firmware_update = false;

#ifdef TEST_RING_BUFFER
const static uint8_t tag_profiles[] = "\x31\x31\x31\x5f\x5f\x5f\x32\x32\x32\x5f\x5f\x5f";
const static uint8_t tag_profiles_2[12][1] = {"1","1","1","_", "_", "_", "2", "2", "2", "_", "_", "_"};
#endif

//****************************************************************************
//
// Local Variables
//
//****************************************************************************


bool daughterboard_on = false;
bool config_accepted = false;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

//void software_update_request_callback(void)
//{
//    g_firmware_update = true;
//}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************

void system_init(void)
//********************
// Desc: Setup the system clock speed, System tick interrupt and enable fault interrupts.
// Inputs: None
// Return: None
{   
    SysCtlMOSCConfigSet(SYSCTL_MOSC_HIGHFREQ);
    
    g_sys_clk = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ | SYSCTL_OSC_MAIN | SYSCTL_USE_PLL | SYSCTL_CFG_VCO_480), PROCESSOR_CLOCK);
                                                                                                         
    IntEnable(FAULT_PENDSV); 
    IntEnable(INT_SYSCTL_TM4C129);
    
    SysTickPeriodSet(PROCESSOR_CLOCK / SYSTICKHZ);

    SysTickEnable();
    SysTickIntEnable();	
}


/*****************************************************************************
*
*** Main
*
*****************************************************************************/    

int main(void)
/************/
{    
    // Disable all interrupts at start-up
    IntMasterDisable();
    
    // This code is needed when using the LM flash programmer as the JTAG hold the 
    // physical reset line low which can cause unspecified behaviour
    for (uint8_t i=0; i<10; i++)
    {
        SysCtlDelay(0xFFFF);
    }
    
    // Initilse ARM System clock and system tick
    system_init();
    
    // Setup the interrupt priorities
    interrupt_init();
    
    // Initilse Debug
    debug_init( DEBUG_GENERAL | DEBUG_TCP );
    debugf(DEBUG_GENERAL, "Starting...");
    debugf(DEBUG_GENERAL, "Sys Clock Speed: %d", g_sys_clk);
        
    // Check if a watchdog reset triggered
    uint32_t reset_cause = SysCtlResetCauseGet();
    
    // Read Configuration Information from flash and EEPROM
    bool _user_reg_empty = flash_init();
    eeprom_init(_user_reg_empty);
    
    rtc_init();
    
    // Initialise the physical ethernet port
    ethernet_init();
    
    // Initialze the lwIP library.
    lwIPInit(PROCESSOR_CLOCK, g_mb_conf.mac, MACRO_IP_ADDR(g_mb_conf.eeprom.ip), MACRO_IP_ADDR(g_mb_conf.eeprom.sn), MACRO_IP_ADDR(g_mb_conf.eeprom.gw), IPADDR_USE_STATIC);
        
    // Initialise the TCP and UDP stacks
    upd_init();
    tcp_api_init();
    //tcp_api_init();
    
    //debug_udp(true);
    
    while(1)
    {
        debugf(DEBUG_GENERAL, "main, Dave is a legend.");
        SysCtlDelay(0xFFFFFFF);
    }
}
