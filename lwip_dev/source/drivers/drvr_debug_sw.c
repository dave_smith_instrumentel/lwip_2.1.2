//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>


// TivaWare includes
#include "inc/hw_types.h"
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/uart.h"
#include "driverlib/gpio.h"
#include "driverlib/sysctl.h"

// LWIP
#include "lwip/ip_addr.h"

// FATFs


// Include the text replace for hardware definitions
#include "hardware.h"
#include "software.h"


// Include the Firmware details file


// Must be called before micro SD

// Instrumentel HW Layer
#include "hw_layer/hw_gpio.h"
#include "hw_layer/hw_uart.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"
#include "drivers/drvr_ring_buffer.h"
#include "drivers/drvr_udp.h"

// Instrumentel Application


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void toggle_led_colour(void);

void debug_interupt( void );
void debug_txdata(const uint8_t* a_buff, int32_t a_bufflen );


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

//! IP Label for debug output
char debug_IP_label[40];

//! Current debug level
unsigned long debug_level_var;
uint32_t debug_sources;

static uint32_t tick_ms;
static Led_Colours_t colour;
static Led_Flashing_t flashing;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void toggle_debug_pin(uint32_t a_pin, bool on)
//******************************
// Desc: 
// Inputs: 
// Return:
{
    if (on)
        GPIOPinWrite( DEBUG_PIN_BASE, a_pin , a_pin );
    else
        GPIOPinWrite( DEBUG_PIN_BASE, a_pin , 0 );
}

void toggle_debug_pin2(void)
//******************************
// Desc: 
// Inputs: 
// Return:
{
    GPIOPinWrite( DEBUG_PIN_BASE, DEBUG_PIN_2 , ~GPIOPinRead( DEBUG_PIN_BASE, DEBUG_PIN_2) );
}

void toggle_debug_pin3(void)
//******************************
// Desc: 
// Inputs: 
// Return:
{
    //GPIOPinWrite( LED_GPIO_BASE, LED_1_PIN , ~GPIOPinRead( LED_GPIO_BASE, LED_1_PIN) );
}

void set_led_pin(uint32_t a_pin, bool a_dir)
//******************************
// Desc: 
// Inputs: 
// Return:
{
    if (a_dir)
    {
        GPIOPinWrite( LED_PIN_BASE, a_pin , a_pin);
    }
    else
    {
        GPIOPinWrite( LED_PIN_BASE, a_pin , 0);
    }
}

//! \brief Debug Initilisation
void debug_init(uint32_t source)
//******************************
// Desc: Setup the UART port for outputting debug information on the UART.
// Inputs: 
// Return:
{    
    uarthw_init(DEBUG_UART);
    
    debug_sources = source;
    
    debug_output(DEBUG_GENERAL, "Debug sources set to %X", debug_sources);
}

static bool debug_udp_output = false;

void debug_udp(bool a_on)
{
    debug_udp_output = a_on;
}

//! \brief Debug Output function
void debug_output(unsigned long source, char *data, ...)
/*************************************************/
{
#ifdef DEBUG_ENABLE
    
    static char preamble[6];
    static char result[3000];
    uint32_t _count = 0;
    va_list args;
    
    memset(preamble, '\0', 6);
    
    va_start(args, data);
    
    if(source & debug_sources)
    {
        // Build the string
        switch (source)
        {
            case DEBUG_GENERAL:         strcpy(preamble, "GEN: ");          break;
            case DEBUG_HUB_CLIENT:      strcpy(preamble, "HUB: ");          break;
            case DEBUG_MICRO_SD:        strcpy(preamble, "SD: ");           break;
            case DEBUG_RTC:             strcpy(preamble, "RTC: ");          break;
            case DEBUG_FLASH:           strcpy(preamble, "FLASH: ");        break;
            case DEBUG_MMC:             strcpy(preamble, "MMC: ");          break;
            case DEBUG_TCP:             strcpy(preamble, "TCP: ");          break;
            case DEBUG_UDP:             strcpy(preamble, "UDP: ");          break;
            case DEBUG_GPS:             strcpy(preamble, "GPS: ");          break;
            case DEBUG_FW70:            strcpy(preamble, "FW70:");          break;
            case DEBUG_RING_BUFFER:     strcpy(preamble, "RB: ");           break;
            case DEBUG_RFID_DECODER:    strcpy(preamble, "RF_DECODER: ");   break;
            case DEBUG_RFID_DDU_SM:     strcpy(preamble, "RFID_DDU_SM: ");  break;
            case DEBUG_CALIB_DATA:      strcpy(preamble, "CALIB_DATA: ");   break;
            case DEBUG_CONFIG_LOAD:     strcpy(preamble, "CONF: ");         break;
            case DEBUG_PULSE:           strcpy(preamble, "PULSE: ");        break;
            case DEBUG_ADC:             strcpy(preamble, "ADC: ");          break;
            case DEBUG_HS_CAPTURE:      strcpy(preamble, "HS_CAP: ");       break;
            case DEBUG_BOOTLOADER:      strcpy(preamble, "BL: ");           break;
            case DEBUG_LWIP:            strcpy(preamble, "LWIP: ");         break;
            default:                    strcpy(preamble, "???: ");          break;
        }
        
        vsprintf(result, data, args);
        va_end(args);
        
        // Send it 
        for(uint32_t i=0; preamble[i]!='\0'; i++)
        {	
            UARTCharPut(DEBUG_UART_BASE, preamble[i]);
        } 
        
        for(uint32_t i=0; result[i]!='\0'; i++)
        {	
            UARTCharPut(DEBUG_UART_BASE, result[i]);
            _count += 1;
        } 

        UARTCharPut(DEBUG_UART_BASE, '\n');
        
        if (debug_udp_output)
        {
            //udp_send_packets(result, strlen(result), (struct ip4_addr*)IP4_ADDR_BROADCAST, 46787);
        }
        /*
        if (udp_socket.timer)
        {
            RingBuffer_CopyIn(&rb_udp, (uint8_t*)&preamble, 5);
            RingBuffer_CopyIn(&rb_udp, (uint8_t*)&result, _count);
            RingBuffer_CopyIn(&rb_udp, (uint8_t*)"\n", 1);
        }
        */
    }
#endif
}


void debug_output_len (char *data, uint32_t len)
//**********************************************
{
    int i;
        
    for(i=0; i<len; i++)
    {	
        UARTCharPut(DEBUG_UART_BASE, data[i]);
    } 
    
    UARTCharPut(DEBUG_UART_BASE,'\n');
}

void debug_output_raw (const char *pcString, ...) 
//**********************************************
{
    int i;

    UARTCharPut(DEBUG_UART_BASE, 'D');
    UARTCharPut(DEBUG_UART_BASE, 'a');
    UARTCharPut(DEBUG_UART_BASE, 'v');
    UARTCharPut(DEBUG_UART_BASE, 'e');
    UARTCharPut(DEBUG_UART_BASE, '\n');
    
    uint32_t len = strlen(pcString);
        
    for(i=0; i<len; i++)
    {	
        UARTCharPut(DEBUG_UART_BASE, pcString[i]);
    } 
}


void set_led_colour(Led_Colours_t a_colour, Led_Flashing_t a_flashing)
//*****************************************
{
    colour = a_colour;
    flashing = a_flashing;
    tick_ms = 0;
    
    switch (a_colour)
    {
        case RED:
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_1 , LED_PIN_1 );
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_2 , 0 );
            break;
        
        case AMBER:
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_1 , LED_PIN_1 );
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_2 , LED_PIN_2 );
            break;
        
        case GREEN:
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_1 , 0 );
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_2 , LED_PIN_2 );
            break;
        
        case LED_OFF:
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_1 , 0 );
            GPIOPinWrite( LED_PIN_BASE, LED_PIN_2 , 0 );
            break;
        
        default:
            break;
    }
}



void led_tick(uint32_t a_tick)
//****************************
{
    tick_ms += a_tick;
    
    switch (flashing)
    {
        case FAST:
            if (tick_ms > 500)
            {
                tick_ms = 0;
                toggle_led_colour();
            }
            break;
        
        case SLOW:
            if (tick_ms > 1000)
            {
                tick_ms = 0;
                toggle_led_colour();
                
            }
            break;
        
        case OFF:
            tick_ms = 0;
            break;
        
        default:
            break;
    }
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************

void toggle_led_colour(void)
//*****************************************
{
    uint32_t _led_1 = GPIOPinRead( LED_PIN_BASE, LED_PIN_1);
    uint32_t _led_2 = GPIOPinRead( LED_PIN_BASE, LED_PIN_2);
    
    if ((_led_1 == 0) && (_led_2 == 0))
    {
        set_led_colour(colour, flashing);
    }
    else
    {
        GPIOPinWrite( LED_PIN_BASE, LED_PIN_1 , 0 );
        GPIOPinWrite( LED_PIN_BASE, LED_PIN_2 , 0 );
    }
}

void debug_format(char *format, ...)
{
    /*
        Hold my beer
    */
    
    static char result[500];
    va_list args;

    va_start(args, format);
    vsprintf(result, format, args);
    va_end(args);
    
    //uartmobodriv_send((const uint8_t*)result, strlen(result), NULL);
    debug_txdata((const uint8_t*)result, strlen(result));
}

void debug_welcomeMessage( void )
{
	debug_format("SW         = %s \n", VERSION_SOFTWARE_STRING);
    debug_format("SW_DESC    = %s \n", VERSION_SOFTWARE_STRING);
	debug_format("Ver        = %02d.%02d.%03d\n", VERSION_YEAR, VERSION_MONTH, VERSION_RELEASE);
    //debug_format("Build date = %s \n", VERSION_BUILDDATE);
}

void debug_setup( void )
{
//    // this will setup the debug port...
//    
//    // grab the UART setup from UART module...
//    SysCtlPeripheralEnable( DEBUG_UART_PERIPH );
//    SysCtlPeripheralEnable( DEBUG_GPIO_PERIPH );
//    //! Set GPIO A0 and A1 as UART pins.
//    GPIOPinTypeUART( DEBUG_GPIO_BASE, DEBUG_GPIO_TXPIN | DEBUG_GPIO_RXPIN );
//    //! Configure the UART for 115,200, 8-N-1 operation.
//    //! Reconfigure the UART for 2,000,000, 8-N-1 operation.
//    UARTConfigSetExpClk( DEBUG_UART_BASE, PROCESSOR_CLOCK, DEBUG_BAUD,
//                        DEBUG_UART_CONFIG );
//    //introduce these when board layout done
//    GPIOPinConfigure(DEBUG_UART_TXCONFPIN);
//    GPIOPinConfigure(DEBUG_UART_RXCONFPIN); 
//    // Enable the UART
//    UARTEnable( DEBUG_UART_BASE );
//    
//    // interrupt on receive data and receive timeout
//    #if DEBUG_UART_INT_ENABLED
//    UARTIntRegister( DEBUG_UART_BASE, debug_interupt);
//    IntPrioritySet(DEBUG_UART_INT, PRIORITY_DEBUG_RX);
//    
//    // and enable the type of interrupt and the interrupt
//	UARTIntEnable( DEBUG_UART_BASE, UART_INT_RX | UART_INT_RT );	
//    #endif
}
void debug_txdata(const uint8_t* a_buff, int32_t a_bufflen )
{
    
    debug_output(DEBUG_FW70, (char*)a_buff);
//    for(int32_t i=0; i<a_bufflen; i++)
//    {
//        UARTCharPut( DEBUG_UART_BASE,a_buff[i]);
//        // Turn the LED off
//        
//    }
}

void debug_flush( void )
{
    // This code will wait until the FIFO is empty
    // useful for getting a whole message before the system executes other code
    
    // Disabled in FW75
    //while(UARTBusy(DEBUG_UART_BASE));
    
}

void debug_interupt(void)
{
//    unsigned long _status;
//    
//    _status = UARTIntStatus(DEBUG_UART_BASE, false);
//    UARTIntClear(DEBUG_UART_BASE, _status);
//    
}

char * debug_byteToASCII(uint8_t a_byte)
{
    static char _result[3];
    _result[0] = 0;
    _result[1] = 0;
    _result[2] = 0;
    
    if(a_byte>=' ' && a_byte <= '~' )
    {
        _result[0] = a_byte;
    }
    else if(a_byte =='\r')
    {
        _result[0] = '\\';
        _result[1] = 'r';
    }
    else if(a_byte =='\n' )
    {
        _result[0] = '\\';
        _result[1] = 'n';
    }
    else
    {
        _result[0] = '.';
    }
    
    return _result;
}

void debug_led(uint32_t a_mode, uint32_t a_offontoggle)
{
    #if 0
    if(a_mode == DEBUGLED_LED1_MODE)
    {
        if(a_offontoggle == DEBUGLED_ON)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_1_PIN , 0xFF );
        }
        else if (a_offontoggle == DEBUGLED_OFF)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_1_PIN , 0x00 );
        }
        else if (a_offontoggle == DEBUGLED_TOGGLE)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_1_PIN , ~GPIOPinRead(LED_GPIO_BASE, LED_1_PIN) );
        }
    }
    else if(a_mode == DEBUGLED_LED2_MODE)
    {
        if(a_offontoggle == DEBUGLED_ON)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_2_PIN , 0xFF );
        }
        else if (a_offontoggle == DEBUGLED_OFF)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_2_PIN , 0x00 );
        }
        else if (a_offontoggle == DEBUGLED_TOGGLE)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_2_PIN , ~GPIOPinRead(LED_GPIO_BASE, LED_2_PIN) );
        }
    }
    else if(a_mode == DEBUGLED_LED3_MODE)
    {
        if(a_offontoggle == DEBUGLED_ON)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_3_PIN , 0xFF );
        }
        else if (a_offontoggle == DEBUGLED_OFF)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_3_PIN , 0x00 );
        }
        else if (a_offontoggle == DEBUGLED_TOGGLE)
        {
            GPIOPinWrite( LED_GPIO_BASE, LED_3_PIN , ~GPIOPinRead(LED_GPIO_BASE, LED_3_PIN) );
        }
    }
    
    #endif
}








