//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

// TivaWare includes


// TivaWare driverlib


// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer


// Instrumentel Drivers
#include "drivers/drvr_ring_buffer.h"
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void RingBuffer_init( RingBuffer * rb, void * pBuf, int32_t bufSize, size_t step_size)
/************************************************************************************/
{
    rb->total_size = bufSize;
    rb->read_index = 0;
    rb->write_index = 0;
    rb->pBuf  = pBuf;
    rb->step_size = step_size;
}

int32_t RingBuffer_Count( RingBuffer * rb )
/*****************************************/
{
    int32_t diff = rb->write_index - rb->read_index;
    
    // If the buffer is full then send the whole thing
    if (rb->buffer_full)
        diff = rb->total_size;
    
    else if (diff < 0)
        diff += rb->total_size;
    
    return diff;
}

int32_t RingBuffer_Space( RingBuffer * rb )
/*****************************************/
{
    uint32_t _space_available;
    
    if (rb->buffer_full)
        _space_available = 0;
    else
        _space_available = rb->total_size - RingBuffer_Count(rb);
    
    return _space_available;
}

void * RingBuffer_GetNextRead( RingBuffer * rb )
/**********************************************/
{
    return (void*) ((uint32_t)rb->pBuf + (rb->read_index * rb->step_size));
}

void RingBuffer_MarkNextRead( RingBuffer * rb )
/*********************************************/
{
    int32_t ri = rb->read_index;
    ri += 1;
    if (ri >= rb->total_size)
        ri = 0;
    rb->read_index = ri;
    
}

void * RingBuffer_GetNextWrite( RingBuffer * rb )
/***********************************************/
{
    return (void*)((uint32_t)rb->pBuf + (rb->write_index * rb->step_size));
}

void RingBuffer_MarkNextWrite( RingBuffer * rb )
/**********************************************/
{
    int32_t wi = rb->write_index;
    wi += 1;
    if (wi >= rb->total_size)
        wi = 0;
    rb->write_index = wi;
    
}

void RingBuffer_Reset(RingBuffer * rb)
//************************************
{
    rb->read_index = 0;
    rb->write_index = 0;
    rb->buffer_full = false;
}

bool RingBuffer_CopyIn( RingBuffer * rb, uint8_t * pBuf, uint32_t len )
/**************************************************************************
*
* Inputs - Ring buffer, buffer to be copied from, len of data to copy.
* Outputs - 1 (success), 0 (failed)
* 
* The function will copy the data from the specified pBuf into the ring 
* buffer (which has already been initialised) and return whether it has 
* been a success or not depending on the available space in the buff.
*
**************************************************************************/
{
    int32_t i, wi;
    uint8_t * p;
    
    // Check if there is any space..
    if (RingBuffer_Space(rb) < len)
    {
        return false; // There is none..
    }
    
    // There is space, start to copy it in.
    wi = rb->write_index;
    
    for(i = 0; i < len; i++)
    {
        p = ((uint8_t*)(rb->pBuf)) + wi;
        *p = pBuf[i];
        wi += 1;

        if (wi >= rb->total_size)
        {
            wi = 0;
        }
    }
    
    rb->write_index = wi;
    
    if (rb->write_index == rb->read_index)
    {
        rb->buffer_full = true;
    }
    else
    {
        rb->buffer_full = false;
    }
    
    return true;
}

uint32_t RingBuffer_CopyOut( RingBuffer * rb, uint8_t * pBuf, uint32_t len )
/**************************************************************************/
{
    int32_t i, ri, wi;
    uint8_t * p;

    wi = rb->write_index;
    ri = rb->read_index;
    
    debugf(DEBUG_RING_BUFFER, "RingBuffer_CopyOut writeIndex = %d, readIndex = %d, buff_size = %d, bufferfull = %d", 
        rb->write_index, rb->read_index, rb->total_size, rb->buffer_full);
    
    // If the indexes are equal and the buffer isnt full, then we have
    // no data to copy out
    if((wi == ri) && (rb->buffer_full == false)) 
    {
        return 0;
    }
    
    // #TODO - Do we want to reject calls to function if len == 0?
    if (len == 0)
    {
        debugf(DEBUG_RING_BUFFER, "RingBuffer_CopyOut, Error, Len == 0");
        return 0;
    }
    
    // Otherwise copy the data to pBuf for the number of bytes in len
    for(i = 0; i < len; i++)
    {        
        p = (uint8_t*)(rb->pBuf) + ri;
        pBuf[i]  = *p;
        ri ++;
        
        // If we hit the end of the buffer, then loop back to the start
        if (ri >= rb->total_size)
            ri = 0;
        
        // have we caught up to where we are in the buffer? Escape
        if(wi == ri)
        {
            i--;
            break;
        }
    }
    
    // Update the read index
    rb->read_index = ri;
    
    if (rb->read_index >= rb->total_size)
        rb->read_index = 0;
    
    // Clear the buffer full flag
    if(rb->buffer_full == true)
    {
        rb->buffer_full = false;
    }
    
    debugf(DEBUG_RING_BUFFER, "!RingBuffer_CopyOut writeIndex = %d, readIndex = %d, buff_size = %d, bufferfull = %d", 
        rb->write_index, rb->read_index, rb->total_size, rb->buffer_full);
    
    return i+1;
}

void write_update(RingBuffer * rb, uint32_t len)
/********************************************************************
*
*** When we write to the SD card, we copy directly to the buffer to 
*** minimise our memory usage, afterwards we call this function to
*** update the indexes correctly
*
********************************************************************/
{
    rb->write_index += len;
    
    if (rb->write_index >= rb->total_size)
        rb->write_index -= rb->total_size;
    
    if (rb->write_index == rb->read_index)
        rb->buffer_full = true;
    else
        rb->buffer_full = false;
}

void debug_ring_buffer(RingBuffer * rb)
/*************************************/
{
    uint32_t *p;
    
    debugf(DEBUG_GENERAL, "debug_ring_buffer");
    debugf(DEBUG_GENERAL, "rb->total_size = %d", rb->total_size);
    
    for (uint32_t x = 0; x < (rb->total_size / rb->step_size); x++)
    {
        p = (uint32_t*)(rb->pBuf) + x;
        
        debugf(DEBUG_GENERAL, "rb[%d]: %X", x, *p);
    }
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************

