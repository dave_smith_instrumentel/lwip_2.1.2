//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdbool.h>

// TivaWare includes
#include "inc/hw_ints.h"
#include "inc/hw_types.h"

// TivaWare driverlib
#include "driverlib/systick.h"

// LWIP
//#include "lwip-1.4.1/src/include/lwip/stats.h"
#include "lwip-2.1.2/src/include/lwip/tcp.h"
//#include "lwip-1.4.1/src/include/lwip/opt.h"
//#include "lwip-1.4.1/src/include/lwip/sys.h"
//#include "lwip-1.4.1/src/include/lwip/mem.h"
//#include "lwip-1.4.1/src/include/lwip/pbuf.h"
//#include "lwip-1.4.1/src/include/lwip/err.h"
//#include "lwip-1.4.1/src/include/lwip/dns.h"
//#include "lwip-1.4.1/src/include/ipv4/lwip/ip.h"
//#include "lwip-1.4.1/src/include/ipv4/lwip/icmp.h"

// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD
#include "fatfs/src/ff.h"
#include "drivers/drvr_ring_buffer.h"

// Instrumentel HW Layer
#include "hw_layer/hw_system_control.h"
#include "hw_layer/hw_ethernet.h"
#include "hw_layer/hw_micro_sd.h"

// Instrumentel Drivers
#include "drivers/lwiplib.h"
#include "drivers/drvr_debug_sw.h"
#include "drivers/drvr_ring_buffer.h"
#include "drivers/drvr_tcp.h"
#include "drivers/drvr_micro_sd_interface.h"

// Instrumentel Application
#include "application/app_network_interface.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

static void close_conn(struct tcp_pcb *pcb, struct API_state *API);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

volatile int gFileReadChunkSize = 1300;

static uint8_t tcp_tx_buffer[1300*10];
RingBuffer rb_tcp_tx;

unsigned long gNetworkSetting;
bool g_do_not_tx;

//****************************************************************************
//
// Local Variables
//
//****************************************************************************

uint32_t packet_size;
bool resend_last_packet;

//! Sturcture for Incomming Connection
static struct API_state *g_API;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void tcp_api_init(void)
// ***************************************************************************
//! \brief API Initilisation
// ***************************************************************************
{
    static struct tcp_pcb *pcb;
    
    #if 0
    // DNS Initilisation
    dns_init();
    #endif

    // TCP/IP Initilisation
    // Main API
    //tcp_init();
    debugf(DEBUG_TCP, "API: TCP/IP Initilised...\0");
    
    // a new tcp_pcb that initially is in state CLOSED
    pcb = tcp_new();
    
    // Bind the pcb to an IP address and port number
    tcp_bind(pcb, IP_ADDR_ANY, 4678);
    
    // Set the PCB so it can accept incoming connections (but not form connections itself.)
    pcb = tcp_listen(pcb);
    
    // Set the function to be called when a connection is formed
    tcp_accept(pcb, conn_accepted);
    
    debugf(DEBUG_TCP, "API: TCP/IP API listening on port 4678...\0");
    RingBuffer_init((RingBuffer*) &rb_tcp_tx, (void *)tcp_tx_buffer, sizeof(tcp_tx_buffer), 1);

    //! Allocate memory for the structure that holds the state of the  connection. 
    g_API = (struct API_state *)mem_malloc(sizeof(struct API_state));

    if (g_API == NULL) 
    {
        debugf(DEBUG_TCP, "No memory for new TCP/IP connection...\0");
    }

    //! Initialize the structure. 
    g_API->State = COMMAND_DEFAULT;
    //API->buf = NULL;
    //API->buf_len = 0;
    g_API->left = 0;
    g_API->retries = 0;
    
    g_API->buf_len = TCP_PAYLOAD_SIZE * 3;
    g_API->buf = mem_malloc(TCP_PAYLOAD_SIZE * 3);
    
    if (g_API->buf == NULL)
    {
        debugf(DEBUG_TCP, "No memory for API buffer.");
    }
}

//! \brief Send Data function
void send_packets(struct tcp_pcb *pcb, struct API_state *API, RingBuffer* a_rb, int datalength)
// ***************************************************************************
//! \brief Send Data function
//! \param Connection Buffer
//! \param Connection Arguments
//! \param pointer of data string to send
//! \param lenght of data to send
// ***************************************************************************
{
    unsigned long _buffer_space_before, _packet_size;
    err_t err;
   
    
    #if 0
    unsigned long buffer_space_after;
    debugf(DEBUG_TCP, "TCP local IP: %d.%d.%d.%d",(pcb->local_ip.addr & 0x000000FF),
                                                   (pcb->local_ip.addr & 0x0000FF00) >> 8,
                                                   (pcb->local_ip.addr & 0x00FF0000) >> 16,
                                                   (pcb->local_ip.addr & 0xFF000000) >> 24 );
    debugf(DEBUG_TCP, "TCP remote IP: %d.%d.%d.%d",(pcb->remote_ip.addr & 0x000000FF),
                                                   (pcb->remote_ip.addr & 0x0000FF00) >> 8,
                                                   (pcb->remote_ip.addr & 0x00FF0000) >> 16,
                                                   (pcb->remote_ip.addr & 0xFF000000) >> 24 );
    
    debugf(DEBUG_TCP, "TCP Buff addr: %X, buff len: %X", API->buf, API->buf_len);
    #endif
    
    #if 0
    // API buff is now set when the connection is accepted.
    
    // Do we have a buffer
    if(g_API->buf == NULL)
    {
        // NO, make one.
        debugf(DEBUG_TCP, "API: TCP/IP No Buffer, creating...\0");
        g_API->buf_len = tcp_mss(pcb);
        g_API->buf = mem_malloc(tcp_mss(pcb));
        // avail_len = tcp_mss(pcb);
        
        debugf(DEBUG_TCP, "TCP Buff addr: %X, buff len: %X", API->buf, API->buf_len);
        
        if (g_API->buf == 0)
        {
            debugf(DEBUG_TCP, "We haven't been able to alloc any mem so exit out.");
            return;
        }
    }
    else
    {
        // How much space on the stack?
        //avail_len = tcp_mss(pcb);
        //debugf(DEBUG_TCP, "API: TCP/IP Buffer valid. Avail_len: %X", avail_len);
        //debugf(DEBUG_TCP, "TCP Buff addr: %X, buff len: %X", API->buf, API->buf_len);
    }
    #endif

    // Blank the TCP live buffer
    memset(g_API->buf,'\0',g_API->buf_len);
    //debugf(DEBUG_TCP, "API: Not Get files...\0");
    
    // Send Data
    if(datalength < 0)
    {      
        debugf(DEBUG_TCP, "Can't send nothing!");
        return;
    }
    
    // Check how much space we have in the live TCP buffer
    _buffer_space_before = tcp_sndbuf(pcb);
    
    #if 0
    
    // Leave atleast 2 packets inside the buffer
    while ((datalength > 0) && (_buffer_space_before > (gFileReadChunkSize * 2)))
    {
        _buffer_space_before = tcp_sndbuf(pcb);
       
        if (datalength >= 1300)
            _packet_size = 1300;
        else
            _packet_size = datalength;
        
        // Copy out of the TCP ring buffer to the live buffer
        RingBuffer_CopyOut(&rb_tcp_tx, (uint8_t*)API->buf, _packet_size); 
        
        // Send the data!
        err = tcp_write(pcb, API->buf, _packet_size, TCP_WRITE_FLAG_COPY);
        
        // Dump the TCP error on the UART port
        if (err)
        {
            debugf(DEBUG_TCP, "tcp_write err = %d", err);
            break;
        }
        
        // Check the buffer size after
        buffer_space_after = tcp_sndbuf(pcb);
                
        debugf(DEBUG_TCP, "send_len: %d, before: %d, after: %d", _packet_size, _buffer_space_before, buffer_space_after);
        
        if (buffer_space_after >= _buffer_space_before)
        {
            debugf(DEBUG_TCP, "API: breaking as buffer_space >= prevBufSpace (INST) before: %d, after: %d", _buffer_space_before, buffer_space_after);
            break;
        }
        else
        {
            debugf(DEBUG_TCP, "API: chunk sent onto TCP (INST) ");
            datalength -= _packet_size;
        }
        
        // Flush TCP Buffer        
        err = tcp_output(pcb);

        API->retries = 0;
    }
    
    #else
    
    _buffer_space_before = tcp_sndbuf(pcb);
    debugf(DEBUG_TCP, "We have %d bytes free to send data and %d bytes to be sent", _buffer_space_before, datalength);
    
    // Check to see if we have more data to send than buffer space
    if (datalength > g_API->buf_len)
    {
        _packet_size = API->buf_len;
    }
    else
    {
        // Set for all the data to be transferred
        _packet_size = datalength;
    }
    
    // Check how much data is free in the TCP buffer and reduce down again if needs be
    if (_buffer_space_before < _packet_size)
    {
        _packet_size = _buffer_space_before; // Leave a spare TCP packet in the buff at all times
        
        if (_packet_size == 0)
        {
            debugf(DEBUG_TCP, "If packet size has been set to zero then we have no space to load data.");
            return;
        }
    }
    
    debugf(DEBUG_TCP, "Assessed the amount of space on TCP stack and set buff load len: %d", _packet_size);

    // Copy out of the TCP ring buffer to the live buffer
    RingBuffer_CopyOut(&rb_tcp_tx, (uint8_t*)g_API->buf, _packet_size); 
    
    // Stack the data ready to be transmitted!
    err = tcp_write(pcb, g_API->buf, _packet_size, TCP_WRITE_FLAG_COPY);
    
    // Dump the TCP error on the UART port
    if (err)
    {
        debugf(DEBUG_TCP, "tcp_write err = %d", err);
        return;
    }
    
    #if 0
    
    // Force TCP Buffer transmission     
    err = tcp_output(pcb);
    
    if (err)
    {
        debugf(DEBUG_TCP, "tcp_output err = %d", err);
        return;
    }
    
    #endif

    g_API->retries = 0;
    
    #endif

    return;
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


//! \brief API Close connection
static void close_conn(struct tcp_pcb *pcb, struct API_state *API)
/****************************************************************/
{
    err_t err;
    
    tcp_arg(pcb, NULL);
    tcp_sent(pcb, NULL);
    tcp_recv(pcb, NULL);
    tcp_err(pcb, NULL);
    tcp_poll(pcb, NULL, 1);

    /*
    if(API->buf)
    {
        mem_free(API->buf);
    }

    mem_free(API);
    */
    err = tcp_close(pcb);
    
    if(err != ERR_OK)
    {
        // Log Error
        debugf(DEBUG_TCP, "API: TCP/IP Error closing connection...\0");
    }

    debugf(DEBUG_TCP, "API: TCP/IP Connection Closed...");
    
    gNetworkSetting = NETWORK_DEFAULT;
}


// ***************************************************************************
// 
// The following group of functions are local, but have pointers at 
// them which are used in tcp init so the functions are called within 
// the tcp interrupt
//
// ***************************************************************************

static err_t conn_accepted(void *arg, struct tcp_pcb *pcb, err_t err)
// ***************************************************************************
//! \brief API New Connection
//! \param Connection Arguments
//! \param Connection Buffer
//! \param Connection Status
//! \return Connection Status
// ***************************************************************************
{
    debugf(DEBUG_TCP, "New TCP/IP Connection.");
    
    //! For this connection Tell lwIP:
    //! We Have accepted connection
    tcp_accepted(pcb);
    //! - what type of structure this connection has
    tcp_arg(pcb, g_API);
    //! - where to go when new data is recieved
    tcp_recv(pcb, packet_received);
    //! - where to go on an Error
    tcp_err(pcb, conn_error);
    //! - where to go on application poll
    tcp_poll(pcb, conn_poll, 0);
    //! - where to go when data is sent
    tcp_sent(pcb, packet_ack);
    
    return ERR_OK;
}

static err_t packet_received(void *arg, struct tcp_pcb *pcb, struct pbuf *p, err_t err)
// ***************************************************************************
//! \brief API New Data Recieved
//! \param Connection Arguments
//! \param Connection Buffer
//! \param Connection Status
//! \return Connection Status
// ***************************************************************************
{
    const uint32_t RX_BUFF_LEN = 1501;
    
    static char RXdataTemp[RX_BUFF_LEN];
    static struct API_state *API;
    
    int32_t actualLen;
    struct pbuf *pBuf;
    int32_t _tcp_err = 0;

    debugf(DEBUG_TCP, "packet_received");
    API = arg;
    pBuf = p;

    memset(RXdataTemp, '\0', RX_BUFF_LEN);
    
    if (err == ERR_OK && pBuf != NULL)
    {
        Inst_PBufIntoArray(p, (int8_t*)RXdataTemp, RX_BUFF_LEN, &actualLen, &pBuf);
        
        debugf(DEBUG_TCP, "Recieved %d bytes of data.", actualLen);
        debugf(DEBUG_TCP, "API: TCP/IP Data is:= %s", (char*)&RXdataTemp);
        
        // Inform TCP that we have taken the data.
        tcp_recved(pcb, p->tot_len);
        
        // Free buffer
        pbuf_free(p);
        
        // Parse the TCP command and decide what to do
        _tcp_err = parse_tcp_command(RXdataTemp, actualLen, API);
        
        // If we have a TCP error
        if (_tcp_err != 0)
            debugf(DEBUG_TCP, "_tcp_err = %d", _tcp_err);
    }

    // Something has gone wrong so kill the connection
    if ((err == ERR_OK && p == NULL) || (_tcp_err == TCP_KILL))
    {
        debugf(DEBUG_TCP, "Closing Connection.");
        close_conn(pcb, g_API);
    }

    return ERR_OK;
}

static err_t packet_ack(void *arg, struct tcp_pcb *pcb, u16_t len)
// ***************************************************************************
//! \brief This function get called for each new Ack of a packet(s)
//! \param Connection Arguments
//! \param Connection Buffer
//! \param len
//! \return Connection Status
// ***************************************************************************
{    
    LWIP_UNUSED_ARG(len);
    g_API->retries = 0;
    // API->buf_len = 0;
    g_API->left = 0;
    
    debugf(DEBUG_TCP, "API: end sent API");
    
    return ERR_OK;
}

static err_t conn_poll(void *arg, struct tcp_pcb *pcb)
//****************************************************
// Desc: This function is called periodically when a TCP connection has been 
//       opened from an external device. It is used to manage data that must 
//       be streamed or handle any actions which must happen after the TCP
//       response has been sent such as REBOOT
// Inputs:  
// Return: 
// ***************************************************************************
//! \brief API poll
//! \param Connection Arguments
//! \param Connection Buffer
//! \return Connection Status
// ***************************************************************************
{
    err_t err;
        
    uint32_t _tx_len = RingBuffer_Count(&rb_tcp_tx);
    
    // If the buffer isnt full and the indexes are the same
    if (((_tx_len == 0) && rb_tcp_tx.buffer_full == false) || g_do_not_tx)
        return ERR_OK;
    
    // If there is data in the TX buffer then send it.
    send_packets(pcb, g_API, &rb_tcp_tx, _tx_len);
    
    // Sometimes we must delay the action of a message until the reply has been sent
    if ((g_API->State == COMMAND_REBOOT) || (g_API->State == COMMAND_FORBOOT))
    {
        Inst_SysCtrlReset();
    }
    
    // When a cct command if received, a flag is set telling the sd_card_service rountine to load data into the ring buffer.
    else if ((g_API->State == COMMAND_READFILE_BUFFER) && read_event.data_available)
    {     
#if 0        
        // If we haven't hit the end of the file then tell the Micro SD routine to read more data
        if (!read_event.file_end)
        {
            add_micro_sd_event(READING);
            //read_event.data_available = false;
        }
#endif
    }

    // TCP functionality on how many attempts to resend the data
    g_API->retries++;

    if (g_API->State==COMMAND_READFILE)
    {
        if(g_API->retries >= 16000)
        {
            gNetworkSetting = NETWORK_DEFAULT;

            tcp_abort(pcb);
            
            return ERR_ABRT;
        }
        else if(g_API->retries >= 4000) 
        {
            gNetworkSetting = NETWORK_DEFAULT;

            close_conn(pcb,g_API);
        }
    }
    // Otherwise a normal time out please
    else
    {
        if(g_API->retries >= 800)
        {
//          Reader_API_Reset(API->State);
            gNetworkSetting = NETWORK_DEFAULT;

            tcp_abort(pcb);
            return ERR_ABRT;
        }
        else if(g_API->retries >= 200) 
        {
            gNetworkSetting = NETWORK_DEFAULT;

            close_conn(pcb,g_API);
        }
    }

    if(err){}
    return ERR_OK;
}

static void conn_error(void *arg, err_t err)
// ***************************************************************************
//! \brief API Connection Error
//! \param Connection Arguments
//! \param Connection Buffer
//! \return Connection Status
// ***************************************************************************
{
    static struct API_state *API;
    LWIP_UNUSED_ARG(err);
    debugf(DEBUG_TCP, "conn_error");
    
    gNetworkSetting = NETWORK_DEFAULT;
    
    //! If we get an error make sure we clear all trace of connection
    if(arg)
    {
        debugf(DEBUG_TCP, "API: TCP/IP Connection: %.4X",(unsigned long)API);
        debugf(DEBUG_TCP, "API: TCP/IP Deleting connection data...\0");
        
        /*
        API = arg;

        if(API->buf)
        {
            mem_free(API->buf);
        }

        mem_free(API);
        */
    } 
    else
    {
        debugf(DEBUG_TCP, "API: TCP/IP No connection data for this connection...\0");
    }
}
