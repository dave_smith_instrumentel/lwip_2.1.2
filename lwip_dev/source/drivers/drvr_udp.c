//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

// TivaWare includes


// TivaWare driverlib


// LWIP
//#include "lwiplib.c"

#if 1
//#include "lwip-2.1.2/src/include/lwip/stats.h"
#include "lwip-2.1.2/src/include/lwip/tcp.h"
#include "lwip-2.1.2/src/include/lwip/udp.h"
//#include "lwip-2.1.2/src/include/lwip/opt.h"
//#include "lwip-2.1.2/src/include/lwip/sys.h"
//#include "lwip-2.1.2/src/include/lwip/mem.h"
//#include "lwip-2.1.2/src/include/lwip/pbuf.h"
//#include "lwip-2.1.2/src/include/lwip/ip.h"
//#include "lwip-2.1.2/src/include/ipv4/lwip/ip_addr.h"
//#include "lwip-2.1.2/src/include/ipv4/lwip/icmp.h"
//#include "lwip-2.1.2/src/include/lwip/err.h"
//#include "lwip-2.1.2/src/include/lwip/dns.h"
#include "lwip-2.1.2/src/include/lwip/ip.h"
#endif

// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_eeprom.h"

// Instrumentel Drivers
#include "drivers/drvr_ring_buffer.h"
#include "drivers/drvr_tcp.h"
#include "drivers/drvr_udp.h"
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application
#include "application/app_network_interface.h"
#include "application/app_gps_decoder.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

static void udp_receive_gps(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);
static void udp_receive_main(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port);

int32_t Inst_PBufIntoArray(struct pbuf *p, int8_t* buff, int32_t bufferLen, int32_t* outputLen, struct pbuf **pEnd);
int32_t pbuf_to_ring_buff(RingBuffer *a_rb, struct pbuf *p);

//****************************************************************************
//
// Global Variables
//
//****************************************************************************

UDPSocket udp_socket;

RingBuffer rb_udp_tx;
uint8_t udp_tx_buff[MAXBUFFERSIZE];

RingBuffer rb_udp_rx;
uint8_t udp_rx_buff[MAXBUFFERSIZE];


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

static struct udp_pcb *udp_main_pcb;
static struct udp_pcb *udp_gps_pcb;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void upd_init(void)
// ****************
{   
    err_t err;
    
    // Setup the UDP ring buffer
    RingBuffer_init((RingBuffer*) &rb_udp_tx, (void *)udp_tx_buff, sizeof(udp_tx_buff), 1);
    RingBuffer_init((RingBuffer*) &rb_udp_rx, (void *)udp_rx_buff, sizeof(udp_rx_buff), 1);
    
    // UDP Initilisation
    udp_init();
    
    // Create the PCB object for the UDP
    udp_main_pcb = udp_new();
    debugf(DEBUG_UDP, "UDP Initilised.");
    
    ip_set_option(udp_main_pcb, SOF_BROADCAST);
    
    // Bind an IP address and port number to the PCB object
    err = udp_bind(udp_main_pcb, IP_ADDR_ANY, 46787);
    debugf(DEBUG_UDP, "udp_bind, err = %d", err);
    
    // Bind a recieve handler function to the PCB object
    debugf(DEBUG_UDP, "API: UDP API listening on port 46787...\0");
    udp_recv(udp_main_pcb, udp_receive_main, 0);
        
    // Liveview and notification centre IP address
    debugf(DEBUG_UDP, "%d:%d:%d:%d", g_mb_conf.eeprom.udp_ip[0], g_mb_conf.eeprom.udp_ip[1], g_mb_conf.eeprom.udp_ip[2], g_mb_conf.eeprom.udp_ip[3]);
        

    // Setup the UDP structure for gps
    udp_gps_pcb = udp_new();    
    udp_bind(udp_gps_pcb, IP_ADDR_ANY, g_mb_conf.eeprom.gps_port);
    udp_recv(udp_gps_pcb, udp_receive_gps, 0);
    
}

void udp_send_main(RingBuffer *a_rb, bool a_broadcast)
//****************************************************
// Desc: UDP packet recieved
// Inputs: *a_rb: Pointer to ring buffer holding the string to transmit. a_broadcast: Whether to send to the connected IP or broadcast.
//         *udpipaddr: Pointer to IP address to send to.
// Return: None
{
    debugf(DEBUG_UDP, "udp_send_main");
    
    err_t err;
    int32_t datalen = RingBuffer_Count(a_rb);
    
    // UDP Strcture for the UDP tx function
    // Buffer for UDP 
    static struct pbuf *pbuf_UPDTX;
    
    // Allocate memory for the Pbuf
    // Connect to remote host
    pbuf_UPDTX = pbuf_alloc(PBUF_TRANSPORT, datalen, PBUF_RAM);
    pbuf_UPDTX->tot_len = datalen;
    pbuf_UPDTX->len = datalen; 
    
    // Pull the data out the buffer
    RingBuffer_CopyOut(a_rb, pbuf_UPDTX->payload, datalen);  
    
    //RingBuffer_CopyOut(a_rb, (uint8_t*)&pbuf_UPDTX->payload, rb_count);  
    debugf(DEBUG_UDP, "Loaded %d bytes into pBuf.", datalen);

    if (a_broadcast)
    {
        err = udp_sendto(udp_main_pcb, pbuf_UPDTX, IP_ADDR_BROADCAST, 46787);
        debugf(DEBUG_UDP, "udp_sendto, err = %d", err);
    }
    else
    {   
        // Send respose
        debugf(DEBUG_UDP, "Sending UDP data from %X:%d to  %X:%d", udp_main_pcb->local_ip, udp_main_pcb->local_port, udp_main_pcb->remote_ip, udp_main_pcb->remote_port);
    
        err = udp_send(udp_main_pcb, pbuf_UPDTX); 
        debugf(DEBUG_UDP, "udp_send, err = %d", err);
    }
    
    udp_disconnect(udp_main_pcb);

    // Free memory used for buffer
    pbuf_free(pbuf_UPDTX);
    
    debugf(DEBUG_UDP, "udp_send_main END");
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************

// ***************************************************************************
// 
// The following group of functions are local, but have pointers at 
// them which are used in udp init so the functions are called within 
// the udp interrupt
//
// ***************************************************************************

static void udp_receive_main(void *arg, struct udp_pcb *upcb, struct pbuf *p, const ip_addr_t *addr, u16_t port)
//*************************************************************************************************************
// Desc: UDP packet recieved
// Inputs: *arg: Pointer to the connection struct. *upcb: Pointer to the UDP struct. *p: Pointer to the data buff
//         *addr: Pointer to IP address UDP packet came from. port: Port number data came from.
// Return: None
{        
    err_t err;
    
    debugf(DEBUG_UDP, "udp_receive_main");
    debugf(DEBUG_UDP, "Remote address: %08X:%d", *addr, port);
    
    if (udp_connect(upcb, addr, port))
        debugf(DEBUG_UDP, "udp_connect, err = %d", err);

    // Move the data out of the pbuf and into the ring buffer
    if (pbuf_to_ring_buff(&rb_udp_rx, p))
    {
        debugf(DEBUG_UDP, "Buffer Exceeded..");
    }

    // Parse the data sent to decide what we need to do.
    parse_udp_command(&rb_udp_rx);
    
    // Free memory used for connection
    if(arg != NULL)
    {
        mem_free(arg);
    }
    
    // Release the pbufs
    pbuf_free(p);
}


static void udp_receive_gps(void *arg, struct udp_pcb *upcb,struct pbuf *p,const ip_addr_t *addr,u16_t port)
/**********************************************************************************************************/
{
    const int tmplen = 500;
    int8_t tmp[tmplen];
    struct pbuf *pBuf;
    int32_t actualLen;
    int32_t r;
    
    debugf(DEBUG_UDP, "udp_receive_gps");

    r = Inst_PBufIntoArray(p, tmp, tmplen, &actualLen, &pBuf);
    
    if (r)
    {
        debugf(DEBUG_UDP, "Buffer Exceeded...\0");
    }
        
    // Parse the GPS data
    if (g_mb_conf.eeprom.bit_flags[1] & BITFLAG_PARSE_GPS)
    {
        //parse_gps_data((char*)tmp, actualLen); 
    }
    else
    {
        //log_gps_data((char*)tmp, actualLen); 
    }
    
    pbuf_free(p);
}

/*
    This will follow a pBuf CHAIN and copy it's contents into the input buffer.
    This will follow the document of following ->Next until tot_len == len. 
    (Not when Next == NULL, this is packet queueing...)
    
    Will determin if the buffer is the buffer is too little or not
*/

int32_t Inst_PBufIntoArray(struct pbuf *p, int8_t* buff, int32_t bufferLen, int32_t* outputLen, struct pbuf **pEnd)
/*****************************************************************************************************************/
{
    int32_t copied = 0;
    int32_t newSize = 0;
    
    do 
    {        
        // Calc if there is enough memory
        newSize = copied + p->len;
        
        if(newSize > bufferLen)
        {
            // Return Fault 
            *outputLen = copied;
            *pEnd = p;
            return 1;
        }
        
        // Space has been determined fine, copy it across
        memcpy(buff, p->payload, p->len);
        buff += p->len;
        copied += p->len;
    }
    while(p->len != p->tot_len && ((p = p->next) != NULL)); // this is intentional short-circuit/lazy evaluation behaviour. 
    
    *outputLen = copied;
    *pEnd = p;
    return 0;
}

/*
    This will follow a pBuf CHAIN and copy it's contents into the ring buffer.
    This will follow the document of following ->Next until tot_len == len. 
    (Not when Next == NULL, this is packet queueing...)
    
    Will determin if the buffer is the buffer is too little or not
*/

int32_t pbuf_to_ring_buff(RingBuffer *a_rb, struct pbuf *p)
//*********************************************************
// Desc: This will follow a pBuf CHAIN and copy it's contents into the ring buffer.
//       This will follow the document of following ->Next until tot_len == len.
//       (Not when Next == NULL, this is packet queueing...)
// Inputs: a_rb: ptr to ring buffer to put the data. p: pointer to buffers holding UDP received data.
// Return: 0: Pass, 1: Fail
{
    int32_t _rb_free_space; // = RingBuffer_Space(a_rb);
    
    do 
    {        
        // Calc if there is enough memory
        _rb_free_space = RingBuffer_Space(a_rb);
        
        if(p->len > _rb_free_space)
        {
            // Return Fault 
            return 1;
        }
                
        // Space has been determined fine, copy it across
        if (RingBuffer_CopyIn( &rb_udp_rx, p->payload, p->len) == false)
        {
            debugf(DEBUG_UDP, "UDP Rx buffer full.");
        }
    }
    while(p->len != p->tot_len && ((p = p->next) != NULL)); // this is intentional short-circuit/lazy evaluation behaviour. 

    return 0;
}
