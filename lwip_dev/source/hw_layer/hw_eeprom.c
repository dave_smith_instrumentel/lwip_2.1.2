//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

// TivaWare includes


// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/rom_map.h"
#include "driverlib/eeprom.h"

// LWIP


// FATFs


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "firmware_details.h"

// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_eeprom.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application



//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void output_eeprom_config(void);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

Config_t g_mb_conf;
//uint8_t g_daughterboard_config[4096];

uint8_t g_daughterboard_config[2048];

uint8_t g_tag_calib[2048];

ActivationZone_t g_activation_zones[NUM_ACTIVATION_ZONES];


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void eeprom_init(bool a_default)
//******************************
// Desc: Init the EEPROM and check the conf hasn't increased above a block size.
//       Read the config and check the IP is valid. If it isn't or the MAC isn't,
//       then load the default config.
// Inputs: Default config if the MAC isn't valid.
// Return: None
{    
    debugf(DEBUG_GENERAL, "eemprom_init");
    
    SysCtlPeripheralEnable(SYSCTL_PERIPH_EEPROM0);
    
    if (sizeof(g_mb_conf) > 0x400)
    {
        debugf(DEBUG_GENERAL, "mb config has exceeded block size.");
        return;
    }
    
    // Init the EEPROM
    EEPROMInit();
    
    // Read out the motherboard config
    eemprom_read();
    
    // We have a mac address but the IP is empty so give it some default info
    if ((g_mb_conf.eeprom.ip[0] == 0xFF) && (g_mb_conf.eeprom.ip[1] == 0xFF) && (g_mb_conf.eeprom.ip[2] == 0xFF) && (g_mb_conf.eeprom.ip[3] == 0xFF))
    {
        a_default = true;
    }
        
    // Replace the Config with a set of default variables.
    if (a_default)
    {
        g_mb_conf.eeprom.ip[0] = STATIC_IP_A; g_mb_conf.eeprom.ip[1] = STATIC_IP_B; 
        g_mb_conf.eeprom.ip[2] = STATIC_IP_C; g_mb_conf.eeprom.ip[3] = STATIC_IP_D;
        
        g_mb_conf.eeprom.sn[0] = STATIC_NM_A; g_mb_conf.eeprom.sn[1] = STATIC_NM_B; 
        g_mb_conf.eeprom.sn[2] = STATIC_NM_C; g_mb_conf.eeprom.sn[3] = STATIC_NM_D;
        
        g_mb_conf.eeprom.gw[0] = STATIC_GW_A; g_mb_conf.eeprom.gw[1] = STATIC_GW_B; 
        g_mb_conf.eeprom.gw[2] = STATIC_GW_C; g_mb_conf.eeprom.gw[3] = STATIC_GW_D;
        
        g_mb_conf.eeprom.bit_flags[0] = 0;
        g_mb_conf.eeprom.bit_flags[1] = 0;
        g_mb_conf.eeprom.bit_flags[2] = 0;
        g_mb_conf.eeprom.bit_flags[3] = 0;
        
        memset((char*)g_mb_conf.eeprom.serial, 0x00, sizeof(g_mb_conf.eeprom.serial));
        memcpy((char*)g_mb_conf.eeprom.serial, "SN999999", 10);
        
        memset((char*)g_mb_conf.eeprom.location, 0x00, sizeof(g_mb_conf.eeprom.location));
        memcpy((char*)g_mb_conf.eeprom.location, "Unassigned", 10);
        
        memset((char*)g_daughterboard_config, 0x00, sizeof(g_daughterboard_config));
        memset((char*)g_tag_calib, 0x00, sizeof(g_tag_calib));
        debugf(DEBUG_GENERAL, "Writing Defaults to EEPROM");
        eemprom_write();        
    }
}

void eemprom_read(void)
//*********************
// Desc: Read the memory block into the config block
// Inputs: None
// Return: None
{
//    uint32_t _offset = 0;
    
    // Read back the motherboard config
    EEPROMRead((uint32_t*)&g_mb_conf.eeprom, 0x000, sizeof(g_mb_conf.eeprom));
    
    // Read back the daughterboard config.
    EEPROMRead((uint32_t*)&g_daughterboard_config, 0x400, 1024);
    EEPROMRead((uint32_t*)&g_daughterboard_config[1024], 0x800, 1024);
	
		EEPROMRead((uint32_t*)&g_tag_calib, 0xC00, 1024);
		EEPROMRead((uint32_t*)&g_tag_calib[1024], 0x1000, 1024);
    
//    _offset = 0xC00;
//    
//    // Write the GPS co-ordinates
//    for (uint8_t x=0; x < NUM_ACTIVATION_ZONES; x++)
//    {
//        EEPROMRead((uint32_t*)&g_activation_zones[x], _offset, sizeof(g_activation_zones[x]));
//        _offset += sizeof(g_activation_zones[x]);
//    }
    
    output_eeprom_config();
}


void eemprom_write(void)
//**********************
// Desc: Read the memory block into the config block.
// Inputs: None
// Return: None
{
//    uint32_t _offset = 0;
    debugf(DEBUG_GENERAL, "eemprom_write, %X", g_mb_conf.eeprom.bit_flags[1]);
    
    // Write the motherboard config
    EEPROMProgram((uint32_t*)&g_mb_conf, 0x000, sizeof(g_mb_conf));
    
    // Write the daughterboard config.
    EEPROMProgram((uint32_t*)&g_daughterboard_config, 0x400, 1024);
    EEPROMProgram((uint32_t*)&g_daughterboard_config[1024], 0x800, 1024);
			
    // calibration stuff might be inserted here
		
    EEPROMProgram((uint32_t*)&g_tag_calib, 0xC00, 1024);
    EEPROMProgram((uint32_t*)&g_tag_calib[1024], 0x1000, 1024);
//    _offset = 0xC00;
    
//    // Write the GPS co-ordinates
//    for (uint8_t x=0; x < NUM_ACTIVATION_ZONES; x++)
//    {
//        EEPROMProgram((uint32_t*)&g_activation_zones[x], _offset, sizeof(g_activation_zones[x]));
//        _offset += sizeof(g_activation_zones[x]);
//    }
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


void output_eeprom_config(void)
//****************************
// Desc: Dump all the flash config in a human readable format on the debug port.
// Inputs: None
// Return: None
{
    uint16_t _db_conf_len = 0;
    uint16_t _db_calib_len = 0;
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM MAC: %02X:%02X:%02X:%02X:%02X:%02X", 
        g_mb_conf.eeprom.mac[0], g_mb_conf.eeprom.mac[1], 
        g_mb_conf.eeprom.mac[2], g_mb_conf.eeprom.mac[3],
        g_mb_conf.eeprom.mac[4], g_mb_conf.eeprom.mac[5]);
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM Location: %s", g_mb_conf.eeprom.location);
    debugf(DEBUG_GENERAL, (char*)"EEPROM Serial Number: %s", g_mb_conf.eeprom.serial);
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM IP: %d.%d.%d.%d", 
        g_mb_conf.eeprom.ip[0], g_mb_conf.eeprom.ip[1], 
        g_mb_conf.eeprom.ip[2], g_mb_conf.eeprom.ip[3]);
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM SN: %d.%d.%d.%d", 
        g_mb_conf.eeprom.sn[0], g_mb_conf.eeprom.sn[1], 
        g_mb_conf.eeprom.sn[2], g_mb_conf.eeprom.sn[3]);
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM GW: %d.%d.%d.%d", 
        g_mb_conf.eeprom.gw[0], g_mb_conf.eeprom.gw[1], 
        g_mb_conf.eeprom.gw[2], g_mb_conf.eeprom.gw[3]);
    
    for(uint8_t x=0; x<NUM_ACTIVATION_ZONES; x++)
    {
        debugf(DEBUG_GENERAL, "EEPROM g_activation_zones[%d] = (%f,%f) (%f,%f) (%f,%f) (%f,%f)", x,
                g_activation_zones[x].longitude_a, g_activation_zones[x].latitude_a,
                g_activation_zones[x].longitude_b, g_activation_zones[x].latitude_b,
                g_activation_zones[x].longitude_c, g_activation_zones[x].latitude_c,
                g_activation_zones[x].longitude_d, g_activation_zones[x].latitude_d);
    }
    
    debugf(DEBUG_GENERAL, (char*)"EEPROM GPS PORT: %d", g_mb_conf.eeprom.gps_port);
    
    for (int32_t x = 0; x < sizeof(g_daughterboard_config); x++)
    {
        if (g_daughterboard_config[x] == 0x00 || g_daughterboard_config[x] == 0xFF)
        {
            debugf(DEBUG_GENERAL, "EEPROM DB Config Found %X at position %d", g_daughterboard_config[x], x);
            _db_conf_len = x;
            break;
        }
    }
		
    for (int32_t y = 0; y <sizeof(g_tag_calib); y++)
    {
        if (g_tag_calib[y] == 0x00 || g_tag_calib[y] == 0xFF)
        {
            debugf(DEBUG_GENERAL, "EEPROM Calibration Data Found %X at position %d", g_tag_calib[y], y);
            _db_calib_len = y;
            break;
        }
    }
    
    debugf(DEBUG_GENERAL, "EEPROM DB config len: %d", _db_conf_len);
    debugf(DEBUG_GENERAL, "DB Config");
    debug_output_len((char*)&g_daughterboard_config, _db_conf_len);
    
    debugf(DEBUG_GENERAL, "EEPROM Calib len: %d", _db_calib_len);
    debugf(DEBUG_GENERAL, "Calibration Data");
	debug_output_len((char*)&g_tag_calib, _db_calib_len);
}
