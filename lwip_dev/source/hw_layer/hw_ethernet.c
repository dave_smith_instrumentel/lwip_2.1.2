//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>

// TivaWare includes
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/emac.h"

// Include the text replace for hardware definitions


// Include the Firmware details file


// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_ethernet.h"

// Instrumentel Drivers


// Instrumentel Application


//****************************************************************************
//
// Prototypes
//
//****************************************************************************


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void ethernet_init(void)
//**********************
// Desc: Initialise the physical pins used to communicate via ethernet
// Inputs: None
// Return: None
{   
    // Enable the Ethernet Controller
    SysCtlPeripheralEnable(SYSCTL_PERIPH_EMAC0);
    SysCtlPeripheralEnable(SYSCTL_PERIPH_EPHY0);
    
    // Reset the Ethernet Controller
    SysCtlPeripheralReset(SYSCTL_PERIPH_EMAC0);
    SysCtlPeripheralReset(SYSCTL_PERIPH_EPHY0);
    
    EMACPHYConfigSet(EMAC0_BASE, (EMAC_PHY_TYPE_INTERNAL | EMAC_PHY_INT_MDIX_EN | EMAC_PHY_AN_100B_T_FULL_DUPLEX));

    // Enable Port F for Ethernet LEDs.
    //  LED0        Bit 0   Output
    //  LED1        Bit 4   Output
    SysCtlPeripheralEnable(SYSCTL_PERIPH_GPIOF);
    GPIOPinConfigure(GPIO_PF0_EN0LED0);
    GPIOPinConfigure(GPIO_PF4_EN0LED1);
    GPIOPinTypeEthernetLED(GPIO_PORTF_BASE, GPIO_PIN_0 | GPIO_PIN_4);
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************
