//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

// TivaWare includes

// TivaWare driverlib
#include "driverlib/flash.h"

// Include the text replace for hardware definitions


// Include the Firmware details file
#include "firmware_details.h"

// Must be called before micro SD
#include "drivers/drvr_ring_buffer.h"


// Instrumentel HW Layer
#include "hw_layer/hw_flash.h"
#include "hw_layer/hw_eeprom.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application
//#include "application/app_network_interface.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void flash_read(uint32_t address, uint32_t size, uint32_t *save_to_var);
void output_flash_config(void);

//****************************************************************************
//
// Global Variables
//
//****************************************************************************

//EEPROMConfig_t g_mb_conf;
//uint8_t g_daughterboard_config[2048];

char g_daughterboard_bin[1024];

//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************


bool flash_init(void)
//*******************
// Desc: Check the one-time-programmable register to see if the MAC address has
//       been set. If not the load a default config, otherwise load all the 
//       config from flash.
// Inputs: None
// Return: true if no user/reg was set in flash
{
    debugf(DEBUG_FLASH, (char*)"flash_init");
    
    uint32_t _user0, _user1;
    
    // Setup the default values
    // Configure the hardware MAC address for Ethernet Controller filtering of
    // incoming packets.
    FlashUserGet(&_user0, &_user1);
    
    if((_user0 == 0xffffffff) || (_user1 == 0xffffffff))
    {
        debugf(DEBUG_FLASH, (char*)"One-time register empty. Load default config.");
        
        // Possibly generate one from UNIQUEID0, UNIQUEID1, UNIQUEID2, UNIQUEID3 registers?
        
        //00:03:93:ec:21:93 
        g_mb_conf.mac[0] = DEFAULT_MAC1;
        g_mb_conf.mac[1] = DEFAULT_MAC2;
        g_mb_conf.mac[2] = DEFAULT_MAC3;
        g_mb_conf.mac[3] = DEFAULT_MAC4;
        g_mb_conf.mac[4] = DEFAULT_MAC5;
        g_mb_conf.mac[5] = DEFAULT_MAC6;
        debugf(DEBUG_FLASH, "mac Default: %02X:%02X:%02X:%02X:%02X:%02X\n", g_mb_conf.mac[0],g_mb_conf.mac[1],g_mb_conf.mac[2],g_mb_conf.mac[3],g_mb_conf.mac[4],g_mb_conf.mac[5]);
        
        return true;			
    }
    else
    {
        g_mb_conf.mac[0] = ((_user0 >>  0) & 0xff);
        g_mb_conf.mac[1] = ((_user0 >>  8) & 0xff);
        g_mb_conf.mac[2] = ((_user0 >> 16) & 0xff);
        g_mb_conf.mac[3] = ((_user1 >>  0) & 0xff);
        g_mb_conf.mac[4] = ((_user1 >>  8) & 0xff);
        g_mb_conf.mac[5] = ((_user1 >> 16) & 0xff);
        debugf(DEBUG_FLASH, "User Reg: %02X:%02X:%02X:%02X:%02X:%02X\n", g_mb_conf.mac[0],g_mb_conf.mac[1],g_mb_conf.mac[2],g_mb_conf.mac[3],g_mb_conf.mac[4],g_mb_conf.mac[5]);
        
        return false;
    }
}

void flash_write(uint32_t a_block)
/*****************************/
{
    uint32_t i;
    uint32_t *_temp;
    uint32_t _offset = 0;
    uint16_t _size;
        
    // Determine whether we are writing the motherboard config or the daughterboard config
    if (FLASH_MOTHERBOARD_CONFIG_START == a_block)
    {
        _temp = (uint32_t*)&g_mb_conf;
        _size = sizeof(g_mb_conf);
    }
    else if (FLASH_DAUGHTERBOARD_CONFIG_START == a_block)
    {
        _temp = (uint32_t*)&g_daughterboard_config;
        _size = sizeof(g_daughterboard_config)/2;
    }
    else if (FLASH_DAUGHTERBOARD_CONFIG_2_START == a_block)
    {
        _temp = (uint32_t*)&g_daughterboard_config[1024];
        _size = sizeof(g_daughterboard_config)/2;
    }
    else
    {
        debugf(DEBUG_FLASH, "Invalid address");
    }
    
    // Erase the block of flash we are working with
    FlashErase(a_block);
    
    // Write to flash
    for(i = 0; i < _size/BYTES_PER_WORD; i++)
    {                        
        FlashProgram((uint32_t*)_temp, a_block + _offset, 8);
        _offset += BYTES_PER_WORD;
        _temp += BYTES_PER_WORD;
    }    
    
    debugf(DEBUG_FLASH, "Finished Writing flash");
}

void flash_read(uint32_t address, uint32_t size, uint32_t *save_to_var)
//*******************************************************************
{
    static unsigned long i;
    uint32_t* addr = (uint32_t*)address;
    
    for(i = 0; i < size; i++)
    {
        save_to_var[i] = *addr;
        
        addr++;
    }
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************


