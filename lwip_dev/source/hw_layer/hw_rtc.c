//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

// TivaWare includes
#include "inc/hw_memmap.h"

// TivaWare driverlib
#include "driverlib/pin_map.h"
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/i2c.h"

// Include the text replace for hardware definitions
#include "hardware.h"
#include "software.h"

// Include the Firmware details file


// Must be called before micro SD
#include "fatfs/src/ff.h"

// Instrumentel HW Layer
#include "hw_layer/hw_rtc.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"

// Instrumentel Application
#include "application/interrupt_handler.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void read_registers(char* a_output);
char read_register(unsigned char reg);
static uint32_t check_rtc_power(void);
static uint8_t wait_till_free(void);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************

struct tm *g_datetime;
struct tm g_file_datetime;
uint32_t g_unixtime, g_unixtime_ms;
uint32_t g_seconds_since_last_read;

DWORD g_RTC_FATTime; // Used by ff.c


//****************************************************************************
//
// Local Variables
//
//****************************************************************************

const uint8_t I2C_BUFF_LEN = 8;


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void rtc_init(void)
//*****************
// Desc: To init RTC we need to setup the I2C to allow comms with the device
// Inputs: system clock to setup I2C clock
// Return: None
{
    char _str_time[TEMP_BUFF_LEN];
    struct tm* _rtc_tmtime;

    // I2C configuration
    SysCtlPeripheralEnable(RTC_I2C_PERIPH);
    SysCtlPeripheralEnable(RTC_GPIO_PERIPH);
    
    GPIOPinConfigure(RTC_I2C_SCL_PIN);
    GPIOPinConfigure(RTC_I2C_SDA_PIN);
    
    // Configure the pads to be open drain.
    GPIOPadConfigSet(I2C_PIN_BASE, I2C_PIN_SDA, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_OD);
    GPIOPadConfigSet(I2C_PIN_BASE, I2C_PIN_SCL, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_OD);
    
    GPIOPinTypeI2C(I2C_PIN_BASE, I2C_PIN_SDA);// Port B 2 y 3
    GPIOPinTypeI2CSCL(I2C_PIN_BASE, I2C_PIN_SCL);
    
    // Initializes the I2C Master block with clock
    // True = 400 kb/s
    // False = 100 kb/s
    I2CMasterInitExpClk(RTC_I2C_BASE, PROCESSOR_CLOCK, true);

    // Get current time from RTC chip
    debugf(DEBUG_RTC, "Get current time");
    uint32_t _read_fault = rtc_read();
    debugf(DEBUG_RTC, "RTC: _read_fault: %d (%s)", _rtc_tmtime, _rtc_tmtime ? "ERROR":"OK");

    _rtc_tmtime = localtime(&g_unixtime);
    memset(_str_time, '\0', sizeof(_str_time));
    debugf(DEBUG_RTC, "RTC Time: %s", asctime(_rtc_tmtime));
    
    if(check_rtc_power() != ERRORFLAG_OK)
    {
        debugf(DEBUG_RTC, "RTC: ERROR - check_rtc_power() != ERRORFLAG_OK");
    }
}


uint32_t rtc_read(void)
//*********************
// Desc: Using the var which monitors seconds since our last read, generate an 
//       expected value so when we read the time, we can check it is close to 
//       what we expect. If it's incorrect then re-attempt the read 5 times.
//       To read the data we send a byte on the I2C and then fill an array which
//       gets parsed into a datetime object.
// Inputs: None
// Return: I2C faults or 0 for pass
{
    unsigned char ret;
    unsigned int _time_zero = 0;
    char _i2c_data[I2C_BUFF_LEN] = {0};
    uint32_t _expected_time;
    int32_t _delta, _attempts = 0;
    static uint32_t _pass = 0, _fail = 0;
    
    debugf(DEBUG_RTC, "Seconds since last read: %d", g_seconds_since_last_read);
    
    // Prep the variables for what we think the time should be
    _expected_time = g_unixtime + g_seconds_since_last_read;
    g_seconds_since_last_read = 0;
    
    do
    {
        char _valid = read_register(FLAG_REG);
        
        // Read the RTC
        _i2c_data[0] = read_register(HUNDRED_SEC_REG);
        _i2c_data[1] = read_register(SEC_REG);
        _i2c_data[2] = read_register(MIN_REG);
        _i2c_data[3] = read_register(HOUR_REG);
        _i2c_data[4] = read_register(DAY_REG);
        _i2c_data[5] = read_register(DATE_REG);
        _i2c_data[6] = read_register(MONTH_REG);
        _i2c_data[7] = read_register(YEAR_REG);
        
        //debugf(DEBUG_RTC, "_valid: %X", _valid & 0x80);
        debugf(DEBUG_RTC, "_i2c_write_buff: [%X, %X, %X, %X, %X, %X, %X, %X]",
            _i2c_data[0], _i2c_data[1], _i2c_data[2], _i2c_data[3],
            _i2c_data[4], _i2c_data[5], _i2c_data[6], _i2c_data[7]);
            
        // Make sure the tm structure is blank.
        g_datetime = localtime(&_time_zero);
        
        // Just to be sure
        g_datetime->tm_sec = 0;
        g_datetime->tm_min = 0;
        g_datetime->tm_hour = 0;
        g_datetime->tm_wday = 0;
        g_datetime->tm_mday = 0;
        g_datetime->tm_mon = 0;
        g_datetime->tm_year = 0;

        // Now popultate the tm structure with data from the RTC chip
        // NOTE: Data from RTC is in BCD format, therefore Least significat nibble needs masking off
        //       and adding to the Most significant nibble, shifted down and mulitplying by 10.
        g_unixtime_ms = 0x0F & (_i2c_data[0]>>4);
        g_datetime->tm_sec =    (((0x0F & (_i2c_data[1]>>4)) * 10) + (0x0F & _i2c_data[1]));
        g_datetime->tm_min =    (((0x0F & (_i2c_data[2]>>4)) * 10) + (0x0F & _i2c_data[2]));
        g_datetime->tm_hour =   (((0x0F & (_i2c_data[3]>>4)) * 10) + (0x0F & _i2c_data[3]));
        g_datetime->tm_wday =   (((0x0F & (_i2c_data[4]>>4)) * 10) + (0x0F & _i2c_data[4]));
        g_datetime->tm_mday =   (((0x0F & (_i2c_data[5]>>4)) * 10) + (0x0F & _i2c_data[5]));
        g_datetime->tm_mon =    (((0x0F & (_i2c_data[6]>>4)) * 10) + (0x0F & _i2c_data[6]));
        g_datetime->tm_year =   (((0x0F & (_i2c_data[7]>>4)) * 10) + (0x0F & _i2c_data[7]));

        // To get the unix time we need to modify the time structure  
        struct tm _temptime = *g_datetime;
        _temptime.tm_mon = _temptime.tm_mon - 1;
        _temptime.tm_year = _temptime.tm_year + 100;
        
        // Update the global unix time variable
        g_unixtime = mktime(&_temptime);
        
        // Calculate the delta between the expected and actual clock reads.
        _delta = _expected_time - g_unixtime;
        
        if ((_delta > 5) || (_delta < -5))
        {
            _fail += 1;
        }
        else
        {
            _pass += 1;
        }
        
        debugf(DEBUG_RTC, "%d/%d, Expected: %d, Actual: %d, Delta: %d", _fail, _pass ,_expected_time, g_unixtime, _delta);
        
        // Prevent the RTC read from locking up
        if (++_attempts > 5)
        {
            debugf(DEBUG_RTC, "Something has gone wrong with the RTC. Re initialise the I2C.");
            //SysCtlPeripheralReset(RTC_I2C_PERIPH);
            //rtc_init();
            break;
        }
    } 
    while((_delta > 5) || (_delta < -5));
    
    // Update the expected time to give more accuracy on the next attempted read. - Only if we read the time successfully.
    if (_attempts <= 5)
    {
        _expected_time = g_unixtime;
    }

    debugf(DEBUG_RTC, "rtc time read %d", g_unixtime);
        
    return ret;
}


//! \brief Write a time to the RTC
uint32_t rtc_write(uint32_t a_time_var)
//***********************************
// Desc: We receive a unix time and have to first parse it into individual bytes to prep to
//       send over the I2C.
// Inputs: None
// Return: I2C faults or 0 for pass
{
    int i;
    unsigned long _i2c_write_buff[I2C_BUFF_LEN];

    // Use the UNIX time the HTTP app recieved and time.h function
    // to convert it to a tm structure. This is done for easy conversion to the BCD format of the RTC
    g_datetime = localtime(&a_time_var);
    debugf(DEBUG_RTC, "RTC: Write ---------------------------------\n");

    // Populate an array and convert time fields to BCD for transmission to RTC
    _i2c_write_buff[0] = (0xF0 & ((g_datetime->tm_sec / 10)<<4)) | (0x0F & (g_datetime->tm_sec % 10));
    _i2c_write_buff[1] = (0xF0 & ((g_datetime->tm_min / 10)<<4)) | (0x0F & (g_datetime->tm_min % 10));
    _i2c_write_buff[2] = (0xF0 & ((g_datetime->tm_hour / 10)<<4)) | (0x0F & (g_datetime->tm_hour % 10));
    _i2c_write_buff[3] = (0xF0 & ((g_datetime->tm_wday / 10)<<4)) | (0x0F & ((g_datetime->tm_wday) % 10));
    _i2c_write_buff[4] = (0xF0 & ((g_datetime->tm_mday / 10)<<4)) | (0x0F & ((g_datetime->tm_mday) % 10));
    _i2c_write_buff[5] = (0xF0 & (((g_datetime->tm_mon + 1) / 10)<<4)) | (0x0F & ((g_datetime->tm_mon + 1) % 10));
    
    // RTC only supports time from year 2000, time.h supports from 1900, and UNIX from 1970,
    // just do a quick chech that date recieved is no earlier than 2000
    if(g_datetime->tm_year<=100)
    {
        // If it is set year to 2000 (0)
        _i2c_write_buff[6]= 0;
    }
    else
    {
        _i2c_write_buff[6] = (0xF0 & (((g_datetime->tm_year-100)/10)<<4))| (0x0F& ((g_datetime->tm_year-100)%10));
    }
    
    debugf(DEBUG_RTC, "%d:%d:%d:%d:%d:%d:%d", _i2c_write_buff[0], _i2c_write_buff[1], _i2c_write_buff[2], _i2c_write_buff[3],
                                                      _i2c_write_buff[4], _i2c_write_buff[5], _i2c_write_buff[6]);
    
    // Set to write mode.
    I2CMasterSlaveAddrSet(I2C0_MASTER_BASE, RTC_I2C_ADDR, RTC_I2C_WRITE);
    
    SysCtlDelay(0xFFFFF);
    
    // Write word address
    // We are not writing 100ths of second, start from address 2 (0x01)
    I2CMasterDataPut(I2C0_MASTER_BASE, RTC_I2C_SECOND_ADDR);
    
    // Setup a multiple byte transimssion
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_START);

    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;

    // Transmit new time to RTC
    for(i=0;i<7;i++)
    {
        SysCtlDelay(0xFFFFF);
        
        // Transmit
        I2CMasterDataPut(I2C0_MASTER_BASE, _i2c_write_buff[i]);
        
        // Send control to say sending next byte
        I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
        
        if (wait_till_free())
            return ERRORFLAG_NOTOKAY;
    }

    // Inform the RTC we have finished transmitting
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);

    debugf(DEBUG_RTC, "RTC: Time written...");
    
    rtc_read();
    

    return ERRORFLAG_OK;
}

DWORD get_fattime (void)
//**********************
// Desc: This is a real time clock service to be called from FatFs module. 
//       Any valid time must be returned even if the system does not support a real time clock.
// Inputs: None
// Return: DWORD Time in FAT-TIME format
{
    return g_RTC_FATTime;
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************

char read_register(unsigned char reg)
//***********************************
// Desc: Read a register of the RTC and return the character.
// Inputs: The register we wish interrogate.
// Return: The character we have read
{
    //specify that we want to communicate to device address with an intended write to bus
    I2CMasterSlaveAddrSet(I2C0_MASTER_BASE, RTC_I2C_ADDR, false);

    //the register to be read
    I2CMasterDataPut(I2C0_BASE, reg);

    //send control byte and register address byte to slave device
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_SEND);

    // Wait until RTC is read for next data
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;

    //read from the specified slave device
    I2CMasterSlaveAddrSet(I2C0_BASE, RTC_I2C_ADDR, true);

    //send control byte and read from the register from the MCU
    I2CMasterControl(I2C0_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);

    // Wait until RTC is read for next data
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;

    //Get the data from the MCU register and return to caller
    return( I2CMasterDataGet(I2C0_BASE));
 }

static uint32_t check_rtc_power(void)
//***********************************
// Desc: Checks if the RTC chip has lost power ever signifying there is a 
//       problem with the battery.
// Inputs: None
// Return: I2C fault or 0 if okay.
{
    static unsigned long _rtc_status;
    
    debugf(DEBUG_RTC, "RTC: Checking RTC power flags...\n");
    
    // Set to write mode.
    I2CMasterSlaveAddrSet(I2C0_MASTER_BASE, RTC_I2C_ADDR, RTC_I2C_WRITE);
    
    // Write word address
    I2CMasterDataPut(I2C0_MASTER_BASE, RTC_I2C_STATUS_ADDR);
    
    // Send single byte
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_SINGLE_RECEIVE);
    
    // Wait until RTC is read for next data
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;
    
    // Set to read mode.
    I2CMasterSlaveAddrSet(I2C0_MASTER_BASE, RTC_I2C_ADDR, RTC_I2C_READ);
    
    // Get Status byte from the RTC chip
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_SINGLE_SEND);
    
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;
    
    _rtc_status = I2CMasterDataGet(I2C0_MASTER_BASE);
    
    if((_rtc_status & RTC_I2C_STATUS_FLAG_OSF) == RTC_I2C_STATUS_FLAG_OSF)
    {
        debugf(DEBUG_RTC, "RTC: status data = %x", _rtc_status);
        debugf(DEBUG_RTC, "RTC: RTC Has lost its power at some point...\n");
    }
    else
    {
        debugf(DEBUG_RTC, "RTC: RTC power okay...\n");
    }
    
    // Then we need to reset the Osc Flag for next time.......
    // Write to RTC chip
    I2CMasterSlaveAddrSet(I2C0_MASTER_BASE, RTC_I2C_ADDR, RTC_I2C_WRITE);
    
    // Write to the status regiser
    I2CMasterDataPut(I2C0_MASTER_BASE, RTC_I2C_STATUS_ADDR);
    
    // Setup a multiple byte transimssion
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_START);

    // Wait until RTC is read for next data
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;
    
    // Transmit clear Osc flag.....
    I2CMasterDataPut(I2C0_MASTER_BASE, RTC_I2C_CLEAR_OSC);
    
    // Send control to say sending next byte
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_CONT);
    
    // Wait until RTC is read for next data
    if (wait_till_free())
        return ERRORFLAG_NOTOKAY;

    // Inform the RTC we have finished transmitting
    I2CMasterControl(I2C0_MASTER_BASE, I2C_MASTER_CMD_BURST_SEND_FINISH);

    return ERRORFLAG_OK;
}

static uint8_t wait_till_free(void)
//**********************************
// Desc: Repeated function used to wait between bytes being sent/received
// Inputs: None
// Return: I2C fault or 0 if okay.
{
    uint32_t x = 0;
    
    // Errata Silicon bug fix. DO NOT REMOVE UNDER PUNISHMENT OF DEATH
    while (!(I2CMasterBusy(I2C0_BASE))) //Wait till end of transaction
    {
        if(++x > RTC_I2C_BUSY_DELAY)
        {
            debugf(DEBUG_RTC, "RTC: ERROR - wait_till_free() - !I2CMasterBusy for too long\n");
            return ERRORFLAG_NOTOKAY;
        }
    }
    
    x = 0;
    
    while (I2CMasterBusy(I2C0_BASE)) //Wait till end of transaction
    {
        if(++x > RTC_I2C_BUSY_DELAY)
        {
            debugf(DEBUG_RTC, "RTC: ERROR - wait_till_free() - I2CMasterBusy for too long\n");
            return ERRORFLAG_NOTOKAY;
        }
    }        
    // Errata Silicon bug fix. DO NOT REMOVE UNDER PUNISHMENT OF DEATH
    
    return ERRORFLAG_OK;
}
