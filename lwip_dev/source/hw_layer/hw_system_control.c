//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <time.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"

// TivaWare driverlib
#include "driverlib/ADC.h"
#include "driverlib/uart.h"
#include "driverlib/flash.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"


// Include the text replace for hardware definitions


// Include the Firmware details file
#include "firmware_details.h"

// Must be called before micro SD


// Instrumentel HW Layer
#include "hw_layer/hw_system_control.h"

// Instrumentel Drivers
#include "drivers/lwiplib.h"
#include "drivers/drvr_debug_sw.h"
//#include "drivers/drvr_bl_emac.h"

// Instrumentel Application
#include "application/main.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************

void Reboot(void);


//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void Inst_SysCtrlReset(void)
{
    HWREG(NVIC_DIS0) = 0xffffffff;
    HWREG(NVIC_DIS1) = 0xffffffff;

    // Also disable the SysTick interrupt.
    SysTickIntDisable();
    
    //HWREG(NVIC_APINT) = NVIC_APINT_VECTKEY | NVIC_APINT_SYSRESETREQ;
    SysCtlDelay(0xFFFFFFF);
    SysCtlReset();
}

void TCPUpdateBegin(void)
{		
    // InstSoftwareUpdateConfighas already configured for bootloader beboot
    Reboot();
}

void UDPUpdateBegin(void)
{		
    // InstSoftwareUpdateConfighas already configured for bootloader beboot
    //FlashErase(FLASH_BOOT_CONF_START);
    Reboot();
}

void Reboot(void)
{
    debugf(DEBUG_GENERAL, "Reboot.");
    
    // Disable all processor interrupts.  Instead of disabling them
    // one at a time (and possibly missing an interrupt if new sources
    // are added), a direct write to NVIC is done to disable all
    // peripheral interrupts.
    HWREG(NVIC_DIS0) = 0xffffffff;
    HWREG(NVIC_DIS1) = 0xffffffff;

    // Also disable the SysTick interrupt.
    SysTickIntDisable();
    IntMasterDisable();

    // Set to not run on PLL
    //SysCtlClockSet(SYSCTL_OSC_MAIN | SYSCTL_XTAL_8MHZ | SYSCTL_SYSDIV_1 | SYSCTL_USE_OSC);

    SysCtlDelay(0xFFFFFF);

    // Reboot the device
    SysCtlReset();	

    // Make Sure we definatly dont do anything else
    while(1);
}

// IP Address 
// 0-3 Hub IP
// 4-7 Router
// 8-11 NM
// 12-15 Server IP
//#define FLASH_HUB_MAC 			FLASH_BOOT_CONF_START
//#define FLASH_HUB_IP 			FLASH_HUB_MAC + 6
//#define FLASH_ROUTE_IP 			FLASH_HUB_IP + 4
//#define FLASH_SNMASK			FLASH_ROUTE_IP + 4
//#define FLASH_TFTP_SERVER_IP	FLASH_SNMASK + 4
//#define FLASH_BIN_FILE		   	FLASH_TFTP_SERVER_IP + 4

int InstDieTemp(void)
{
    uint32_t ulValue;
    debugf(DEBUG_GENERAL, "Getting Die Temp");
    SysCtlPeripheralEnable(SYSCTL_PERIPH_ADC0);
    ADCSequenceConfigure(ADC0_BASE, 0, ADC_TRIGGER_PROCESSOR, 0);
    // ADC_CTL_TS is the temperature sensor...
    ADCSequenceStepConfigure(ADC0_BASE, 0, 0, ADC_CTL_IE | ADC_CTL_END | ADC_CTL_TS);
    ADCSequenceEnable(ADC0_BASE, 0);
    
    // Wait until the sample sequence has completed.
    while(!ADCIntStatus(ADC0_BASE, 0, false))
    { }

    // Trigger the sample sequence.
    ADCProcessorTrigger(ADC0_BASE, 0);
    // Read the value from the ADC.
    ADCSequenceDataGet(ADC0_BASE, 0, &ulValue);
    debugf(DEBUG_SYS, "%d", ulValue);
    
    return (int)ulValue;
}

void InstSoftwareUpdateConfig(unsigned long IP_addresses[16], char *filename)
{
    static uint32_t *raddress;
    static uint32_t address;
    unsigned char *pucTemp;
    uint32_t ulIPAddress,temp_IP[4];
    int i;
    static uint32_t *putdata;
    static char *cputdata, tempfilename[20];
    static uint32_t l_IP_addresses[16];
    
    debugf(DEBUG_GENERAL, "InstSoftwareUpdateConfig %s", filename);

    memset(tempfilename, '\0', 20);
    strncpy(tempfilename, filename, 19);
    
    address = FLASH_HUB_IP;
    
    // Erase Flash
    
    debugf(DEBUG_GENERAL, "Flash Erased, saving bootloader config...");
    debugf(DEBUG_GENERAL, "IP address. %d.%d.%d.%d", IP_addresses[0], IP_addresses[1], IP_addresses[2], IP_addresses[3]);
    
    // Our IP
    // If not specified use ours
    if(IP_addresses[0] == 0)
    {
        ulIPAddress = lwIPLocalIPAddrGet();
        pucTemp = (unsigned char *)&ulIPAddress;
        l_IP_addresses[0] = pucTemp[0];
        l_IP_addresses[1] = pucTemp[1];
        l_IP_addresses[2] = pucTemp[2];
        l_IP_addresses[3] = pucTemp[3];
        debugf(DEBUG_GENERAL, "Using Current IP address. %d.%d.%d.%d", l_IP_addresses[0], l_IP_addresses[1], l_IP_addresses[2], l_IP_addresses[3]);
    }
    else
    {
        // Otherwise, use the one supplied
        for(i=0;i<4;i++)
        {
            l_IP_addresses[i] = IP_addresses[i];
        }

        debugf(DEBUG_GENERAL, "User specified IP address ");	
    }

    // Router Address
    // If not specified use ours
    if(IP_addresses[4]==0)
    {
        ulIPAddress = lwIPLocalGWAddrGet();
        pucTemp = (unsigned char *)&ulIPAddress;
        l_IP_addresses[4] = pucTemp[0];
        l_IP_addresses[5] = pucTemp[1];
        l_IP_addresses[6] = pucTemp[2];
        l_IP_addresses[7] = pucTemp[3];
        debugf(DEBUG_GENERAL, "Using Current IP address. %d.%d.%d.%d", l_IP_addresses[0], l_IP_addresses[1], l_IP_addresses[2], l_IP_addresses[3]);
        debugf(DEBUG_GENERAL, "Using Current Gateway address. %d.%d.%d.%d", l_IP_addresses[4], l_IP_addresses[5], l_IP_addresses[6], l_IP_addresses[7]);
    }
    else
    {
        // Otherwise, use the one supplied
        for(i=4;i<8;i++)
        {
            l_IP_addresses[i] = IP_addresses[i];
        }	

        debugf(DEBUG_GENERAL, "User specified Gateway address...");
    }

    // SN Mask
    if(IP_addresses[8]==0)
    {
        ulIPAddress = lwIPLocalNetMaskGet();
        pucTemp = (unsigned char *)&ulIPAddress;
        l_IP_addresses[8] = pucTemp[0];
        l_IP_addresses[9] = pucTemp[1];
        l_IP_addresses[10] = pucTemp[2];
        l_IP_addresses[11] = 0;//pucTemp[3];
        debugf(DEBUG_GENERAL, "Using Current IP address. %d.%d.%d.%d", l_IP_addresses[0], l_IP_addresses[1], l_IP_addresses[2], l_IP_addresses[3]);
        debugf(DEBUG_GENERAL, "Using Current Gateway address. %d.%d.%d.%d", l_IP_addresses[4], l_IP_addresses[5], l_IP_addresses[6], l_IP_addresses[7]);
        debugf(DEBUG_GENERAL, "Using Current Netmask address. %d.%d.%d.%d", l_IP_addresses[8], l_IP_addresses[9], l_IP_addresses[10], l_IP_addresses[11]);
    }
    else
    {
        // Otherwise, use the one supplied
        for(i=8;i<12;i++)
        {
            l_IP_addresses[i] = IP_addresses[i];
        }

        debugf(DEBUG_GENERAL, "User specified Netmask...");	
    }

    // TFTP Address
    if(IP_addresses[12]==0)
    {
        DEFAULT_TFTPADDRESS(temp_IP);
        l_IP_addresses[12] = temp_IP[0];
        l_IP_addresses[13] = temp_IP[1];
        l_IP_addresses[14] = temp_IP[2];
        l_IP_addresses[15] = temp_IP[3];
        debugf(DEBUG_GENERAL, "Using Current IP address. %d.%d.%d.%d", l_IP_addresses[0], l_IP_addresses[1], l_IP_addresses[2], l_IP_addresses[3]);
        debugf(DEBUG_GENERAL, "Using Current Gateway address. %d.%d.%d.%d", l_IP_addresses[4], l_IP_addresses[5], l_IP_addresses[6], l_IP_addresses[7]);
        debugf(DEBUG_GENERAL, "Using Current Netmask address. %d.%d.%d.%d", l_IP_addresses[8], l_IP_addresses[9], l_IP_addresses[10], l_IP_addresses[11]);
        debugf(DEBUG_GENERAL, "Using Current TFTP address. %d.%d.%d.%d", l_IP_addresses[12], l_IP_addresses[13], l_IP_addresses[14], l_IP_addresses[15]);
    }
    else
    {
        // Otherwise, use the one supplied
        for(i=12;i<16;i++)
        {
            l_IP_addresses[i] = IP_addresses[i];
        }

        debugf(DEBUG_GENERAL, "User specified TFTP Address...");	
    }

    address = FLASH_HUB_IP;

    // Save IP settings
    for(i=0;i<16;i++)
    {
        putdata = &l_IP_addresses[i];
        FlashProgram(putdata, address, 4);
        address += 4;
    }

    if(tempfilename[0] =='\0')
    {
        memcpy(tempfilename,DEFAULT_BLIMAGE_FILENAME, 20);
        debugf(DEBUG_GENERAL, "Using Default filename:= %s", tempfilename);
    }
    else
    {
        debugf(DEBUG_GENERAL, "User specified filename:= %s", tempfilename);
    }

    address = FLASH_BIN_FILE;

    // And file name
    for(i=0;i<20;i++)
    {
        cputdata = &tempfilename[i];
        FlashProgram((uint32_t *)cputdata, address, 4);
        address += 4;
    }
    
    raddress = (uint32_t *)FLASH_HUB_IP;

    for(i=0;i<16;i++)
    {
        l_IP_addresses[i] = *raddress;
        raddress++;
    }

    debugf(DEBUG_GENERAL, "IP: %d.%d.%d.%d", l_IP_addresses[0], l_IP_addresses[1], l_IP_addresses[2], l_IP_addresses[3]);
    debugf(DEBUG_GENERAL, "GP: %d.%d.%d.%d", l_IP_addresses[4], l_IP_addresses[5], l_IP_addresses[6], l_IP_addresses[7]);
    debugf(DEBUG_GENERAL, "NM: %d.%d.%d.%d",l_IP_addresses[8],l_IP_addresses[9],l_IP_addresses[10],l_IP_addresses[11]);
    debugf(DEBUG_GENERAL, "TFTP: %d.%d.%d.%d",l_IP_addresses[12],l_IP_addresses[13],l_IP_addresses[14],l_IP_addresses[15]);

    raddress = (uint32_t *)FLASH_BIN_FILE;
    memset(tempfilename,'\0',20);

    for(i=0;i<19;i++)
    {
        tempfilename[i] = *raddress;
        raddress++;
    }

    debugf(DEBUG_GENERAL, tempfilename);
    
    FlashErase(FLASH_BOOT_CONF_START);
    
    //FlashErase(FLASH_APP_START_ADD);
    
    //software_update_begin(g_sys_clk);
}


//****************************************************************************
//
// Local Functions
//
//****************************************************************************


