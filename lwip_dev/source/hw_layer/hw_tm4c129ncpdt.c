//
// Define all the hardware preihprails availble on the Chip for FW70 to program
//
//
#include <stdint.h>
#include <stdbool.h>
#include "inc/hw_memmap.h"   // For *_BASE
#include "inc/hw_ints.h"        // for INT_*
#include "driverlib/sysctl.h"     // For  SYSCTL_PERIPH_* 
#include "driverlib/gpio.h"         // for GPIO_PIN*
#include "driverlib/pin_map.h"      // for mappings of pin configure e.g. GPIO_PA1_U0TX
#include "driverlib/timer.h"        // for TIMER_*
#include "driverlib/adc.h"          // for ADC_CTL_*

#include "software.h"

#include "hw_layer/hw_uart.h" // for UartChannel
#include "drivers/drvr_fifo.h"  // for AnalogueMap


#include "hw_layer/hw_uart.h"

////
//
// UARTS
//
////
const UartChannel * uart_getHardwareChannels( int32_t * a_count )
{
    const static UartChannel _channels[] = {
        #ifndef __UNIT_TEST_BUILD__
        // Uart 0 - Daughter Board Comms UART - (likely DDU?)
        {
            0,                  
            UART0_BASE,
            SYSCTL_PERIPH_UART0,
            GPIO_PORTA_BASE,
            SYSCTL_PERIPH_GPIOA,
            GPIO_PIN_1,
            GPIO_PIN_0,
            GPIO_PA1_U0TX,    // Pin 34 - TX PA1
            GPIO_PA0_U0RX,    // PIN 33 - RX PA0
            INT_UART0, 
        },
        // Uart 1 - 
        {
            1, 
            UART1_BASE,
            SYSCTL_PERIPH_UART1,
            GPIO_PORTB_BASE,
            SYSCTL_PERIPH_GPIOB,
            GPIO_PIN_1,
            GPIO_PIN_0,
            GPIO_PB1_U1TX,      // Pin 96 - TX PB1
            GPIO_PB0_U1RX,      // Pin 95 - RX PB0 (Alt Pin is 102/Q4)
            INT_UART1,
        },
        // Uart 2
        {
            2, 
            UART2_BASE,
            SYSCTL_PERIPH_UART2,
            GPIO_PORTA_BASE,
            SYSCTL_PERIPH_GPIOA,
            GPIO_PIN_7,
            GPIO_PIN_6,
            GPIO_PA7_U2TX,      // Pin 41 - TX PA7
            GPIO_PA6_U2RX,      // Pin 40 - RX PA6
            INT_UART2,
        },
        // Uart 4
        {
            4, 
            UART4_BASE,
            SYSCTL_PERIPH_UART4,
            GPIO_PORTK_BASE,
            SYSCTL_PERIPH_GPIOK,
            GPIO_PIN_1,
            GPIO_PIN_0,
            GPIO_PK1_U4TX,      // Pin 19 - TX PK1
            GPIO_PK0_U4RX,      // Pin 18 - RX PK0
            INT_UART4,
        },
        // Uart 5
        {
            5, 
            UART5_BASE,
            SYSCTL_PERIPH_UART5,
            GPIO_PORTC_BASE,
            SYSCTL_PERIPH_GPIOC,
            GPIO_PIN_7,
            GPIO_PIN_6,
            GPIO_PC7_U5TX,      // Pin 22 - TX PC7
            GPIO_PC6_U5RX,      // Pin 23 - RX PC6
            INT_UART5,
        }
       
        
        #endif
    };
    
    *a_count  = sizeof(_channels)/sizeof(UartChannel);
    
    return _channels;
}



