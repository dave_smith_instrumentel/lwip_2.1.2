//*****************************************************************************
//
// Include
//
//*****************************************************************************

// Standard Library Includes
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

// TivaWare includes
#include "inc/hw_memmap.h"
#include "inc/hw_ints.h"

// TivaWare driverlib
#include "driverlib/sysctl.h"
#include "driverlib/gpio.h"
#include "driverlib/uart.h"
#include "driverlib/pin_map.h"
#include "driverlib/interrupt.h"

// Include the text replace for hardware definitions
#include "hardware.h"
#include "software.h"

// Include the Firmware details file


// Must be called before uart
#include "software.h"

// Instrumentel HW Layer
#include "hw_layer/hw_uart.h"

// Instrumentel Drivers
#include "drivers/drvr_debug_sw.h"
#include "drivers/drvr_uart_codec.h"

// Instrumentel Application
#include "application/app_uart_db_api.h"


//****************************************************************************
//
// Prototypes
//
//****************************************************************************



//****************************************************************************
//
// Global Variables
//
//****************************************************************************


//****************************************************************************
//
// Local Variables
//
//****************************************************************************
				
//FPGARFID_UART_PERIPH,			
//				FPGARFID_GPIO_PERIPH,			
//				FPGARFID_GPIO_BASE,				
//				FPGARFID_GPIO_TXPIN,       
//				FPGARFID_GPIO_RXPIN,       
//				FPGARFID_UART_INT,         
//				FPGARFID_UART_CONFIG,      
//				FPGARFID_UART_BASE,        
//				FPGARFID_BAUD,             
//				FPGARFID_UART_TXCONFPIN,   
//				FPGARFID_UART_RXCONFPIN,  
//				FPGARFID_UART_INT_ENABLED, 
//				FPGARFID_UART_INT_PRIO 

static uart_init_struct uart_init_data[NUMBER_OF_UARTS] =
{
    {
        DEBUG_UART_PERIPH,
        DEBUG_GPIO_PERIPH,
        DEBUG_GPIO_BASE,
        DEBUG_GPIO_TXPIN,
        DEBUG_GPIO_RXPIN,
        DEBUG_UART_INT,
        DEBUG_UART_CONFIG,
        DEBUG_UART_BASE,
        DEBUG_BAUD,
        DEBUG_UART_TXCONFPIN,
        DEBUG_UART_RXCONFPIN,
        DEBUG_UART_INT_ENABLED,
        DEBUG_UART_INT_PRIO
    },
    {
				ARM_TO_ARM_UART_PERIPH,
        ARM_TO_ARM_GPIO_PERIPH,
        ARM_TO_ARM_GPIO_BASE,
        ARM_TO_ARM_GPIO_TXPIN,
        ARM_TO_ARM_GPIO_RXPIN,
        ARM_TO_ARM_UART_INT,
        ARM_TO_ARM_UART_CONFIG,
        ARM_TO_ARM_UART_BASE,
        ARM_TO_ARM_BAUD,
        ARM_TO_ARM_UART_TXCONFPIN,
        ARM_TO_ARM_UART_RXCONFPIN,
        ARM_TO_ARM_UART_INT_ENABLED,
        ARM_TO_ARM_UART_INT_PRIO
    },
    {
        SPARE_UART_PERIPH,
        SPARE_GPIO_PERIPH,
        SPARE_GPIO_BASE,
        SPARE_GPIO_TXPIN,
        SPARE_GPIO_RXPIN,
        SPARE_UART_INT,
        SPARE_UART_CONFIG,
        SPARE_UART_BASE,
        SPARE_BAUD,
        SPARE_UART_TXCONFPIN,
        SPARE_UART_RXCONFPIN,
        SPARE_UART_INT_ENABLED,
        SPARE_UART_INT_PRIO
    }
};


//****************************************************************************
//
// Global Functions
//
//****************************************************************************

void uarthw_init(uint32_t a_index)
//******************************
// Desc: Initialise the UART periphrial.
// Inputs: a_index, index of the uart to be initialised uart_init_struct uart_init_data[NUM_UARTS]
// Return: None
{
    if (a_index != DEBUG_UART)
    {
        debugf(DEBUG_GENERAL, "Init a new UART module: Index : %d", a_index);
    }

    // Check if input is in range
    if (a_index > NUMBER_OF_UARTS)
    {
        // too high, can not service
        // debugf(DEBUG_UART_HW, "CRITICAL: uart_init called to service index: %d - invalid\n", a_index);
        return;
    }

    // Fetch the _uart structure
    uart_init_struct * _uart = &uart_init_data[a_index];

    // Initialize the UART + GPIO pin perhipheral
    SysCtlPeripheralEnable(_uart->uart_peripheral);
    SysCtlDelay(5);
    SysCtlPeripheralEnable(_uart->gpio_peripheral);
    SysCtlDelay(5);

    // Pin setup  needs both GPIO-Pin-Configure and GPIO-Pin-Tpye-Uart
    GPIOPinConfigure(_uart->rx_conf_pin);
    GPIOPinConfigure(_uart->tx_conf_pin);

    GPIOPinTypeUART(_uart->uart_gpio_base, _uart->tx_pin);
    GPIOPinTypeUART(_uart->uart_gpio_base, _uart->rx_pin);

    // Configure the UART for 8-N-1 operation at given baud.
    UARTConfigSetExpClk(_uart->uart_base, PROCESSOR_CLOCK, _uart->baud, _uart->uart_config);

    // set the num bytes interrupt fire at 1
    UARTFIFOLevelSet(_uart->uart_base, 1, 1); // #TODO set this level into the main _uart-> config
    
    debugf(DEBUG_GENERAL, "UART config'd.");

    // Flush any characters in the RX buffer
    while (UARTCharsAvail(_uart->uart_base))
    {
        uint8_t c = UARTCharGet(_uart->uart_base);
    }

    // Interrupt on receive data and receive timeout
    if (uart_init_data[a_index].interupt_enabled)
    {
        IntEnable(_uart->uart_int);
        debugf(DEBUG_GENERAL, "UART Int config'd.");
    }
    else
    {
        IntDisable(_uart->uart_int);
    }

    UARTIntEnable(uart_init_data[a_index].uart_base, UART_INT_RX | UART_INT_RT);

    // Enable the UART
    UARTEnable(_uart->uart_base);
    
    if (a_index != ARM_TO_ARM_UART)
    {
        debugf(DEBUG_GENERAL, "UART %d initialised", a_index);
    }
    
    debugf(DEBUG_GENERAL, "UART Int config'd.");
}

void uarthw_send(uint32_t a_index, uint8_t *data, uint32_t len)
//***********************************************************
// Desc: Send data on the specified UART port
// Inputs: Index on UART to send data on (All UART's held in array), pointer to 
//         data to send. Lenght of data to send.
// Return: None
{
    // Fetch the _uart structure
    uart_init_struct* _uart = &uart_init_data[a_index];
            
    for (uint32_t i=0; i<len; i++)
    {	
        UARTCharPut(_uart->uart_base, data[i]);
    }
    
    UARTCharPut(_uart->uart_base,'\n');
}

void uarthw_send_string(const uint8_t * buff,  uint32_t len, void * ud)
/*******************************************************************/
{
    uint32_t i;
    
    //sprintf(debug_string, "Size of data to output %d  %X", len, (unsigned long)((struct UARTEncoder*)ud)->tag);
    //debugf((char*)debug_string,1,DEBUG_XML_DETAIL);
    
    debugf(DEBUG_GENERAL, "Send DB data.");
    UARTEncoder * uart_encoder = ((struct UARTEncoder*)ud);
    UARTEncoderTag * uart_encoder_tag = (struct UARTEncoderTag*)uart_encoder->tag;
    
    for (i = 0; i< len; i++)
    {
        UARTCharPut(uart_encoder_tag->uart_base, buff[i]);
    }
}

//****************************************************************************
//
// Local Functions
//
//****************************************************************************

